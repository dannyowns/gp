GP - A General Purpose Blocks Programming System  
Copyright (c) 2017 John Maloney, Jens Mönig, and Yoshiki Ohshima

GP is a Scratch-like blocks programming language designed for "general purpose" programming and application deployment across a wide range of platforms, from desktop and laptop computers to tablets and smart phones.

GP is being developed by John Maloney, Jens Mönig, and Yoshiki Ohshima and is licensed under the MIT open source license (see LICENSE.txt). The source code will be made public some time after the first public 1.0 (not beta) release.

To run GP on Mac OS X, double-click on "gp" in the GP folder. Requires Mac OS 10.5 or later (earliest tested version is 10.6).

Note: You'll get a warning dialog the first time you run a new version of GP. Depending on your Mac's security settings, you can either click the "Open" button to proceed or click the "?" button to find out what to do.