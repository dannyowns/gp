GP - A General Purpose Blocks Programming Language  
Copyright (c) 2014-2017 John Maloney, Jens Mönig, and Yoshiki Ohshima

GP is a free Scratch-like blocks programming language designed for "general purpose" programming and application deployment across a wide range of platforms.

GP is being developed by John Maloney, Jens Mönig, and Yoshiki Ohshima. The source code will be made public some time after the first public 1.0 release.

The GP folder contains executables for Windows, Macintosh, Linux (tested on Ubuntu), and Raspberry Pi. To run GP, double-click on the executable for your platform.

The Examples folder contains a set of demo and starter projects. If the user modifies and saves any of these projects, the modified version will be saved in their own GP folder. The original project will not be changed.

The runtime folder contains some files that GP needs, including the source code for the GP programming environment itself, which is written in a textual form of GP. It also includes sample MIDI music files, GP's musical instrument library, some low-level technical documentation, and the license. Most people won't need to look in this folder.

GP.app has a copy of the runtime folder built in.

For more info or help, see https://gpblocks.org.
