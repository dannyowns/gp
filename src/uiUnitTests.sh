#!/bin/sh

ARGS=""

cd ..

for var in "$@"; do
	ARGS+=`echo src/tests/*${var}*.gp`" "
done

if [ -z "$ARGS" ]
then
	ARGS=src/tests/uitest?*.gp
fi

for t in $ARGS; do
	echo $t
	./mac_gp runtime/lib/*.gp $t
done
