#!/bin/sh

# Build headless VM
# On Windows, add -D NO_SOCKETS, omit socketPrims.c, and replace -lpthread with -lwinmm

gcc -std=gnu99 -m32 -Wall \
  -D NO_GRAPHICS -D NO_SOUND -D NO_CAMERA \
  cache.c dict.c gp.c embeddedFS.c interp.c mem.c memGC.c oop.c parse.c prims.c serialPortPrims.c sha2.c socketPrims.c \
  -lm -lpthread -lz \
  -o gp_headless
