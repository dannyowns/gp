to demo {
  page = (newPage 1000 800)
  open page 'GP Explorer'
  exp = (explorerOn page)
  setPosition (morph exp) 10 10
  addPart page exp
  startStepping page
}

demo