// interactive turtle blocks demo

defineClass Sheet color morph

method initialize Sheet clr {
  morph = (newMorph this)
  setGrabRule morph 'handle'
}

method redraw Sheet {
  setCostume morph (newBitmap (width (bounds morph)) (height (bounds morph)) color)
}

method wantsDropOf Sheet handler {return (isClass handler 'Turtle')}

to demo {
  page = (newPage 900 500)
  open page

  se = (newScriptEditor 300 400)
  setPosition (morph se) 50 50
  resizeHandle se
  addPart page se

  s = (new 'Sheet' (color 255 255 220))
  initialize s
  setPosition (morph s) 400 50
  setExtent (morph s)  50 50
  resizeHandle s
  addPart page s

  s = (new 'Sheet' (color 255 220 255))
  initialize s
  setPosition (morph s) 415 65
  setExtent (morph s) 50 50
  resizeHandle s
  addPart page s

  s = (new 'Sheet' (color 220 255 255))
  initialize s
  setPosition (morph s) 430 80
  setExtent (morph s)  50 50
  resizeHandle s
  addPart page s

  t = (newTurtle)
  setPosition (morph t) 630 230
  setField t 'x' 650
  setField t 'y' 230
  addPart page t

  startStepping page
}

demo
