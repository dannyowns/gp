to listFonts {
  openWindow 150 20 // need a window to use fonts
  print 'Font list:'
  fonts = (fontNames)
  for i (count fonts) {
    fontName = (at fonts i)
    n = (glyphCount fontName)
    print fontName '-- ascent:' (fontAscent) 'descent:' (fontDescent) 'glyphs:' (glyphCount fontName)
  }
  closeWindow
}

to glyphCount fontName {
  setFont fontName 12
  count = 0
  for i 65535 {
    if (fontHasGlyph i) { count += 1 }
  }
  return count
}

listFonts
