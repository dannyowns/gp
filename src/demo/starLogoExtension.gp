addExtension (authoringSpecs) 'Star Logo2' (array
  (array 'r'	'closestCarInFront'		'closest car in front')

  (array 'r' 'createPatches'			'new patches w _ h _' 'num num' 200 125)
  (array ' ' 'self_drawPatches'			'show patches _ color _ : scale _' 'patches color num')
  (array ' ' 'diffusePatches'			'diffuse patches _ : factor _' 'patches num' nil 0.1)
  (array ' ' 'evaporatePatches'			'evaporate _ rate _' 'patches num' nil 10)

  (array ' ' 'self_getPatch'			'get my patch _' 'patches')
  (array ' ' 'self_setPatch'			'set my patch _ to _' 'patches num' nil 0)
  (array ' ' 'self_increasePatch'		'increase my patch _ by _' 'patches num' nil 0)

  (array 'r' 'self_gradientStrength'	'gradient strength _' 'patches num')
  (array 'r' 'self_gradientDirection'	'gradient direction _' 'patches num')

  (array ' ' 'self_ifOffstageWrap'		'if offstage, wrap around')
)

// [x] new patches w _ h _
// [x] show patches _ color _
// [x] diffuse patches _
// [x] evaporate patches _ rate _
// [ ] get my patch _
// [ ] set my patch _  to _
// [ ] increase my patch _ by _
// [ ] my patch gradient _ : threshold _

to closestCarInFront {
  distanceToClosest = 10000000
  closest = 0
  for thing (self_neighbors 100000) {
	dir = (self_directionToSprite thing)
	if (or (dir > 350) (dir < 10)) {
	  if ((self_distanceToSprite thing) < distanceToClosest) {
		closest = thing
		distanceToClosest = (self_distanceToSprite thing)
	  }
	}
  }
  return closest
}

defineClass Patches w h data

method width Patches { return w }
method height Patches { return h }
method data Patches { return data }

to createPatches w h {
  return (new 'Patches' w h (newArray (w * h) 0))
}

to self_drawPatches patches color scale {
  m = (morph (implicitReceiver))
  bm = (costumeData m)
  if (not (isClass bm 'Bitmap')) { return }
  if (or ((width bm) != (width patches)) ((height bm) != (height patches))) {
	bm = (newBitmap (width patches) (height patches))
	setCostume m bm
  }
  patchData = (data patches)
  pixelData = (pixelData bm)
  r = (red color)
  g = (green color)
  b = (blue color)
  rgb = (((r << 16) | (g << 8)) | b)
  starLogoDrawPatches patchData pixelData rgb scale
  costumeChanged m
}

to self_drawPatchesOLD patches color {
  m = (morph (implicitReceiver))
  bm = (costumeData m)
  if (not (isClass bm 'Bitmap')) { return }
  if (or ((width bm) != (width patches)) ((height bm) != (height patches))) {
	bm = (newBitmap (width patches) (height patches))
	setCostume m bm
  }
  patchData = (data patches)
  pixelData = (pixelData bm)
  r = (red color)
  g = (green color)
  b = (blue color)
  for i (count pixelData) {
//	a = (round (255 * (at patchData i)))
	a = (toInteger (at patchData i))
	if (a < 0) { a = 0 }
	if (a > 255) { a = 255 }
	setPixelRGBA pixelData i r g b a
//	setPixelRGBA pixelData i r g b (clamp a 0 255)
  }
  costumeChanged m
}

method evaporatePatches Patches rate {
  factor = ((1000 - rate) / 1000)
  factor = (clamp factor 0 1)
  if (1 == factor) { return }
  for i (count data) {
	atPut data i (factor * (at data i))
  }
}

method diffusePatches Patches factor {
//   if (isNil factor) { factor = 0.1 }
//   factor = (clamp factor 0 0.25)
  starLogoDiffuse1 data w factor
}

method diffusePatches1 Patches factor {
  if (isNil factor) { factor = 1 }
  oldData = (copy data)
  rowRange = (range 2 (w - 1))
  for r (range 2 (h - 1)) {
	for c rowRange {
	  i = ((w * (r - 1)) + c)
	  prevRow = (i - w)
	  nextRow = (i + w)
	  sum = (+
		(at oldData (prevRow - 1)) (at oldData prevRow)      (at oldData (prevRow + 1))
	  	(at oldData (i - 1))       (factor * (at oldData i)) (at oldData (i + 1))
	  	(at oldData (nextRow - 1)) (at oldData nextRow)      (at oldData (nextRow + 1))
	  )
	  atPut data i (sum / (factor + 8))
	}
  }
}

method diffusePatches2 Patches factor {
  if (isNil factor) { factor = 0.1 }
  oldData = (copy data)
  rowRange = (range 2 (w - 1))
  for r (range 2 (h - 1)) {
	for c rowRange {
	  i = ((w * (r - 1)) + c)
	  prevRow = (i - w)
	  nextRow = (i + w)
	  centerVal = (at oldData i)
	  neighborSum = (+
		((at oldData prevRow) - centerVal)
	  	((at oldData (i - 1)) - centerVal)
		((at oldData (i + 1)) - centerVal)
	  	((at oldData nextRow) - centerVal)
	  )
	  sum = (centerVal + (factor * neighborSum))
	  atPut data i sum
	}
  }
}

to self_getPatch patches {
  m = (morph (implicitReceiver))
  stage = (ownerThatIsA m 'Stage')
  p = (rotationCenter m)
  if (notNil stage) {
    p = (normal stage (first p) (last p))
	atPut p 2 (0 - (at p 2))
  }
  w = (width patches)
  h = (height patches)
  patchesScale = (800 / w)
  x = (round (((at p 1) + 400) / patchesScale))
  y = (round ((250 - (at p 2)) / patchesScale))
  if (or (x < 1) (x > w)) { return 0 }
  if (or (y < 1) (y > h)) { return 0 }
  i = ((w * (y - 1)) + x)
  return (at (data patches) i)
}

to self_setPatch patches newValue {
  m = (morph (implicitReceiver))
  stage = (ownerThatIsA m 'Stage')
  p = (rotationCenter m)
  if (notNil stage) {
    p = (normal stage (first p) (last p))
	atPut p 2 (0 - (at p 2))
  }
  w = (width patches)
  h = (height patches)
  patchesScale = (800 / w)
  x = (round (((at p 1) + 400) / patchesScale))
  y = (round ((250 - (at p 2)) / patchesScale))
  if (or (x < 1) (x > w)) { return 0 }
  if (or (y < 1) (y > h)) { return 0 }
  i = ((w * (y - 1)) + x)
  atPut (data patches) i newValue
}

to self_increasePatch patches delta {
  m = (morph (implicitReceiver))
  stage = (ownerThatIsA m 'Stage')
  p = (rotationCenter m)
  if (notNil stage) {
    p = (normal stage (first p) (last p))
	atPut p 2 (0 - (at p 2))
  }
  w = (width patches)
  h = (height patches)
  patchesScale = (800 / w)
  x = (round (((at p 1) + 400) / patchesScale))
  y = (round ((250 - (at p 2)) / patchesScale))
  if (or (x < 1) (x > w)) { return 0 }
  if (or (y < 1) (y > h)) { return 0 }
  i = ((w * (y - 1)) + x)
  newValue = ((at (data patches) i) + delta)
  atPut (data patches) i newValue
}

to self_gradientStrength patches {
  m = (morph (implicitReceiver))
  stage = (ownerThatIsA m 'Stage')
  p = (rotationCenter m)
  if (notNil stage) {
    p = (normal stage (first p) (last p))
	atPut p 2 (0 - (at p 2))
  }
  patchesScale = (800 / (width patches))
  patchX = (round (((at p 1) + 400) / patchesScale))
  patchY = (round ((250 - (at p 2)) / patchesScale))
  vector = (gradientVectorAt patches patchX patchY)
  vecX = (first vector)
  vecY = (last vector)
  result = (3 * ((logBase (sqrt (vecX * vecX) (vecY * vecY)) 10) + 30))
  if (result < 0) { result = 0 }
  return result
}

to self_gradientDirection patches {
  m = (morph (implicitReceiver))
  stage = (ownerThatIsA m 'Stage')
  p = (rotationCenter m)
  if (notNil stage) {
    p = (normal stage (first p) (last p))
	atPut p 2 (0 - (at p 2))
  }
  patchesScale = (800 / (width patches))
  patchX = (round (((at p 1) + 400) / patchesScale))
  patchY = (round ((250 - (at p 2)) / patchesScale))
  vector = (gradientVectorAt patches patchX patchY)
  return (atan (last vector) (first vector))
}


to self_ifOffstageWrap {
  m = (morph (implicitReceiver))
  stage = (ownerThatIsA m 'Stage')
  if (isNil stage) { return }
  p = (rotationCenter m)
  p = (normal stage (first p) (last p))

  newX = ((((first p) + 1200) % 800) - 400)
  newY = ((((last p) + 750) % 500) - 250)
  newP = (transform stage newX newY)
  placeRotationCenter m (first newP) (last newP)
}

method gradientVectorAt Patches x y {
  // Return an (x, y) vector for the gradient at the given location.
  x = (clamp x 2 (w - 1))
  y = (clamp y 2 (h - 1))

  i = ((w * (y - 1)) + x)
  prevRow = (i - w)
  nextRow = (i + w)
  myVal = (at data i)

  // compute weighted vector sum of differences with neighbors
  dx = 0
  dy = 0
  // left and right
  diff = ((at data (i - 1)) - myVal)
  dx += (- diff)
  diff = ((at data (i + 1)) - myVal)
  dx += diff
  // above and below
  diff = ((at data prevRow) - myVal)
  dy += (- diff)
  diff = ((at data nextRow) - myVal)
  dy += diff
  // corners
  diff = ((at data (prevRow - 1)) - myVal)
  dx += (- diff)
  dy += (- diff)
  diff = ((at data (prevRow + 1)) - myVal)
  dx += diff
  dy += (- diff)
  diff = ((at data (nextRow - 1)) - myVal)
  dx += (- diff)
  dy += diff
  diff = ((at data (nextRow + 1)) - myVal)
  dx += diff
  dy += diff

  return (array dx dy)
}
