defineClass SosaruSwitch board index
method switch SosaruSwitch {
  setState this (not (state this))
}
method setState SosaruSwitch bool {atPut (getField board 'state') index bool}
method state SosaruSwitch {return (at (getField board 'state') index)}

defineClass SosaruBoard state switches triggers flipMap scoreText moveText moves page

method random SosaruBoard {
  return ((rand 0 1) == 0)
}

method init SosaruBoard p {
  page = p
  moves = 0
  flipMap = (array (array 1 2 4) (array 1 2 3 5) (array 2 3 6)
                   (array 1 4 5 7) (array 2 4 5 6 8) (array 3 5 6 9)
                   (array 4 7 8) (array 5 7 8 9) (array 6 8 9))
  state = (newArray 9)
  switches = (newArray 9)
  triggers = (newArray 9)
  for i 9 {
    atPut state i (random this)
    s = (new 'SosaruSwitch' this i)
    t = (toggleButton (action 'flipAt' this i) (action 'state' s))
    setPosition (morph t) ((((i - 1) % 3) * 45) + 5) ((((i - 1) / 3) * 45) + 5)
    addPart (morph page) (morph t)
    atPut switches i s
    atPut triggers i t
  }
  scoreText = (newText (join 'score: ' (toString (score this))) 'Baskerville' 14 (color) 'right')
  setPosition (morph scoreText) 5 170
  addPart (morph page) (morph scoreText)

  moveText = (newText (join 'moves: ' '0') 'Baskerville' 14 (color) 'right')
  setPosition (morph moveText) 70 170
  addPart (morph page) (morph moveText)
}

method score SosaruBoard {
  point = 0
  for i (count state) {
    if (at state i) {
      point += 1
    }
  }
  return point
}

method flipAt SosaruBoard index {
  moves += 1
  a = (at flipMap index)
  for i (count a) {
    ind = (at a i)
    switch (at switches ind)
    refresh (at triggers ind)
  }
  setText scoreText (join 'score: ' (toString (score this)))
  setText moveText (join 'moves: ' (toString moves))

  if ((score this) == 9) {
    text = (newText 'Yay!' 'Baskerville' 48 (color 120 120 250) 'right' (color) 1)
    setPosition (morph text) 20 40
    addPart (morph page) (morph text)
  }
}

to sosaru {
  page = (newPage 150 200)
  open page

  sosaru = (new 'SosaruBoard')
  init sosaru page
  startStepping page
}

sosaru
