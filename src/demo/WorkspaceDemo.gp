to demo {
  page = (newPage 1000 800)
  open page 'GP Workspace'
  ws = (new 'Workspace')
  initialize ws
  setPosition (morph ws) 10 10
  addPart page ws
  startStepping page
}

demo