defineClass Atom c x y dx dy

to newAtom {
  c = (randomColor)
  x = (rand 10 485)
  y = (rand 10 485)
  dx = (rand -10 10)
  dy = (rand -10 10)
  if (and (dx == 0) (dy == 0)) { dx = 1; dy = -1 }
  return (new 'Atom' c x y dx dy)
}

method step Atom {
  // bounce: if outside of box, ensure direction is inward
  if (x < 10) { dx = (abs dx) }
  if (y < 10) { dy = (abs dy) }
  if (x > 485) { dx = (0 - (abs dx)) }
  if (y > 485) { dy = (0 - (abs dy)) }
  x += dx
  y += dy
}

method draw Atom {
  fillRect nil c x y 5 5
}

defineClass BouncingAtoms atoms

method run BouncingAtoms {
  while true {
	clearBuffer
	for i (count atoms) {
	  atom = (at atoms i)
	  step atom
	  draw atom
	}
	flipBuffer
  }
}
 
to newBouncingAtoms n {
  openWindow
  atoms = (list)
  repeat n { add atoms (newAtom) }
  return (new 'BouncingAtoms' atoms)
}

run (newBouncingAtoms 10000)
