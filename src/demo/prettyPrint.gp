to baseName aString {
  i = (lastIndexOf (letters aString) '/')
  if (notNil i) {
	return (substring aString (i + 1) (count aString))
  }
  return aString
}

to prettyPrintFiles {
  p = (new 'PrettyPrinter')
  fileList = (commandLine)
  i = (indexOf fileList '--')
  if (isNil i) {
    print 'No arguments. List files to prettyPrint after "--" on the command line.'
	return
  }
  fileList = (copyArray fileList ((count fileList) - i) (i + 1))
  for fileName fileList {
    base = (baseName fileName)
    print fileName (join '-> pp/' base)
    prettyPrintFileToFile p fileName (join 'pp/' base)
  }
}

prettyPrintFiles
