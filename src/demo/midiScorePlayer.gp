defineClass NoteOn when key velocity chan duration
defineClass NoteOff when key chan
defineClass MIDIScorePlayer voices voiceIndex noteOffs timer

to newMIDIScorePlayer { return (new 'MIDIScorePlayer' (array) (array)) 0 }

method addVoice MIDIScorePlayer v { voices = (copyArrayWith voices v) }

method play MIDIScorePlayer {
  print 'Starting playback'
  reset this
  while (notDone this) {
    t = (msecs timer)
	endNotes this t
	startNotes this t
	sleep 10
  }
  allNotesOff
  print 'Done!'
}

method reset MIDIScorePlayer {
  voiceIndex = (newArray (count voices))
  fillArray voiceIndex 1
  noteOffs = (list)
  timer = (newTimer)
}

method notDone MIDIScorePlayer {
  for v (count voices) {
    if ((at voiceIndex v) <= (count (at voices v))) { return true }
  }
  return ((count noteOffs) > 0)
}

method endNotes MIDIScorePlayer t {
  if ((count noteOffs) == 0) { return }
  newNoteOffs = (list)
  for i (count noteOffs) {
	r = (at noteOffs i)
	if ((getField r 'when') < t) {
	  noteOff (getField r 'key') (getField r 'chan')
	} else {
	  add newNoteOffs r
	}
  }
  noteOffs = newNoteOffs
}

method startNotes MIDIScorePlayer t {
  for v (count voices) {
    if ((at voiceIndex v) <= (count (at voices v))) {
	  note = (at (at voices v) (at voiceIndex v))
	  if ((getField note 'when') < t) {
		noteOn (getField note 'key') (getField note 'velocity') v
		add noteOffs (new 'NoteOff' (t + (getField note 'duration')) (getField note 'key') v)
		atPut voiceIndex v ((at voiceIndex v) + 1)
	  }
	}
  }
}

to noteOn key velocity chan {
  sendMIDI (144 | ((chan - 1) & 15)) (key & 127) (velocity & 127)
}

to noteOff key chan {
  sendMIDI (144 | ((chan - 1) & 15)) (key & 127) 0
}

to allNotesOff {
  for chan 16 { sendMIDI (176 | ((chan - 1) & 15)) 123 0 }
}

to copyArrayWith a newEl {
  a = (copyArray a ((count a) + 1))
  atPut a (count a) newEl
  return a
}
