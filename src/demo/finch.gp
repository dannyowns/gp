defineClass Finch morph

to newFinch {
  finch = (new 'Finch')
  setField finch 'morph' (newMorph finch)
  setCostume (morph finch) (newBitmap 30 30 (color 255 100 50))
  setGrabRule (morph finch) 'handle'
  return finch
}

// context menu

method touchHold Finch aHand {
  menu = (menu 'Finch' this)
  addBlock this (finchHelpBlock this) menu
  addBlock this (finchCmdBlock this) menu
  addBlock this (finchInputBlock this) menu
  popUpAtHand menu (page aHand)
  return true
}

method addBlock Finch aBlock menu {
  addItem menu (fullBitmap (morph aBlock)) (action 'grab' this aBlock)
}

method grab Finch aBlock {
  h = (hand (handler (root morph)))
  setCenter (morph aBlock) (x h) (y h)
  grab h aBlock
}

method finchHelpBlock Finch { return (block 'command' (color 74 108 212) 'finchHelp' (slot this)) }
method finchCmdBlock Finch { return (block 'command' (color 74 108 212) 'finchCmd' (slot this) (slot 'motor/100/100')) }
method finchInputBlock Finch { return (block 'reporter' (color 74 108 212) 'finchInput' (slot this) (slot 'temperature')) }

// Finch API

method finchSend Finch path {
  if (isNil path) { path = '' }
  socket = (openClientSocket '127.0.0.1' 22179)
  if (isNil socket) { return '' }
  nl = (string 13 10)
  request = (join
    'GET ' path ' HTTP/1.1' nl
    'Host: 127.0.0.1' nl nl)
  writeSocket socket request
  sleep 10 // wait a bit
  response = (readSocket socket)
  closeSocket socket
  return response
}

method finchHelp Finch { print (finchSend this '/') }
method finchCmd Finch cmd { finchSend this (join '/finch/out/' cmd) }

method finchInput Finch sensor {
  response = (finchSend this (join '/finch/in/' sensor))
  data = (words (last (lines response)))
  for i (count data) {
    atPut data i (first (parse (at data i)))
  }
  print 'data:' data
  if ((count data) == 0) { return nil }
  if ((count data) == 1) { return (first data) }
  return data
}

to finchDemo {
  page = (newPage 500 400)
  open page false 'Finch Demo'
  addPart page (newFinch)
  startStepping page
}

finchDemo
