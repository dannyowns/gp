// interactive blocks demo

to demo {
  page = (newPage 840 420)
  open page

  y = 10
  space = 10


  b = (block 'command' (color 74 108 212) 'imagine' (slot 42) 'or' (slot 'Garply') (cmd))
  setPosition (morph b) space y
  addPart page b
  y += (space + (height (bounds (morph b))))

  b = (block 'command' (color 143 86 227) 'print' (slot 'Hello, World!'))
  setPosition (morph b) space y
  addPart page b
  y += (space + (height (bounds (morph b))))

  b = (block 'command' (color 207 74 217) 'program' (slot 'something'))
  setPosition (morph b) space y
  addPart page b
  y += (space + (height (bounds (morph b))))

  b = (block 'command' (color 230 168 34) 'scale' (slot 5) (slot 2))
  setPosition (morph b) space y
  addPart page b
  y += (space + (height (bounds (morph b))))

  b = (block 'command' (color 98 194 19) 'share')
  setPosition (morph b) space y
  addPart page b
  y += (space + (height (bounds (morph b))))

  b = (block 'reporter' (color 98 194 19) 'join' (slot 'Hello ') (slot 'World'))
  setPosition (morph b) space y
  addPart page b
  y += (space + (height (bounds (morph b))))

  b = (block 'command' (color 0 161 120) 'amaze' (slot 91))
  setPosition (morph b) space y
  addPart page b
  y += (space + (height (bounds (morph b))))

  b = (block 'command' (color 4 148 220) 'discover')
  setPosition (morph b) space y
  addPart page b
  y += (space + (height (bounds (morph b))))
 
  b = (block 'reporter' (color 4 148 220) 'version')
  setPosition (morph b) space y
  addPart page b
  y += (space + (height (bounds (morph b))))

  b = (block 'command' (color 230 168 34) 'if' (slot 42) (cmd) 'else' (cmd))
  setPosition (morph b) space y
  addPart page b
  y += (space + (height (bounds (morph b))))

  b = (block 'command' (color 230 168 34) 'repeat' (slot 42) (cmd))
  setPosition (morph b) space y
  addPart page b
  y += (space + (height (bounds (morph b))))

  b = (block 'command' (color 230 168 34) 'while' (slot 42) (cmd))
  setPosition (morph b) space y
  addPart page b

  se = (newScriptEditor 300 400)
  setPosition (morph se) 250 space
  addPart page se

  t = (newTurtle)
  setPosition (morph t) 600 100
  setField t 'x' 600
  setField t 'y' 100
  addPart page t

  startStepping page
}

demo
