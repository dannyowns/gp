defineClass FloodFillDemo morph window image slider threshold clickPoint buttonsPane imagePane

to newFloodFillDemo { return (initialize (new 'FloodFillDemo')) }

method initialize FloodFillDemo {
  scale = (global 'scale')
  window = (window 'Flood Fill Demo')
  morph = (morph window)
  setHandler morph this

  buttonsPane = (makeButtonsPane this)
  addPart morph (morph buttonsPane)

  imagePane = (scrollFrame (newBox (newMorph) (gray 100) 0 0 false))
  addPart morph (morph imagePane)

  setGrabRule morph 'handle'
  setMinExtent morph (scale * 300) (scale * 200)
  setExtent morph (minWidth morph) (minHeight morph)

  return this
}

method redraw FloodFillDemo {
  fixLayout window
  redraw window
  fixPaneLayout this
}

method fixPaneLayout FloodFillDemo {
  packer = (newPanePacker (clientArea window))
  packPanesH packer buttonsPane '100%'
  packPanesH packer imagePane '100%'
  packPanesV packer buttonsPane 67 imagePane '100%'
  finishPacking packer
}

method makeButtonsPane FloodFillDemo {
  scale = (global 'scale')
  pane = (newBox nil (gray 250) nil nil false false)

  buttonX = (10 * scale)
  buttonY = (10 * scale)
  b = (button this 'Choose image' 'chooseImage')
  setPosition b buttonX buttonY
  addPart (morph pane) b

  buttonX = ((right b) + (10 * scale))

  b = (button this 'Reset' 'resetImage')
  setPosition b buttonX buttonY
  addPart (morph pane) b

  w = (100 * scale)
  h = (8 * scale)
  threshold = 0
  slider = (slider 'horizontal' w (action 'setThreshold' this) h 0 7 threshold 1)
  setPosition (morph slider) (10 * scale) (45 * scale)
  addPart (morph pane) (morph slider)

  return pane
}

method button FloodFillDemo label selector {
  scale = (global 'scale')
  w = (scale * 54)
  h = (scale * 23)
  nbm = (buttonBody this label w h false)
  hbm = (buttonBody this label w h true)
  b = (new 'Trigger' nil (action selector this) nbm hbm hbm)
  setData b label
  setMorph b (newMorph b)
  setCostume (morph b) nbm
  return (morph b)
}

method buttonBody FloodFillDemo label w h highlight {
  scale = (global 'scale')
  fillColor = (gray 230)
  borderColor = (gray 120)
  textColor = (gray 100)
  border = (scale * 1)
  radius = (scale * 4)
  if (true == highlight) {
    fillColor = (darker fillColor 15)
	textColor = (darker textColor 15)
  }
  setFont 'Arial Bold' (scale * 14)
  w = (max w ((stringWidth label) + 8))
  bm = (newBitmap w h)
  pen = (newPen bm)
  setColor pen borderColor
  fillRoundedRect pen (rect 0 0 w h) radius
  setColor pen fillColor
  fillRoundedRect pen (rect border border (w - (2 * border)) (h - (2 * border))) radius

  labelBM = (stringImage label 'Arial Bold' (scale * 14) textColor)
  x = ((w - (width labelBM)) / 2)
  y = ((h - (height labelBM)) / 2)

  drawBitmap bm labelBM x y
  return bm
}

// button actions

method chooseImage FloodFillDemo {
  menu = (menu nil (action 'readImageFile' this) true)
  for fn (listFiles) {
    if (endsWith fn '.png') { addItem menu fn }
  }
  popUpAtHand menu (global 'page')
}

method readImageFile FloodFillDemo fileName {
  data = (readFile fileName true)
  image = (readFrom (new 'PNGReader') data)
  resetImage this
}

method resetImage FloodFillDemo {
  if (isNil image) { return }
  clickPoint = nil
  setCostume (morph (contents imagePane)) (copy image)
  updateSliders imagePane
}

method setThreshold FloodFillDemo n {
  if (notNil slider) { setValue slider n }
  if (n == threshold) { return }
  threshold = n
  if (and (notNil image) (notNil clickPoint)) {
	bm = (copy image)
	floodFill bm (at clickPoint 1) (at clickPoint 2) (color 0 0 200) threshold
	setCostume (morph (contents imagePane)) bm
  }
}

method clicked FloodFillDemo hand {
  m = (morph (contents imagePane))
  imgX = ((x hand) - (left m))
  imgY = ((y hand) - (top m))
  clickPoint = (array imgX imgY)
  if (and (notNil threshold) (notNil image)) {
	imgMorph = (morph (contents imagePane))
	floodFill (costume imgMorph) imgX imgY (color 0 0 200) threshold
	changed imgMorph
  }
}

to floodFillDemo {
  page = (newPage 1000 800)
  open page tryRetina
  addPart page (newFloodFillDemo)
  (setGlobal 'page' page)
  go
}

floodFillDemo
