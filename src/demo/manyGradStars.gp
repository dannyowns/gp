to star {
 return (toFloat32Array (array
    250.00000 150.00000 237.65650 183.01064 225.31301 216.02128
    225.31301 216.02128 190.10368 217.55979 154.89434 219.09830
    154.89434 219.09830 182.47498 241.03850 210.05562 262.97871
    210.05562 262.97871 200.63855 296.94020 191.22147 330.90169
    191.22147 330.90169 220.61073 311.45084 250.00000 292.00000
    250.00000 292.00000 279.38926 311.45084 308.77852 330.90169
    308.77852 330.90169 299.36144 296.94020 289.94437 262.97871
    289.94437 262.97871 317.52501 241.03850 345.10565 219.09830
    345.10565 219.09830 309.89631 217.55979 274.68698 216.02128
    274.68698 216.02128 262.34349 183.01064 250.00000 150.00000
))
}

defineClass Star geometry x y s a dy da ds color

method x Star {return x}
method y Star {return y}
method s Star {return s}
method a Star {return a}

method randomColor Star {
  color = (toFloat32Array (array 0.0 ((rand 0 255) / 255.0) ((rand 0 255) / 255.0) ((rand 0 255) / 255.0)
                                 0.0 ((rand 0 255) / 255.0) ((rand 0 255) / 255.0) ((rand 0 255) / 255.0)
                                 1.0
				 210.0
				 210.0
				280.0
				280.0))
  a = ((rand 0 255) / 255.0)
  atPut color 1 a
  atPut color 5 a
}

method next Star ySize {
  y += dy
  if ((y + 230.0) >= ySize) {
    y = -270.0
  }
  a += da
}

method renderStar Star gezira bits {
  renderGeometry (getField geometry 'data') (getField color 'data') x y s a bits gezira
}

method renderStrokedStar Star gezira bits {
  renderStrokeGeometry (getField geometry 'data') (getField color 'data') x y s a 8.0 bits gezira
}

to run {
  openWindow
  bits = (allocateGeziraImage 500 500)
  bm = (newBitmap 500 500 (color))
  timer = (newTimer)

  stars = (newArray 500)
  data = (star)
  for i (count stars) {
    atPut stars i (new 'Star' data (250 - (rand 0 500)) (250 - (rand 0 500)) 0.25 0 (((rand 0 200) / 100.0) + 0.1) (((rand 0 100) - 50) / 2000.0))
    randomColor (at stars i)
  }

  msecs = (msecs timer)
  lastRenderCount = 0
  for renderCount 1000 {
    evt = (nextEvent)
    if (and (notNil evt) (notNil (at evt 'textinput'))) {
      print evt
    }
    fillGeziraImage bits
    gezira = (initializeGezira bits 5)
    for i (count stars) {
      s = (at stars i)
      renderStar s gezira bits
      next s 500
    }
    geziraSync gezira
    copyBits bits bm
    clearBuffer c
    drawBitmap nil bm
    flipBuffer
    now = (msecs timer)
    if ((now - msecs) > 1000) {
      print (renderCount - lastRenderCount)
      msecs = now
      lastRenderCount = renderCount
    }
  }
}

run
