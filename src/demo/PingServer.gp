// PingServer.gp - A trivial server that responds with the uptime and hit count.

defineClass PingServer startSecs requestCount gcCount

to ping host {
// '127.0.0.1' // local host
// '208.82.117.98' // tinlizzie
// '104.236.119.50' // digital ocean
  if (isNil host) { host = '104.236.119.50' }
  port = 2001
  t = (newTimer)
  sock = (openClientSocket host port)
  result = ''
  while (and ('' == result) ((msecs t) < 500)) {
	result = (join result (readSocket sock))
  }
  closeSocket sock
  print result (join '(' (msecs t) ' msecs)')
}

method start PingServer {
  // start (new 'PingServer')
  startSecs = (first (time))
  requestCount = 0
  gcCount = 0
  gc
  serverLoop this
}

method serverLoop PingServer {
  port = 2001
  serverSocket = (openServerSocket port)
  print 'Ping server listening on port' port
  while true {
	clientSock = (acceptConnection serverSocket)
	if (notNil clientSock) {
	  handleConnection this clientSock
	  gcIfNeeded this
	} else {
	  sleep 5
	}
  }
}

method handleConnection PingServer socket {
  t = (newTimer)
  addr = (remoteAddress socket)
  requestCount += 1
  response = (join 'Requests: ' requestCount ' Uptime: ' (timeSince startSecs) ' GCs: ' gcCount)
  writeSocket socket response
  closeSocket socket
  print 'Ping from' addr (join '(' (msecs t) ' msecs)')
}

method gcIfNeeded PingServer {
  wordsAllocated = (at (memStats) 4)
  if (wordsAllocated > 500000) {
	gc
	gcCount += 1
  }
}
