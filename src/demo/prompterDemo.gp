
to demo {
  page = (newPage 800 500)
  open page true 'GP Prompter'


  // non-callback version:

  b = (pushButton 'you...' (color 180 130 130) (action 'askName' page) 100 50)
  setPosition (morph b) 250 200
  addPart (morph page) (morph b)

  b = (pushButton 'bio...' (color 180 130 130) (action 'askBio' page) 100 50)
  setPosition (morph b) 400 200
  addPart (morph page) (morph b)

  b = (pushButton 'ask...' (color 180 130 130) (action 'askAlright' page) 100 50)
  setPosition (morph b) 550 200
  addPart (morph page) (morph b)


  // callback version

  b = (pushButton 'you...' (color 130 130 180) (action 'askNameCallback' page) 100 50)
  setPosition (morph b) 250 400
  addPart (morph page) (morph b)

  b = (pushButton 'bio...' (color 130 130 180) (action 'askBioCallback' page) 100 50)
  setPosition (morph b) 400 400
  addPart (morph page) (morph b)

  b = (pushButton 'ask...' (color 130 130 180) (action 'confirmCallback' page) 100 50)
  setPosition (morph b) 550 400
  addPart (morph page) (morph b)

  startStepping page
}


// non-callbacks

to askName aPage {
  inform aPage (join 'your name is' (string 13) (prompt aPage 'what''s your name?' 'Alois' 'line'))
}

to askBio aPage {
  inform aPage (join 'your story is' (string 13) (prompt aPage 'what''s your story?' 'Well...' 'editable'))
}

to askAlright aPage {
  inform aPage (join 'your answer is' (string 13) (confirm aPage nil 'I am alright'))
}

// callback versions

to askNameCallback aPage {
  prompt aPage 'what''s your name?' 'Alois' 'line' (action 'showName' aPage)
}

to askBioCallback aPage {
  prompt aPage 'what''s your story?' 'Well...' 'editable' (action 'showBio' aPage)
}

to confirmCallback aPage {
  confirm aPage nil 'I am alright' nil nil (action 'showConfirmation' aPage)
}

to showName aPage aString {
  inform aPage (join 'your name is' (string 13) aString)
}

to showBio aPage aString {
  inform aPage (join 'your story is' (string 13) aString)
}

to showConfirmation aPage aBool {
  inform aPage (join 'you confirmed' (string 13) aBool)
}

demo
