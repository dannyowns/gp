// test of pane layout

defineClass LayoutTest morph window levelFrame categoriesFrame subcategoriesFrame blocksFrame scriptsFrame

method initialize LayoutTest {
  scale = (global 'scale')
  window = (window 'Methods Palette')
  morph = (morph window)
  setHandler morph this
  setMinExtent morph (scale * 300) (scale * 200)

  box = (newBox)
  levelFrame = (scrollFrame box (randomColor))
  addPart morph (morph levelFrame)

  box = (newBox)
  categoriesFrame = (scrollFrame box (randomColor))
  addPart morph (morph categoriesFrame)

  box = (newBox)
  subcategoriesFrame = (scrollFrame box (randomColor))
  addPart morph (morph subcategoriesFrame)

  blocksPane = (newBox)
  blocksFrame = (scrollFrame blocksPane  (randomColor))
  addPart morph (morph blocksFrame)

  scriptsPane = (newScriptEditor 10 10)
  scriptsFrame = (scrollFrame scriptsPane (getField scriptsPane 'color'))
  addPart morph (morph scriptsFrame)

  setExtent morph (scale * 500) (scale * 500)
  return this
}

method redraw LayoutTest {
  redraw window
  fixLayout this
}

method fixLayout LayoutTest {
  t = (newTimer)
  fixLayout window
  packer = (newPanePacker (clientArea window))
  packPanesH packer levelFrame 90 blocksFrame '35%' scriptsFrame '65%'
  packPanesH packer categoriesFrame 90
  packPanesH packer subcategoriesFrame 90
  packPanesV packer levelFrame 60 categoriesFrame '20%' subcategoriesFrame '80%'
  packPanesV packer blocksFrame '100%'
  packPanesV packer scriptsFrame '100%'
  finishPacking packer
  print (msecs t) 'msecs'
}

to layoutTest {
  print 'test'
  page = (newPage 600 600)
  open page false
  addPart page (initialize (new 'LayoutTest'))
  startStepping page
}

layoutTest
