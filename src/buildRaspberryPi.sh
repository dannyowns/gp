#!/bin/sh
# Prerequisits:
#   Install packge: pango1.0-dev
#   Configure/make/install SDL2.0.3 and portaudio

gcc -std=gnu99 -Wall -O3 \
  -I/usr/local/include/SDL2 -Ijpeg -Icairo \
  `pkg-config --cflags pangocairo` \
  cache.c dict.c embeddedFS.c events.c gp.c graphicsPrims.c interp.c jpegPrims.c mem.c memGC.c oop.c \
  parse.c prims.c serialPortPrims.c sha1.c sha2.c socketPrims.c soundPrims.c textAndFontPrims.c vectorPrims.c \
  /usr/local/lib/libSDL2.a \
  `pkg-config --libs pangocairo` \
  jpeg/libjpeg.a \
  /usr/local/lib/libportaudio.a -lasound \
 -ldl -lm -lpthread -lrt -lz \
 -o gp
