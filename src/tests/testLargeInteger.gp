call (function {

  l = (largeInteger 128 64 32 16)
  assert (byteAt (getField l 'data') 1) 128
  assert (byteAt (getField l 'data') 2) 64
  assert (byteAt (getField l 'data') 3) 32
  assert (byteAt (getField l 'data') 4) 16

  assert (toStringBase16 l) '80402010'

  l = (l << 3)

  assert (digitLength l) 5
  assert (toStringBase16 l) '0402010080'

  l = (l << 2)
  assert (digitLength l) 5
  assert (toStringBase16 l) '1008040200'

  l = (l >> 2)
  assert (digitLength l) 5
  assert (toStringBase16 l) '0402010080'

  l = (l >> 3)
  assert (digitLength l) 4
  assert (toStringBase16 l) '80402010'

  l = (toLargeInteger 807407616)
  assert (toStringBase16 l) '30201000'

  k = (toLargeInteger 50462976)
  assert (toStringBase16 k) '03020100'

  assert (toStringBase16 (toLargeInteger (l | k))) '33221100'
  assert (l & k) 0

  assert (toStringBase16 (toLargeInteger (l | 16))) '30201010'
  assert (toStringBase16 (toLargeInteger 63)) '3F'
  assert (toStringBase16 ((toLargeInteger 255) << 24)) 'FF000000'
  
  l = (| ((toLargeInteger 255) << 24)
         ((toLargeInteger 192) << 16)
         ((toLargeInteger 224) << 8)
         ((toLargeInteger 63) << 0))

  assert (toStringBase16 l) 'FFC0E03F'

  l = (| ((toLargeInteger 255) << 24)
         ((toLargeInteger 192) << 16)
         ((toLargeInteger 224) << 8)
         ((toLargeInteger 63) << 0))

  assert (toStringBase16 (l & ((toLargeInteger 255) << 24))) 'FF000000'

  assert ((l & ((toLargeInteger 255) << 24)) >> 24) 255

  l = (| (toLargeInteger ((toLargeInteger 0) << 24))
         0
	 0
         13)
  assert l 13 'normalized'

  l = (| (toLargeInteger 0)
         10092544
	 39936
//         24
)
  assert l 10132480 'normalized 10132480'
})
