// test resizing a scrolled general list box

to demo {
  cr = (string 13)
  data = (array
    'eins'
    'zwei'
    (join 'drei' cr '  und')
    'vier'
    (join 'fünf' cr '  sechs' cr 'sieben')
    'acht'
    'neun'
    (join cr 'zehn' cr)
  )

  page = (newPage 800 800)
  open page

  clr = (color 220 220 255)
  lb = (listBox data 'id' 'nop' clr)
  sf = (scrollFrame lb clr)
  setPosition (morph sf) 50 50
  setExtent (morph sf) 100 300
  setGrabRule (morph sf) 'handle'
  addPart (morph page) (morph sf)
  resizeHandle sf

  startStepping page
}

demo
