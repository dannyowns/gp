to test {
  openWindow 1000 750
  // clearBuffer (color 255 255 0)
  pen = (newPen)

  // concentric circles, Bresenham
  setColor pen (color 0 0 100)
  for r 11 {
    drawCircle pen 120 120 (120 - (r * 10))
  }

  // concentric circles, Wu (antialiased)
  setColor pen (color 0 0 100)
  for r 11 {
    drawWuCircle pen 119 599 (120 - (r * 10)) 1 1
    drawWuCircle pen 121 599 (120 - (r * 10)) 1 2
    drawWuCircle pen 121 601 (120 - (r * 10)) 1 3
    drawWuCircle pen 119 601 (120 - (r * 10)) 1 4
  }

  // concentric circle quadrants, Wu (antialiased)
  setColor pen (color 0 0 100)
  for r 11 {
    drawWuCircle pen 120 360 (120 - (r * 10)) 1
  }

  // set up alternating colors
  cd = (dictionary)
  atPut cd 1 (color 255 0 0)
  atPut cd -1 (color 0 255 0)

  // filled circles, non-antialiased, contrasting colors
  c = 1
  for r 11 {
    setColor pen (at cd c)
    c = (c * -1)
    drawCircle pen 360 120 (120 - (r * 10))
  }

  // filled circles, antialiased, contrasting colors
  c = 1
  for r 11 {
    setColor pen (at cd c)
    c = (c * -1)
    fillWuCircle pen 360 360 (120 - (r * 10)) 1
  }

  // filled quadrants, antialiased, contrasting colors
  fillRect nil (color) 360 600 1 1 / center pixel
  c = 1
  for r 11 {
    setColor pen (at cd c)
    c = (c * -1)
    fillWuCircleQuadrant pen 1 359 599 (120 - (r * 10)) 1
    fillWuCircleQuadrant pen 2 361 599 (120 - (r * 10)) 1
    fillWuCircleQuadrant pen 3 361 601 (120 - (r * 10)) 1
    fillWuCircleQuadrant pen 4 359 601 (120 - (r * 10)) 1
  }

  // inversely filled quadrants, antialiased, contrasting colors
//  fillRect nil (color) 360 600 1 1 / center pixel
  c = 1
  for r 11 {
    setColor pen (at cd c)
    c = (c * -1)
    fillInverseWuCircleQuadrant pen 1 599 599 (r * 10) 1
//    fillInverseWuCircleQuadrant pen 2 361 599 (120 - (r * 10)) 1
    fillInverseWuCircleQuadrant pen 3 601 601 (r * 10) 1
    fillInverseWuCircleQuadrant pen 4 599 601 (r * 10) 1
  }

  // filled rounded rectangles, non-antialiased
  c = 1
  rct = (rect 500 10 220 220)
  for r 10 {
    setColor pen (at cd c)
    c = (c * -1)
    fillRoundedRect pen rct (60 - (r * 5))
    rct = (insetBy rct 10)
  }

  // filled rounded rectangles, antialiased
  c = 1
  rct = (rect 500 250 220 220)
  for r 10 {
    setColor pen (at cd c)
    c = (c * -1)
    fillRoundedRect pen rct (60 - (r * 5)) true 1
    rct = (insetBy rct 10)
  }

  // partially filled quadrants
  setColor pen (color 0 0 100)
  fillPartialWuCircleWQuadrant pen 1 839 119 100 90 1
  fillPartialWuCircleWQuadrant pen 2 841 119 100 70 1
  fillPartialWuCircleWQuadrant pen 3 841 121 100 50 1
  fillPartialWuCircleWQuadrant pen 4 839 121 100 30 1

  // thick quadrants
  setColor pen (color 0 0 100)
  setLineWidth pen 5
  drawTopLeftCircleQuadrant pen 839 359 80 true 1
  setLineWidth pen 25
  drawTopRightCircleQuadrant pen 841 359 80 true 1
  setLineWidth pen 40
  drawBottomRightCircleQuadrant pen 841 361 80 true 1
  setLineWidth pen 60
  drawBottomLeftCircleQuadrant pen 839 361 80 true 1

  // thick quadrants with inner color (alternative rendering)
  setColor pen (color 0 0 100)
  setLineWidth pen 5
  drawTopLeftCircleQuadrant pen 839 599 80 true 1 (color 200 200 200)
  setLineWidth pen 25
  drawTopRightCircleQuadrant pen 841 599 80 true 1 (color 200 200 200)
  setLineWidth pen 40
  drawBottomRightCircleQuadrant pen 841 601 80 true 1 (color 200 200 200)
  setLineWidth pen 60
  drawBottomLeftCircleQuadrant pen 839 601 80 true 1 (color 200 200 200)

  flipBuffer
  sleep 2000
}

test

