// test and demonstrate sliders

to sliderDemo {
  page = (newPage 500 300)
  open page

  th = (newText 'value')
  setPosition (morph th) 240 50
  addPart (morph page) (morph th)

  floorText = (newText 'floor')
  setPosition (morph floorText) 20 20
  addPart (morph page) (morph floorText)

  ceilText = (newText 'ceiling')
  setPosition (morph ceilText) 460 20
  addPart (morph page) (morph ceilText)

  sizeText = (newText 'size')
  setPosition (morph sizeText) 240 110
  addPart (morph page) (morph sizeText)

  sh = (slider 'horizontal' 400 (action 'setText' th) 18)
  setPosition (morph sh) 50 20
  addPart (morph page) (morph sh)

  s = (slider 'horizontal' 300 (array (action 'setText' sizeText) (action 'setSize' sh)) nil 0 100 10)
  setPosition (morph s) 100 80
  addPart (morph page) (morph s)

  s = (slider 'vertical' 200 (array (action 'setText' floorText) (action 'setFloor' sh)) nil -50 50 0)
  setPosition (morph s) 20 50
  addPart (morph page) (morph s)

  s = (slider 'vertical' 200 (array (action 'setText' ceilText) (action 'setCeiling' sh)) nil 50 150 100)
  setPosition (morph s) 460 50
  addPart (morph page) (morph s)

  startStepping page
}

sliderDemo
