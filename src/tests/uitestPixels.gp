to unmultiply bm {
  for yy ((height bm) - 1) {
    for xx ((width bm) - 1) {
      pp = (getPixel bm (xx - 1) (yy - 1))
      rr = (red pp)
      gg = (green pp)
      bb = (blue pp)
      aa = (alpha pp)
      if (aa == 0) {
        mm = 0
      } else {
	    ratio = (255.0 / (aa + 0.5))
		rr = (toInteger ((ratio * rr) + 0.5))
		gg = (toInteger ((ratio * gg) + 0.5))
		bb = (toInteger ((ratio * bb) + 0.5))
      }
      cc = (color rr gg bb aa)
      setPixel bm (xx - 1) (yy - 1) cc
   }
  }
}

to pixelTests {
  openWindow 100 200
  str = 'dero desu dennen'
  setFont 'Arial' 9
  w = (stringWidth str)
  h = ((fontAscent) + (fontDescent))
  x = 10
  y = h
  testX = 51
  testY = 7

  bm = (newBitmap w h (color 255 0 0 0))
  drawString bm str (color 255 0 0)
  drawBitmap nil bm x y
  y += h

  pix = (getPixel bm testX testY)
  print 'original r' (red pix) 'g' (green pix) 'b' (blue pix) 'a' (alpha pix)

  bm2 = (newBitmap w h)
  drawBitmap bm2 bm 0 0
  pix = (getPixel bm2 testX testY)
  print 'composited r' (red pix) 'g' (green pix) 'b' (blue pix) 'a' (alpha pix)
  drawBitmap nil bm2 x y
  y += h

  unmultiply bm2
  pix = (getPixel bm2 testX testY)
  print 'unmultiplied r' (red pix) 'g' (green pix) 'b' (blue pix) 'a' (alpha pix)
  drawBitmap nil bm2 x y
  y += h

  flipBuffer
  sleep 5000
}

pixelTests
