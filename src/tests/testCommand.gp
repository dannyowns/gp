to foo {
  return ((arg 1) - (arg 2))
}

assert (foo 3 4) -1 '(foo 3 4)'
// expect (foo 3 true) error 'command 2: '
assert (foo 3 4 5) -1 '(foo 3 4 5)'

to bar {
  return (arg 3)
}

assert (bar 3 4) nil '(bar 3 4)'
assert (bar 3 4 5) 5 '(bar 3 4 5)'

to baz {
  assert (argCount) 3
}

assert (baz 3 4 5) nil '(baz 3 4 5)'
