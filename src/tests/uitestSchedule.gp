// test schedule objects

defineClass Counter count
method increment Counter {count += 1}
method count Counter {print '  ' count}

to test {
  fire = (action 'print' '  fire!')
  counter = (new 'Counter' 0)
  count = (array (action 'increment' counter) (action 'count' counter))

  print
  print 'fire 5 times at 1 sec interval'
  sched = (schedule fire 1000 5)
  while (not (isDone sched)) {
    step sched
  }

  print
  print 'count 7 times at 100 msec interval'
  sched = (schedule count 100 7)
  while (not (isDone sched)) {
    step sched
  }

  print
  print 'repeat forever at 2 sec interval'
  sched = (schedule fire 2000 (action 'return' true))
  while (not (isDone sched)) {
    step sched
    gc
  }
}

test