call (function {
  moduleDef = '
module
  moduleExports test

  // this is the expander
  method foo String {
	return (byteCount this)
  }

  to test {
	return (foo ''abcde'')
  }
'

  mod = (initialize (new 'Module'))
  loadModuleFromString mod moduleDef
  callInitializer mod

  expanders = (getField mod 'expanders')
  assert (isClass expanders 'Array') true
  assert (count expanders) 1

  e = (first expanders)
  assert (isClass e 'Function') true
  assert (classIndex e) (classIndex (class 'String'))

  assert (call (at mod 'test')) 5
})
