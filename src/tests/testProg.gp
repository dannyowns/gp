call (function {
cmd = (at (parse 'to foo a b {
 x = (a + b)
 return (x + 3)}') 1)

fixedFields = (fieldNameCount (classOf cmd))

assert (primName cmd) 'to' 'primName cmd'
assert (getField cmd (fixedFields + 1)) 'foo' 'getField foo'
assert (getField cmd (fixedFields + 2)) 'a' 'getField a'
assert (primName (nextBlock (getField cmd (count cmd))))  'return'


f = (function a b {return (a + b)})
assert (functionName f) nil 'function name'  // How do we make it have a name?
assert (argNames f) (array 'a' 'b') 'argNames'

assert (primName (cmdList f)) 'return' 'function cmdList'

})