call (function {
t = true
f = false
n = nil

assert t true 't'
assert f false 'f'
assert nil n 'nil'

//expect (true = 3) error
//expect (nil = 4) error
})
