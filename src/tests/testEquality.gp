call (function {
x = 1
y = 2

assert (x > y) false '(x > y)'
assert (y > x) true  '(y > x)'

assert (x >= 1) true '(x >= 1)'
assert (x >= 2) false '(x >= 2)'

assert (x < 2) true '(x < 2)'
assert (y < 1) false '(y < 1)'

assert (y <= y) true '(y <= y)'
assert (y <= x) false '(y <= x)'

assert (y == y) true '(y == y)'
assert (y == x) false '(y == x)'

assert (y != y) false '(y != y)'
assert (y != x) true '(y != x)'

s = 'abc'
t = 'abc'

assert (s == s) true '(s == s)'
assert (s != t) false '(s != t)'

assert (s === s) true '(s === s)'
assert (s === t) false '(s === t)'
})