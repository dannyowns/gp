{
  primeCount = 0
  flags = (newArray 8190)
  fillArray (v flags) true
  i = 2
  repeat 8188 {
    if (at (v flags) (v i)) {
      log (v i) // i is prime
      primeCount += 1
      j = (2 * (v i))
      while (< (v j) 8190) { // mark multiples of i as non-prime
	atPut (v flags) (v j) false
	j += (v i)
      }
    }
    i += 1
  }
  log 'Found' (v primeCount) 'prime numbers'
}
