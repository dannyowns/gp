assert (isNil nil) true 'isNil nil'
assert (notNil nil) false 'notNil nil'
assert (isNil 'a') false  'isNil a'
assert (notNil 'a') true  'notNil a'
assert (isNil 42) false  'isNil 42'
assert (notNil 42) true  'notNil 42'

assert (max 1 2 4 3 2) 4 'max ints'
assert (max 'a' 'zz' 'zzb' 'c' 'nn' 'zzb') 'zzb' 'max strings'
assert (max  'zzb' 'a' 'zz' 'c' 'nn') 'zzb' 'max strings'

assert (min 1 2 4 3 2) 1 'min ints'
assert (min 'a' 'zz' 'zzb' 'c' 'nn' 'zzb') 'a' 'min strings'
assert (min 'zzb' 'zz' 'c' 'nn' 'a' )      'a' 'min strings'

//expect (min 42' 'zz' 'c' 'nn' 'a' ) error

assert (hasField (list) 'contents') true 'hasField true'
assert (hasField (list) 'foo') false 'hasField false'

assert (implements (list) 'count') true 'implements true'

// though it still understand it
assert (implements (list) 'classOf') false 'implements false'

