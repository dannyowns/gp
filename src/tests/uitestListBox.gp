// test and demonstrate list boxes

to demo {
  page = (newPage 800 700)
  open page

  cls = (listBox (classes) 'className' nil (color 230 230 255))
  setPosition (morph cls) 20 20
  addPart (morph page) (morph cls)

  fun = (listBox (functions) 'functionName')
  setPosition (morph fun) (+ (right (bounds (morph cls))) 20) 20
  addPart (morph page) (morph fun)

  txt = (newText (at (version) 1))
  setPosition (morph txt) (+ (right (bounds (morph fun))) 50) 20
  addPart (morph page) (morph txt)

  onSelect cls (action 'selectClass' fun txt)
  onSelect fun (action 'selectFunction' txt)

  startStepping page
}

to selectClass aListBox aText aClass {
  setCollection aListBox (methods aClass)
  setText aText (documentationFor (className aClass))
}

to selectFunction aText aFunction {
  pp = (new 'PrettyPrinter')
  br = '
'
  code = (join (functionName aFunction) ' ' (toString (argNames aFunction)) br br)
  nb = (cmdList aFunction)
  while (nb != nil) {
    code = (join code br (prettyPrint pp nb))
    nb = (nextBlock nb)
  }
  setText aText code
}

demo
