call (function {
a = (dictionary)

(atPut a 0 'a')
assert (at a 0) 'a' '(at a 0)'
assert (count a) 1 '(count a)'

(atPut a 8 'b')
assert (at a 8) 'b' '(at a 8)'
assert (count a) 2 '(count a)'

(atPut a 16 'c')
assert (at a 16) 'c' '(at a 16)'
assert (at a 0) 'a' '(at a 0)'
assert (count a) 3 '(count a)'

(atPut a 256 'd')
assert (at a 256) 'd' '(at a 256)'
assert (at a 0) 'a' '(at a 0)'
assert (at a 16) 'c' '(at a 16)'
assert (count a) 4 '(count a)'

(atPut a 256 'e')
assert (at a 256) 'e' '(at a 256)'
assert (at a 0) 'a' '(at a 0)'
assert (at a 16) 'c' '(at a 16)'
assert (count a) 4 '(count a)'

(remove a 16)
assert (at a 0) 'a' '(at a 0)'
assert (at a 16) nil '(at a 16)'
assert (at a 256) 'e' ''
assert (count a) 3 ''
})
