// test resizing a scrolled list box containing both strings and bitmaps

to demo {
  page = (newPage 800 800)
  open page

  clr = (color 220 220 255)
  lb = (listBox (functions) 'blockForFunction' 'nop' clr)
  sf = (scrollFrame lb clr)
  setPosition (morph sf) 50 50
  setExtent (morph sf) 100 300
  setGrabRule (morph sf) 'handle'
  addPart (morph page) (morph sf)
  resizeHandle sf

  startStepping page
}

demo
