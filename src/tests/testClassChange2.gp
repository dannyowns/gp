defineClass Class1 a b c

call (function {
  addField (class 'Class1') 'f'
  assert (fieldNames (class 'Class1')) (array 'a' 'b' 'c' 'f') 'added field f'

  cls = (class 'Class1')
  assert (fieldNames cls) (array 'a' 'b' 'c' 'f') 'check it with another reference to class'

  defineClass Class1 a b c f

  assert (fieldNames (class 'Class1')) (array 'a' 'b' 'c' 'f') 'deleted field f'
  assert (fieldNames cls) (array 'a' 'b' 'c' 'f') 'check it with another reference to class'
})
