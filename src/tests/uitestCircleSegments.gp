// test circle-top segments rendering
call (function {
openWindow
setGlobal 'flat' false
setGlobal 'flatBlocks' false

scale = 4
color = (color 100 50 200)
radius = (3 * scale)
dent = (2 * scale)
inset = (4 * scale)
border = 5

x = 100
w = 250
bm = (newBitmap 500 500)
drawBlock (newShapeMaker bm) x 50 w 100 color radius dent inset border
drawHatBlock (newShapeMaker bm) x 200 w 100 180 color radius dent inset border

drawBitmap nil bm
flipBuffer
sleep 2000
})
