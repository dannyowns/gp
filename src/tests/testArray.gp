call (function {

a = (newArray 10)

assert (count a) 10 '(count a)'

assert (at a 1) nil '(at a 1)'
assert (at a 2) nil '(at a 2)'

//assert (atPut a 1 'abc') 'abc' 'array: 4'
assert (atPut a 1 'abc') nil '(atPut a 1 abc)'
assert (at a 1) 'abc' '(at a 1)'

// expect (at a 11) error

a = (array 1 2 3 4 5)

assert (count a) 5 '(count a)'

assert (at a 1) 1 '(at a 1)'
assert (at a 5) 5 '(at a 5)'

//assert (atPut a 1 'def') 'abc' 'array: 9'
assert (atPut a 2 'def') nil '(atPut a 2 def)'
assert (at a 2) 'def' '(at a 2)'

// expect (at a 6) error
// expect (atPut a 6 'ghi') error

assert ((array 1 2 3) == (array 1 2 3)) true '=='
assert ((array 1 2 3) == 'foo') false '== not (other class)'
assert ((array 1 2 3) == (array 1 2 3 4)) false '== not (other count)'
assert ((array 1 2 3) == (array 1 5 3 )) false '== not (other element)'

assert ((array 1 2 (array 'foo' 'bar') 3) == (array 1 2 (array 'foo' 'bar') 3)) true '== (nested)'
assert ((array 1 2 (array 'foo' 'bar') 3) == (array 1 2 (array 'foo' 'bar' 'qux') 3)) false '== not (nested)'

assert ((array 1 2 3) != (array 1 4 3)) true '!='
assert ((array 1 2 3) != (array 1 2 3)) false '!= not'

assert (isEmpty (array)) true 'isEmpty'
assert (isEmpty (array 42 'foo' 'bar')) false 'isEmpty not'

assert (isClass (toList (array 42 'foo' 'bar')) 'List') true 'toList type'
assert (count (toList (array 42 'foo' 'bar'))) 3 'toList count'
assert (at (array 42 'foo' 'bar') 1) 42 'toList element 1'
assert (at (array 42 'foo' 'bar') 2) 'foo' 'toList element 2'
assert (at (array 42 'foo' 'bar') 3) 'bar' 'toList element 3'

assert (indexOf (array 'foo' 'bar' 'qux' 'garply') 'bar') 2 'indexOf (bar)'
assert (indexOf (array 'foo' 'bar' 'qux' 'garply') 'foo') 1 'indexOf (foo)'
assert (indexOf (array 'foo' 'bar' 'qux' 'garply') 'garply') 4 'indexOf (garply)'
assert (indexOf (array 'foo' 'bar' 'qux' 'garply') 'qux' 2) 3 'indexOf with start (qux)'
assert (indexOf (array 'foo' 'bar' 'qux' 'garply') 'qux' 4) nil 'indexOf with start after occurrence (qux)'
assert (indexOf (array 'foo' 'bar' 'qux' 'garply') 'waldo') nil 'indexOf (waldo)'

assert (lastIndexOf (array 'foo' 'bar' 'qux' 'foo' 'garply') 'garply') 5 'lastIndexOf (garply)'
assert (lastIndexOf (array 'foo' 'bar' 'qux' 'foo' 'garply') 'foo') 4 'lastIndexOf (foo)'
assert (lastIndexOf (array 'foo' 'bar' 'qux' 'foo' 'garply') 'foo' 3) 1 'lastIndexOf with start (foo)'
assert (lastIndexOf (array 'foo' 'bar' 'qux' 'foo' 'garply') 'garply' 3) nil 'lastIndexOf with start (garply)'
assert (lastIndexOf (array 'foo' 'bar' 'qux' 'foo' 'garply') 'waldo') nil 'lastIndexOf with start (waldo)'

assert (contains (array 42 'foo' 'bar') 'foo') true 'contains'
assert (contains (array 42 'foo' 'bar') 'qux') false 'contains not'

a = (array 1 2 3 4 5 6)
arrayShift a -2
assert a (array 3 4 5 6 nil nil) 'shift left 2'
arrayShift a -2
assert a (array 5 6 nil nil nil nil) 'shift left 4'
arrayShift a 2
assert a (array nil nil 5 6 nil nil) 'shift left 4, then right 2'

a = (array 1 2 3 4 5 6)
// expect (arrayShift a 7) error

a = (array 1 2 3 4 5 6)
replaceArrayRange a 1 3 a 3
assert a (array 3 4 5 4 5 6) 'replace left'

a = (array 1 2 3 4 5 6)
replaceArrayRange a 3 5 a 1
assert a (array 1 2 1 2 3 6) 'replace right'

a = (array 1 2 3 4 5 6)
replaceArrayRange a 3 3 a 1
assert a (array 1 2 1 4 5 6) 'replace 1 length'

a = (array 1 2 3 4 5 6)
replaceArrayRange a 3 2 a 1
assert a (array 1 2 3 4 5 6) 'replace 0 length'

assert (join (array 1 2 3) (array 4 5 6)) (array 1 2 3 4 5 6) 'join array'
assert (join (array 1 2 3) (list 4 5 6)) (array 1 2 3 4 5 6) 'join array and list'

assert (join (array 1 2 3) (array)) (array 1 2 3) 'join array and 0-array'

assert (toArray (flattened (array (array 1 2) (array 3 (array 4 '5'))))) (array 1 2 3 4 '5') 'flattened'

a = (array 1 2 3 4 5 6)
b = (copyArray a)
assert b a 'copyArray'

atPut a 3 -3

assert (join (array) (array 1 2 3)) (array 1 2 3) 'join array and 0-array'

assert b (array 1 2 3 4 5 6) 'copied array'
assert a (array 1 2 -3 4 5 6) 'mutated array'

a = (shuffled (range 1 999))
assert (isSorted (sorted a)) true 'array sort'

})