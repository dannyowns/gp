// test and demonstrate morphic stepping fps

defineClass Blinker morph

method init Blinker {
  morph = (newMorph this)
  setExtent morph 150 100
}

method redraw Blinker {
  setCostume morph (newBitmap (width (bounds morph)) (height (bounds morph)) (color))
}

method step Blinker {
  if (isVisible morph) {
    hide morph
  } else {
    show morph
  }
}

to fpsDemo {
  page = (newPage 500 300)
  open page

  bl = (new 'Blinker')
  init bl
  setCenter (morph bl) 250 150
  addPart page bl

  th = (newText 'value')
  setPosition (morph th) 240 50
  addPart page th

  sh = (slider 'horizontal' 400 (array (action 'setText' th) (action 'setFPS' (morph bl))))
  setPosition (morph sh) 50 20
  addPart page sh

  startStepping page
}

fpsDemo
