to numList {
    // Return a list of integers in reverse order
    result = (list)
    for i 65536 { add result (65537 - i) }
	return result
}

to binarySearch anArray k low high {
  comment '
	Use binary search to find k in the range [low..high] in anArray.
	Return the index of k, or 0 if k is not found.
	Assumes that anArray is sorted.'

  if (isNil low) { low = 1 }
  if (isNil high) { high = (count anArray) }
  while (low <= high) {
    i = ((low + high) / 2)
    el = (at anArray i)
    if (k == el) { return i }
    if (k > el) { low = (i + 1) } else { high = (i - 1) }
  }
  return 0
}

to dictionaryPerfTest d {
  timer = (newTimer)

  // adding
  for i 65536 { add d i }
  time1 = (msecSplit timer)

  // lookup
  if (isClass d 'List') {
	a = (toArray d)
    for i 65536 { binarySearch a i }
  } else {
    for i 65536 { at d i }
  }
  time2 = (msecSplit timer)

  // removing
  if (isClass d 'List') {
    for i 65536 { removeFirst d }
  } else {
    for i 65536 { remove d i }
  }
  time3 = (msecSplit timer)

  className = (getField (classOf d) 'className')
  print className 'adding:' time1 'lookup:' time2 'removing:' time3

  if (isClass d 'List') {
    d = (numList)
    reset timer
	sorted d
	time4 = (msecs timer)

    print '  List lookup assumes list is sorted. Sort time:' time4
  }
}

dictionaryPerfTest (dictionary)
d = (dictionary)
grow d 100000
dictionaryPerfTest d
dictionaryPerfTest (list)
//dictionaryPerfTest (newCDictionary)
