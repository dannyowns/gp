to testIterators data {
  assert (count (map (function n {return (n * n)}) data)) (count data) 'map result count'
  assert (at (map (function n {return (n * n)}) data) 1) 1 'map element 1'
  assert (at (map (function n {return (n * n)}) data) 2) 4 'map element 2'
  assert (at (map (function n {return (n * n)}) data) 3) 9 'map element 3'
  assert (at (map (function n {return (n * n)}) data) 4) 16 'map element 4'

  assert (count (filter (function n {return ((n % 2) == 0)}) data)) 5 'filter result count'
  assert (toArray (filter (function n {return ((n % 2) == 0)}) data)) (array 2 4 6 8 10) 'filter'

  assert (detect (function n {return (n > 5)}) data) 6 'detect'
  assert (detect (function n {return (n > 10)}) data 42) 42 'detect otherwise'

  assert (reduce '+' (array)) nil 'reduce empty array'
  assert (reduce '+' (array 1)) 1 'reduce single element array'
  assert (reduce '+' (array 1 2 3 4 5)) 15 'reduce +'
  assert (reduce (function a b {return (a + b)}) (array 1 2 3 4 5)) 15 'reduce (function a b {return (a + b)})'
  assert (reduce 'max' (array 3 5 7 42 8 9)) 42 'reduce max'
  assert (reduce (function a b {if (a > b) {return a}; return b}) (array 3 5 7 42 8 9)) 42 'reduce f(max)'
}

testIterators (array 1 2 3 4 5 6 7 8 9 10)
testIterators (list 1 2 3 4 5 6 7 8 9 10)
