call (function {
a = (list)

assert (isEmpty a) true 'isEmpty of empty list'
assert (notEmpty a) false 'notEmpty of empty list'

assert (count a) 0 'count of new list:'

(add a 2)
assert (count a) 1 'count of one element list:'
assert (at a 1) 2 'first element in one element list:'
assert (first a) 2 'first element in one element list:'
assert (last a) 2 'last element in one element list:'

assert (isEmpty a) false 'isEmpty of non-empty list'
assert (notEmpty a) true 'notEmpty of non-empty list'

(add a 4)
assert (count a) 2 'count of two element list:'
assert (at a 1) 2 'first element in two element list:'
assert (at a 2) 4 'second element in two element list:'
assert (first a) 2 'first element in two element list:'
assert (last a) 4 'last element in two element list:'

(add a 6)
assert (count a) 3 'count of three element list:'
assert (at a 1) 2 'first element in three element list:'
assert (at a 2) 4 'second element in three element list:'
assert (at a 3) 6 'third element in three element list:'
assert (first a) 2 'first element in three element list:'
assert (last a) 6 'last element in three element list:'

(add a 8)
assert (count a) 4 'count of four element list:'
assert (at a 1) 2 'first element in four element list:'
assert (at a 2) 4 'second element in four element list:'
assert (at a 3) 6 'third element in four element list:'
assert (at a 4) 8 'fourth element in four element list:'

(add a 10)
assert (count a) 5 'count of five element list:'
assert (at a 1) 2 'first element in five element list:'
assert (at a 2) 4 'second element in five element list:'
assert (at a 3) 6 'third element in five element list:'
assert (at a 4) 8 'fourth element in five element list:'
assert (at a 5) 10 'fifth element in five element list:'

(atPut a 2 3)
(atPut a 5 9)
assert (count a) 5 'count after mutation:'
assert (at a 1) 2 'first element after mutation:'
assert (at a 2) 3 'second element after mutation:'
assert (at a 3) 6 'third element after mutation:'
assert (at a 4) 8 'fourth element after mutation:'
assert (at a 5) 9 'fifth element after mutation:'


a = (list)

(add a 2)
(add a 4)
(add a 6)
(add a 8)

assert (removeFirst a) 2 'removeFirst return value'
assert (count a) 3 'remove one element'
assert (at a 1) 4 'first element after removal:'
assert (at a 2) 6 'second element after removal:'
assert (at a 3) 8 'third element after removal:'

(add a 10)
assert (count a) 4 'add to end'
assert (at a 1) 4 'first element (1):'
assert (at a 2) 6 'second element (1):'
assert (at a 3) 8 'third element (1):'
assert (at a 4) 10 'fourth element (1):'

a = (list 2 4 6 8)

assert (removeFirst a) 2 'removeFirst return value'
assert (count a) 3 'remove one element'
assert (at a 1) 4 'first element after removal:'
assert (at a 2) 6 'second element after removal:'
assert (at a 3) 8 'third element after removal:'

(add a 10)
assert (count a) 4 'add to end'
assert (at a 1) 4 'first element (2):'
assert (at a 2) 6 'second element (2):'
assert (at a 3) 8 'third element (2):'
assert (at a 4) 10 'fourth element (2):'

a = (list 2 4 6 8)

(addFirst a 10)
assert (count a) 5 'add to first'
assert (at a 1) 10 'first element after addFirst:'
assert (at a 2) 2 'second element addFirst:'
assert (at a 3) 4 'third element addFirst:'
assert (at a 4) 6 'fourth element addFirst:'
assert (at a 5) 8 'fourth element addFirst:'

a = (list 2 4 6 8)
b = (list 2 4 6 8)
addAll a b

assert (count a) 8 'addAll'

//----------------------------------

a = (list)
size = 32768

(add a 0)

for i (size - 1) {
  prev = (last a)
  (add a (((prev * 269) + 9601) % size))
}

assert (count a) size 'count of large collection'
assert (isSorted (toArray (sorted a))) true 'sort list'

//----------------------------------

to testListExtensions {
  // initializes samples
  l1 = (list)
  l2 = (list)
  l3 = (list)
  for i 5 {
    add l1 i
    add l2 i
    add l3 i
  }

  add l3 l1
  
  assert (l1 == l2) true '=='
  assert (l1 == (toList (array 1 2 3 4 5))) true '== (converted Array)'
  assert (l1 == (toList (array 1 2 3 4))) false '== not (other last)'
  assert (l1 == (toList (array 1 2 'foo' 4 5))) false '== not (other contents)'

  assert (l3 == (toList (array 1 2 3 4 5 (toList (array 1 2 3 4 5))))) true '== (nested)'
  assert (l3 == (toList (array 1 2 3 4 5 (toList (array 1 2 'foo' 4 5))))) false '== not (nested)'
  
  assert (l1 != l3) true '!='
  assert (l1 != l2) false '!= not'

  assert (isClass (toArray l1) 'Array') true 'toArray type'
  assert (toArray l1) (array 1 2 3 4 5) true 'toArray elements'

  assert (contains (toList (array 42 'foo' 'bar')) 'foo') true 'contains'
  assert (contains (toList (array 42 'foo' 'bar')) 'qux') false 'contains not'
  
  assert (indexOf (list 'foo' 'bar' 'qux' 'garply') 'bar') 2 'indexOf (bar)'
  assert (indexOf (list 'foo' 'bar' 'qux' 'garply') 'foo') 1 'indexOf (foo)'
  assert (indexOf (list 'foo' 'bar' 'qux' 'garply') 'garply') 4 'indexOf (garply)'
  assert (indexOf (list 'foo' 'bar' 'qux' 'garply') 'qux' 2) 3 'indexOf with start (qux)'
  assert (indexOf (list 'foo' 'bar' 'qux' 'garply') 'qux' 4) nil 'indexOf with start after occurrence (qux)'
  assert (indexOf (list 'foo' 'bar' 'qux' 'garply') 'waldo') nil 'indexOf (waldo)'

  assert (lastIndexOf (list 'foo' 'bar' 'qux' 'foo' 'garply') 'garply') 5 'lastIndexOf (garply)'
  assert (lastIndexOf (list 'foo' 'bar' 'qux' 'foo' 'garply') 'foo') 4 'lastIndexOf (foo)'
  assert (lastIndexOf (list 'foo' 'bar' 'qux' 'foo' 'garply') 'foo' 3) 1 'lastIndexOf with start (foo)'
  assert (lastIndexOf (list 'foo' 'bar' 'qux' 'foo' 'garply') 'garply' 3) nil 'lastIndexOf with start (garply)'
  assert (lastIndexOf (list 'foo' 'bar' 'qux' 'foo' 'garply') 'waldo') nil 'lastIndexOf with start (waldo)'

  l = (toList (array 'foo' 'bar' 'baz' 'qux' 'garply'))
  removeAt l 2
  assert l (toList (array 'foo' 'baz' 'qux' 'garply')) 'removeAt (2)'
  removeAt l 4
  assert l (toList (array 'foo' 'baz' 'qux')) 'removeAt (4)'
  removeAt l 1
  assert l (toList (array 'baz' 'qux')) 'removeAt (1)'

  l = (toList (array 'foo' 'bar' 'foo' 'baz' 'qux' 'foo' 'garply'))
  remove l 'qux'
  assert l (toList (array 'foo' 'bar' 'foo' 'baz' 'foo' 'garply')) 'remove (qux)'
  remove l 'foo'
  assert l (toList (array 'bar' 'baz' 'garply')) 'remove (foo - multiple occurrances)'
}

testListExtensions

// ---------------------------------
a = (list 2 4 6 8)

assert (toArray (copyFromTo a 3 4)) (array 6 8) 'list copy 6 8'
assert (toArray (copyFromTo a 1 0)) (array) 'list copy 0'
assert (toArray (copyFromTo a)) (array 2 4 6 8) 'list copy all'
//expect (copyFromTo a 2 5) error
//expect (copyFromTo a 1 6) error
//expect (copyFromTo a 4 6) error
})