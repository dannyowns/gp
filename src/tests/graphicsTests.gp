to drawSquares target {
  w = 50
  h = 50
  r = (color 200 20 20 255)
  g = (color 20 200 20 255)
  b = (color 20 20 200 255)
  fillRect target r 10 10 w h
  fillRect target g 20 20 w h
  fillRect target b 30 30 w h
  alpha = 100
  r = (color 200 20 20 alpha)
  g = (color 20 200 20 alpha)
  b = (color 20 20 200 alpha)
  fillRect target r 150 10 w h 1
  fillRect target g 160 20 w h 1
  fillRect target b 170 30 w h 1
}

to drawBitmaps target {
  bm = (newBitmap 100 100)
  for y 100 {
	for x 100 {
	  setPixel bm (x - 1) (y - 1) (color (2 * x) (2 * y) 0)
	}
  }
  drawBitmap target bm 10 90
  drawBitmap target bm 150 90 100
}

to drawStrings target {
  setFont 'Times New Roman Bold' 28
  bm = (newBitmap 100 100)
  r = (color 200 20 20)
  g = (color 20 200 20)
  b = (color 20 20 200)
  drawString bm 'Hello!' r
  drawBitmap target bm 10 200
  drawString bm 'Hello!' g
  drawBitmap target bm 20 210
  drawString bm 'Hello!' b
  drawBitmap target bm 30 220
  alpha = 100
  drawString bm 'Hello!' r
  drawBitmap target bm 150 200 alpha
  drawString bm 'Hello!' g
  drawBitmap target bm 160 210 alpha
  drawString bm 'Hello!' b
  drawBitmap target bm 170 220 alpha
}

to clearAll {
  clearBuffer
  flipBuffer
  clearBuffer
  flipBuffer
}

to testWin {
  clearBuffer
  drawSquares nil
  drawStrings nil
  drawBitmaps nil
  flipBuffer
}

to testBitmap {
  bm = (newBitmap 500 500)
  fill bm (color 255 255 255 255)
  drawSquares bm
  drawStrings bm
  drawBitmaps bm
  clearBuffer
  drawBitmap nil bm 0 0 255 1
  flipBuffer
}

to testTexture {
  t = (newTexture 500 500)
  fill t (color 255 255 255 255)
  drawSquares t
  drawStrings t
  drawBitmaps t
  clearBuffer
  showTexture nil t
  flipBuffer
}

to testTextureClipped {
  t = (newTexture 500 500)
  fill t (color 255 255 255 255)
  drawSquares t
  drawStrings t
  drawBitmaps t
  clearBuffer
  showTexture nil t 0 0 255 1 1 0 false 0 (rect 15.1 40 200.5 190)
  // args: dst src x y alpha xScale yScale rotationDegrees flip blendMode clipRect]
  flipBuffer
}

to testBitmapClipped {
  bm = (newBitmap 500 500)
  fill bm (color 255 255 255 255)
  drawSquares bm
  drawStrings bm
  drawBitmaps bm
  clearBuffer
  drawBitmap nil bm 0 0 255 1 (rect 15.1 40 200.5 190)
  flipBuffer
  sleep 3000
  bm2 = (newBitmap 500 500)
  drawBitmap bm2 bm 0 0 255 1 (rect 12.1 30 190.5 190)
  clearBuffer
  drawBitmap nil bm2 0 0 255 1
  flipBuffer
}

to testReadTexture {
  openWindow 100 100
  txt = (newTexture 4 4)
  bm = (newBitmap 4 4)
  fillRect txt (color 200 100 50 127) 0 0 2 2
  clearBuffer (gray 240)
  showTexture nil txt 50 50
  flipBuffer
  readTexture bm txt
  print (toArray (pixelData bm))
}

to testTextureTransforms {
  // mental model of what happens:
  //  1. place texture at coordinate
  //  2. scale it
  //  3. rotate it around the scaled version's geometric center
  //  4. clip it along an absolute rectangle
  // the surprise is the part about the scaled geometric center
  // remember that! ;-)
  openWindow
  t = (newTexture 100 100 (color))
  for i 24 {
    sc = (1 + ((i - 1) / 10))
    clip = (rect 100 100 (100 * sc) (100 * sc))
    showTexture nil t 100 100 20 sc sc ((i - 1) * 5) false nil clip
  }
  flipBuffer
}
