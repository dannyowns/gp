call (function {
b = (newBitmap 10 10)

assert (width b) 10 'width'
assert (height b) 10 'height'

c = (color 255 255 255 255)

fill b c

p = (getPixel b 5 5)
assert (red p) 255 'red full'
assert (green p) 255 'green full'
assert (blue p) 255  'blue full'
assert (alpha p) 255 'alpha full'

p = (setPixel b 5 5 (color 128 0 0 255))

p = (getPixel b 5 5)
assert (red p) 128   'red half'
assert (green p) 0   'green half'
assert (blue p) 0    'blue half'
assert (alpha p) 255 'alpha half'
})
