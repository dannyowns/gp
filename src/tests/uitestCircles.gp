to circles x y {
  pen = (newPen)
  drawLine pen x (y - 50) x (y + 50)
  drawLine pen (x - 50) y (x + 50) y
  r = 3
  repeat 20 {
    drawCircle pen x y r
    r += 2
  }
}

to filled x y {
  pen = (newPen)
  black = (color)
  white = (color 255 255 255)
  clr = black
  setColor pen clr
  r = 42
  repeat 40 {
    drawCircle pen x y r
    r += -1
    if (clr == black) {
      clr = white
    } else {
      clr = black
    }
    setColor pen clr
  }
  setColor pen black
  drawLine pen x (y - 50) x (y + 50)
  drawLine pen (x - 50) y (x + 50) y
}

to overlay x y r offset reversed {
  if (isNil reversed) {reversed = false}
  pen = (newPen)
  if reversed {
    drawCircle pen x y (r + offset)
    setColor pen (color 0 255 0)
    drawCircle pen x y r
  } else {
    drawCircle pen x y r
    setColor pen (color 0 255 0)
    drawCircle pen x y (r + offset)
  }
}

to rects x y {
  pen = (newPen)
  black = (color)
  white = (color 255 255 255)
  clr = black
  setColor pen clr
  s = 40
  r = 15
  repeat 8 {
    area = (rect (x - s) (y - s) (+ 1(s * 2)) (+ 1 (s * 2)))
    fillRoundedRect pen area r
    r += -2
    s += -5
    if (clr == black) {
      clr = white
    } else {
      clr = black
    }
    setColor pen clr
  }
  setColor pen black
  drawLine pen x (y - 50) x (y + 50)
  drawLine pen (x - 50) y (x + 50) y
}

to overlayRects x y s r clr {
  pen = (newPen)
  area = (rect (x - s) (y - s) (+ 1(s * 2)) (+ 1 (s * 2)))

  fillRect nil (color 0 255 0) (left area) (top area) (width area) (height area)
  setColor pen clr

  fillRoundedRect pen area r

  setColor pen (color 255 0 0)
  drawLine pen x (y - 50) x (y + 50)
  drawLine pen (x - 50) y (x + 50) y
}

to test {
  openWindow
  circles 100 100
  filled 200 100
  overlay 300 100 45 -1
  overlay 400 100 10 -1

  overlay 300 200 45 0 true
  overlay 400 200 10 0 true

  overlay 300 300 45 0
  overlay 400 300 10 0

  rects 100 200

  overlayRects 200 200 40 20 (color)
  overlayRects 200 200 10 7 (color 0 0 255)

  flipBuffer
}

test
sleep 2000
