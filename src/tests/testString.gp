call (function {
a = 'Maloney'
assert (byteCount a) 7
assert (byteAt a 1) 77 'M'
assert (byteAt a 2) 97 'a'
assert (byteAt a 3) 108 'l'
assert (byteAt a 4) 111 'o'
assert (byteAt a 5) 110 'n'
assert (byteAt a 6) 101 'e'
assert (byteAt a 7) 121 'y'

assert (count a) 7
assert (letters a) (array 'M' 'a' 'l' 'o' 'n' 'e' 'y') 'letters of Maloney'

b = 'Mönig'

assert (byteCount b) 6
assert (byteAt b 1) 77 'M'
assert (byteAt b 2) 195 'ö 1'
assert (byteAt b 3) 182 'ö 2'
assert (byteAt b 4) 110 'n'
assert (byteAt b 5) 105 'i'
assert (byteAt b 6) 103 'g'

assert (count b) 5
assert (letters b) (array 'M' 'ö' 'n' 'i' 'g') 'letters of Mönig'
assert (at (letters b) 1) 'M' 'first letter of Mönig'
assert (at (letters b) 2) 'ö' 'second letter of Mönig'

c = '大島'

assert (byteCount c) 6
assert (byteAt c 1) 229 '大 1'
assert (byteAt c 2) 164 '大 2'
assert (byteAt c 3) 167 '大 3'
assert (byteAt c 4) 229 '島 1'
assert (byteAt c 5) 179 '島 2'
assert (byteAt c 6) 182 '島 3'

assert (count c) 2
assert (letters c) (array '大' '島') 'letters of 大島'
assert (at (letters c) 1) '大' 'first letter of 大島'
assert (at (letters c) 2) '島' 'last letter of 大島'

// ---------------------------------------------

code = (codePoints a)
assert (count code) 7 'Maloney count'
str = (stringFromCodePoints (array 77 97 108 111 110 101 121))
assert str a 'newString for Maloney'

code = (codePoints b)
assert (count code) 5 'Mönig count'
str = (stringFromCodePoints (array 77 246 110 105 103))
assert str b 'newString for Mönig'

code = (codePoints c)
assert (count code) 2 '大島 count'
str = (stringFromCodePoints (array 22823 23798))
assert str c 'newString for 大島'

// ---------------------------------------------

str = (join a ' ' b ' ' c)

assert str 'Maloney Mönig 大島' 'join'

str = (joinStringArray (array a ' ' b ' ' c))
assert str 'Maloney Mönig 大島' 'joinStringArray'

str2 = (letters str)
assert (count str2) 16 'letters count'
assert (classOf (at str2 1)) (class 'String') 'class in letters 1'
assert (classOf (at str2 16)) (class 'String') 'class in letters 16'

assert (isDigit '0') true '0 is a digit'
assert (isDigit '3') true '3 is a digit'
assert (isDigit '9') true '9 is a digit'
assert (isDigit 'a') false 'a not a digit'
assert (isLetter 'z') true 'z is a letter'
assert (isLetter '0') false '0 is not a letter'

assert (toInteger '0') 0 'read integer 0'
assert (toInteger '22823') 22823 'read integer 22823'
assert (toInteger '-22823') -22823 'read integer -22823'
assert (toInteger '-0') 0 'read integer -0'

assert (printString 'abc') '''abc''' 'printString abc'
assert (printString 'a''bc') '''a''''bc''' 'printString a'bc'

assert (beginsWith 'abc' 'ab') true 'beginsWith abc ab'
assert (beginsWith 'abc' 'abc') true 'beginsWith abc abc'
assert (beginsWith 'ab' 'abc') false 'beginsWith ab abc'

assert (splitWith 'aa%bb%cc' '%') (array 'aa' 'bb' 'cc')  'splitWith 1'
assert (splitWith '%aa%bb%cc' '%') (array '' 'aa' 'bb' 'cc')  'splitWith 2'
assert (splitWith '%aa%bb%cc%' '%') (array '' 'aa' 'bb' 'cc' '')  'splitWith 3'
assert (splitWith '%aa%%bb%cc%' '%') (array '' 'aa' '' 'bb' 'cc' '')  'splitWith 4'
assert (splitWith '%%aa%bb%cc%' '%') (array '' '' 'aa' 'bb' 'cc' '')  'splitWith 5'
assert (splitWith '%%aa%bb%%cc%%' '%') (array '' '' 'aa' 'bb' '' 'cc' '' '' )  'splitWith 6'

assert (format '%aa%bb%cc%' 'w' 'x' 3 'z') 'waaxbb3ccz'  'format 1'
assert (format '%aa%bb%cc' 'x' 2 'z') 'xaa2bbzcc'  'format 2'
assert (format 'aa%bb%cc%' 1 2 'z') 'aa1bb2ccz'  'format 3'
})
