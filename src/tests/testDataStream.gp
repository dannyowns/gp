call (function {

  a = (dataStream (newBinaryData 10))
  putUInt8 a 1
  putUInt8 a 2
  putUInt8 a 3
  putUInt8 a 4
  putUInt8 a 5
  putUInt8 a 6
  putUInt8 a 7
  putUInt8 a 8
  putUInt8 a 9
  putUInt8 a 10

  assert (byteCount (getField a 'data')) 10
  assert (getField a 'position') 10

  putUInt8 a 11

  assert (byteCount (getField a 'data')) 20
  assert (getField a 'position') 11

  data = (newBinaryData 9)
  for i 9 {
    byteAtPut data i i
  }

  nextPutAll a data 1 9

  assert (byteCount (getField a 'data')) 20
  assert (getField a 'position') 20

  assert (byteAt (getField a 'data') 20) 9

  for i 9 {
    byteAtPut data i (i + 10)
  }

  nextPutAll a data 1 9

  assert ((byteCount (getField a 'data')) > 20) true
  assert (getField a 'position') 29

  assert (byteAt (getField a 'data') 29) 19
})

