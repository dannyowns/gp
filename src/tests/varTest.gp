defineClass VarTest fieldVar

method countUsingField VarTest {
  fieldVar = 0
  repeat 10000000 { fieldVar += 1 }
}

method countUsingLocal VarTest {
  localVar = 0
  repeat 10000000 { localVar += 1 }
}

{
  timer = (newTimer)
  countUsingField (new 'VarTest')
  log 'field var:' (msecs timer) 'msecs'

  timer = (newTimer)
  countUsingLocal (new 'VarTest')
  log 'local var:' (msecs timer) 'msecs'

  timer = (newTimer)
  count = 0
  repeat 10000000 { count += 1 }
  log 'context var:' (msecs timer) 'msecs'
}
