// test resizing a scrolled tree list boxes

defineClass Frob name subs

method name Frob {return name}
method subs Frob {return subs}
method info Frob {return (count subs)}
method printName Frob {print name}
method informName Frob {inform (global 'page') name}

to frob name subs... {
  f = (new 'Frob' name)
  s = (list)
  for i ((argCount) - 1) {
    add s (arg (i + 1))
  }
  setField f 'subs' s
  return f
}

to demo {
  page = (newPage 800 800)
  open page

  clr1 = (color 220 220 255)
  clr2 = (color 255 255 255)

  dta = (frob 'GP'
    (frob 'activities'
      (frob 'authoring')
      (frob 'extending')
    )
    (frob 'components'
      (frob 'blocks'
        (frob 'commands')
        (frob 'reporters')
        (frob 'event hats')
      )
      (frob 'slots'
        (frob 'strings')
        (frob 'numbers')
        (frob 'Booleans')
        (frob 'variables')
        (frob 'object values')
        (frob 'drop-down menus')
        (frob 'variadic inputs')
      )
      (frob 'panes')
      (frob 'text code')
    )
      (frob 'precursors'
        (frob 'LOGO'
          (frob 'LISP')
          (frob 'FORTRAN')
          (frob 'APL')
        )
        (frob 'ETOYS'
          (frob 'Squeak')
          (frob 'Nassi/Shneiderman')
        )
        (frob 'Scratch')
        (frob 'Snap/BYOB')
        (frob 'KScript')
      )
      (frob 'big ideas'
        (frob 'Constructionism'
          (frob 'assimilation')
          (frob 'accomodation')
          (frob 'public artefacts')
        )
        (frob 'maker movement'
          (frob 'fostering creativity')
          (frob 'broading participation in STEM'
            (frob 'females')
            (frob 'underrepresented minorites')
            (frob 'children')
          )
        )
        (frob 'coding movement')
      )
      (frob 'creators'
          (frob 'John Maloney')
          (frob 'Yoshiki Ohshima')
          (frob 'Jens Mönig')
      )
    )

  tree = (treeBox nil dta 'name' 'subs' 'printName' clr1 'informName' 'info') // show root and hover-hints
  sf = (scrollFrame tree clr1)
  setPosition (morph sf) 50 50
  setExtent (morph sf) 100 100
  setGrabRule (morph sf) 'handle'
  addPart (morph page) (morph sf)
  resizeHandle sf

  tree = (treeBox nil dta 'name' 'subs' 'printName' clr2 'informName' nil true) // hide root, don't show hints
  sf = (scrollFrame tree clr2)
  setPosition (morph sf) 50 300
  setExtent (morph sf) 100 150
  setGrabRule (morph sf) 'handle'
  addPart (morph page) (morph sf)
  resizeHandle sf

  startStepping page
}

demo
