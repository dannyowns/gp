call (function {
to foo {
  return ((arg 1) - (arg 2))
}

assert (call 'foo' 3 4) -1

to bar {
   return ((arg 1) * (call 'foo' (arg 2) (arg 3)))
}

assert (call 'bar' 3 4 5) -3

assert (call '*' 3 4 5) 60
})
