// Morphic scroll frame demo

to scrollDemo {

  str = 'A programming genius called Hugh
Said "I really must see if it''s true."
So he wrote a routine
To ask "What''s it all mean?"
But the answer was still "42".

A programming genius called HEAP
Had trouble in getting to sleep
So he made his lambs troup
through a huge FOR-NEXT loop
FOR 1 TO 10000: NEXT sheep.

if(computer.fail==true){
background.setColor(blue);
user.frown();
sys.shutdown();
user.scream("OH, F-- YOU");}'


  page = (newPage 700 700)
  open page

  t = (newText str 'Arial' 24)
  setColor t nil nil (color 200 200 255)
  sf = (scrollFrame t (color 255 200 200))
  setPosition (morph sf) 50 50
  setExtent (morph sf) 200 200
  resizeHandle sf
  addPart (morph page) (morph sf)

  startStepping page
}

scrollDemo