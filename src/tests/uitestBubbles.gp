// test drawing speech triangles

to bubble {
  openWindow 300 300
  bm = (newBitmap 300 300)
  scale = 3

  fillRect nil (color 100 255 255) 10 10 300 180
  drawSpeechBubble (newShapeMaker bm) (rect 10 10 100 60) scale

  fillRect nil (color 100 255 255) 10 100 300 180
  drawSpeechBubble (newShapeMaker bm) (rect 110 100 100 60) scale 'right'

  drawBitmap nil bm
  flipBuffer
}

bubble
sleep 2000
