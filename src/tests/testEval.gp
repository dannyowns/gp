to foo {
  return ((arg 1) - (arg 2))
}

assert (eval 'return (foo 3 4)') -1 'eval 1a'
assert (eval '(foo 3 4)') -1 'eval 1b'

to bar {
   return ((arg 1) * (call 'foo' (arg 2) (arg 3)))
}

assert (eval 'return (bar 3 4 5)') -3 'eval 2a'
assert (eval '(bar 3 4 5)') -3 'eval 2b'
