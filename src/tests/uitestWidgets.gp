// test and demonstrate pushbuttons, togglebuttons and fancy actions

defineClass Switch state
method switch Switch {state = (not state)}
method setState Switch bool {state = bool}
method state Switch {return state}

to buttonDemo {
  page = (newPage 400 150)
  open page

  s = (new 'Switch' false)

  t = (toggleButton (action 'switch' s) (action 'state' s))
  setPosition (morph t) 250 55
  addPart (morph page) (morph t)

  b = (pushButton 'on' (color 130 130 130) (array (action 'setState' s true) (action 'refresh' t)) 60 40)
  setPosition (morph b) 50 50
  addPart (morph page) (morph b)

  b = (pushButton 'off' (color 130 130 130) (array (action 'setState' s false) (action 'refresh' t)) 60 40)
  setPosition (morph b) 150 50
  addPart (morph page) (morph b)

  startStepping page
}

buttonDemo
