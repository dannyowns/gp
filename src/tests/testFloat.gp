assert 0.0 0 '0'
assert (toFloat 10) 10.0 'toFloat 10'
assert (toFloat -10) -10.0 'toFloat 10'
assert (truncate -10.5) -10 'truncate negative'
assert (truncate 10.5) 10 'truncate positive'
assert (truncate 0.5) 0 'truncate positive'
assert (truncate -0.5) 0 'truncate negative'

assert (classOf (toFloat 10)) (class 'Float') 'class toFloat'

assert (1.0 + 1.0) (toFloat 2) '1.0 + 1.0'
assert (+ 1.0 1.0) (toFloat 2) '+ 1.0 1.0'
assert (+ 1.5 1.25) 2.75 '2.75'

assert ((toFloat 20) + 30) 50.0 '20.0 + 30'

assert (20 + (toFloat 30)) 50.0 '20 + 30.0'
assert (+ 20 (toFloat 30) 2.5 3) 55.5 '+ 20 30.0 2.5 3'
assert (20.0 - (toFloat 30)) -10.0 '20 - 30.0'
assert (20 - (toFloat 30)) -10.0 '20 - 30.0'
// expect (- 20 (toFloat 30) 2.5 3) error '- 20 30.0 2.5 3'
assert (20.0 * 30) 600.0 '20.0 * 30'
assert (* 20.0 30 2.5 -3) -4500.0 '* 20.0 30 2.5 -3'
assert (30 / 4) 7.5 '30 / 4'
assert (truncate (30 / 4)) 7 '(truncate (30 / 4))'
assert (30.0 / 4) 7.5 '30.0 / 4'
assert (30 / 4.0) 7.5 '30 / 4.0'

assert (((pi) * 1000000000) > 3141592653.0) true 'compare'
assert (truncate ((pi) * 10000000)) 31415926 'truncate'
assert (((sin 90) * 1000000000.0) > 999999999.0) true 'sin lower'
assert (((sin 90) * 1000000000.0) <= 1000000000.0) true 'sin upper'

assert (((cos 180) * 1000000000) < -999999999.0) true 'cos lower'
assert (((cos 180) * 1000000000) >= -1000000000.0) true 'cos higher'

assert (sqrt 1) 1 'sqrt 1'
assert (sqrt 0.25) 0.5 'sqrt 0.25'
assert ((sqrt 10) > 3.1622776601) true 'sqrt 10 lower'
assert (((sqrt 10) * 100000000) < 316227767) true 'sqrt 10 upper'

assert (20 > 30) false '20 > 30'
assert (20.0 > 30.0) false '20.0 > 30.0'
assert (20.0 > 30) false '20.0 > 30'
assert (20 > 30.0) false '20 > 30.0'

assert (20 < 30) true '20 < 30'
assert (20.0 < 30.0) true '20.0 < 30.0'
assert (20.0 < 30) true '20.0 < 30'
assert (20 < 30.0) true '20 < 30.0'

assert ((toFloat -30) == (toFloat -30)) true '-30.0 == -30.0'
assert (-30 == (toFloat -30)) true '-30.0 == -30.0'

assert (fractionPart 1.5) 0.5
assert (fractionPart -1.5) -0.5
assert (fractionPart 1073741823.5) 0.5
assert (fractionPart 1073741824.5) 0.5
assert (fractionPart -1073741824.5) -0.5
assert (fractionPart -1073741825.5) -0.5

assert (integerPart 1.5) 1
assert (integerPart -1.5) -1
assert (integerPart 1073741823.5) 1073741823
assert (integerPart 1073741824.5) 1073741824.0
assert (integerPart -1073741824.5) -1073741824
assert (integerPart -1073741825.5) -1073741825.0

assert (ceiling 1.5) 2
assert (ceiling -1.5) -1
assert (ceiling 1073741822.5) 1073741823
assert (ceiling 1073741823.5) 1073741824.0
assert (ceiling -1073741824.5) -1073741824
assert (ceiling -1073741825.5) -1073741825.0

assert (floor 1.5) 1
assert (floor -1.5) -2
assert (floor 1073741823.5) 1073741823
assert (floor 1073741824.5) 1073741824.0
assert (floor -1073741823.5) -1073741824
assert (floor -1073741824.5) -1073741825.0

assert (round 1.49) 1
assert (round -1.49) -1
assert (round 1.5) 2
assert (round -1.5) -2

assert (round 1073741823.49) 1073741823
assert (round 1073741824.49) 1073741824.0
assert (round -1073741824.49) -1073741824
assert (round -1073741825.49) -1073741825.0

assert (round 1073741822.5) 1073741823
assert (round 1073741823.5) 1073741824.0
assert (round -1073741823.5) -1073741824
assert (round -1073741824.5) -1073741825.0

to testIncrement {
  n = 0.0
  n += 1.5
  assert n 1.5 'float += float'

  n = 1
  n += 1.5
  assert n 2.5 'int += float'

  n = 2.5
  n += 1
  assert n 3.5 'float += int'
}

testIncrement
