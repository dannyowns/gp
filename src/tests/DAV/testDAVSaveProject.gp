call (function {
  m = (loadModule 'modules/DAVDirectory.gpm')

  u = (new (at m 'URL'))
  setScheme u 'http'
  setHost u '127.0.0.1'
  setPort u 8000
  setPath u (array 'ohshima' 'proj1.gpp')

  setShared 'username' 'ohshima' (sessionModule)

  p = (emptyProject)

  saveProject p (toString u)
})

