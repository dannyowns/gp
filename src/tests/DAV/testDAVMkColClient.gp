call (function {
  m = (loadModule 'modules/DAVDirectory.gpm')

  u = (new (at m 'URL'))
  setScheme u 'http'
  setHost u 'tinlizzie.org'
  setPort u 8000
  setPath u (array 'yoshiki')

  d = (new (at m 'DAVClient'))
  openURL d u
  mkcol d
})
