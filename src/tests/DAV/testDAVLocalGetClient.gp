call (function {
  m = (loadModule 'modules/DAVDirectory.gpm')

  u = (new (at m 'URL'))
  setScheme u 'http'
  setHost u '127.0.0.1'
  setPort u 8000
  setPath u (array 'ohshima' 'xdvi-env')

  d = (new (at m 'DAVClient'))
  setUser d 'user'
  setPassword d 'gpgp'
  openURL d u
  print (get d)
})

