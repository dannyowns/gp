call (function {
a = (dictionary)

obj1 = 'abc'
obj2 = 'abc'
obj3 = 'def'

obj4 = (dictionary)
obj5 = (array 1 2)
obj6 = (new 'Rectangle' 1 2 3 4)

(atPut a obj1 1)
assert (at a obj1) 1 'at a obj1'
assert (at a obj2) 1 'at a obj2'

(atPut a obj2 2)
assert (at a obj1) 2 'overwrite obj1'
assert (at a obj2) 2 'overwrite obj2'

(atPut a obj3 3)
assert (at a obj1) 2 'overwrite obj1'
assert (at a obj3) 3 'at a obj3'

(atPut a obj4 4)
assert (at a obj1) 2 'overwrite obj1'
assert (at a obj3) 3 'at a obj3'
assert (at a obj4) 4 'at a obj4'

(atPut a obj5 5)
assert (at a obj1) 2 'overwrite obj1'
assert (at a obj3) 3 'at a obj3'
assert (at a obj4) 4 'at a obj4'
assert (at a obj5) 5 'at a obj4'

(atPut a obj6 6)
assert (at a obj1) 2 'overwrite obj1'
assert (at a obj3) 3 'at a obj3'
assert (at a obj4) 4 'at a obj4'
assert (at a obj5) 5 'at a obj5'
assert (at a obj6) 6 'at a obj6'

(remove a obj1)
assert (at a obj1) nil 'obj1 removed'
assert (at a obj2) nil 'obj2 also removed'

(remove a obj6)
assert (at a obj6) nil 'obj1 removed'
})
