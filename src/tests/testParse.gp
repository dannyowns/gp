call (function {
str = 'to foo {
  return 5
}
to bar a b {
  c = (a + b)
  return (c * 3)
}'

expressions = (parse str)
assert (count expressions) 2 'parse count'

cmd = (at expressions 1)
assert (classOf cmd) (class 'Reporter') 'parse 1'
assert (primName cmd) 'to' 'op 1'
assert (at (argList cmd) 1) 'foo' 'arg 1 of cmd 1'
body = (last (argList cmd))
assert (classOf body) (class 'Command') 'body of cmd 1'
assert (primName body) 'return' 'body op of cmd 1'

cmd = (at expressions 2)
assert (classOf cmd) (class 'Reporter') 'parse 2'
assert (primName cmd) 'to' 'op 2'
assert (at (argList cmd) 1) 'bar' 'arg 1 of cmd 2'
assert (at (argList cmd) 2) 'a' 'arg 1 of cmd 2'
assert (at (argList cmd) 3) 'b' 'arg 2 of cmd 2'
body = (last (argList cmd))
assert (classOf body) (class 'Command') 'body of cmd 2'
assert (primName body) '=' 'body op of cmd 2'

cmd = (first (parse '1 + 2'))
assert (classOf cmd) (class 'Reporter') 'parse 3'
assert (eval cmd) 3 'eval 3'

cmd = (first (parse '(1 + 2)'))
assert (classOf cmd) (class 'Reporter') 'parse 4'
assert (eval cmd) 3 'eval 4'

cmd = (first (parse '(1 + 2) + 3'))
assert (classOf cmd) (class 'Reporter') 'parse 5'
assert (eval cmd) 6 'eval 5'

cmd = (first (parse 'print 1'))
assert (classOf cmd) (class 'Reporter') 'parse 6'

cmd = (first (parse '{print 1}'))
assert (classOf cmd) (class 'Command') 'parse 7'

cmd = (first (parse 'print 1; print 2'))
assert (classOf cmd) (class 'Command') 'parse 8'

})
