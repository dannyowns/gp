// test drawing isosceles triangles

to isosceles {
  side = 100
  margin = 10
  dim = ((side * 2) + (margin * 3))
  openWindow dim dim
  clearBuffer (gray 255)
  yellow = (color 255 255 0)
  black = (color)
  pen = (newPen)

  setColor pen yellow
  fillRect pen margin margin side side
  setColor pen black
  fillIsoscelesTriangle pen (insetBy (rect margin margin side side) 1) 'right'

  setColor pen yellow
  fillRect pen ((margin * 2) + side) margin side side
  setColor pen black
  fillIsoscelesTriangle pen (insetBy (rect ((margin * 2) + side) margin side side) 1) 'down'

  setColor pen yellow
  fillRect pen margin ((margin * 2) + side) side side
  setColor pen black
  fillIsoscelesTriangle pen (insetBy (rect margin ((margin * 2) + side) side side) 1) 'up'

  setColor pen yellow
  fillRect pen ((margin * 2) + side) ((margin * 2) + side) side side
  setColor pen black
  fillIsoscelesTriangle pen (insetBy (rect ((margin * 2) + side) ((margin * 2) + side) side side) 1) 'left'

  flipBuffer
}

to corners {
  side = 100
  s2 = (side / 2)
  margin = 10
  dim = ((side * 2) + (margin * 3))
  openWindow dim dim
  clearBuffer (gray 255)
  yellow = (color 255 255 0)
  black = (color)
  pen = (newPen)

  setColor pen yellow
  fillRect pen margin margin side side
  setColor pen black
  fillBottomLeftCorner pen (insetBy (rect margin margin side side) 1)

  setColor pen yellow
  fillRect pen ((margin * 2) + side) margin side side
  setColor pen black
  fillTopRightCorner pen (insetBy (rect ((margin * 2) + side) margin side side) 1)

  setColor pen yellow
  fillRect pen margin ((margin * 2) + side) side side
  setColor pen black
  fillBottomRightCorner pen (insetBy (rect margin ((margin * 2) + side) side side) 1)

  setColor pen yellow
  fillRect pen ((margin * 2) + side) ((margin * 2) + side) side side
  setColor pen black
  fillTopLeftCorner pen (insetBy (rect ((margin * 2) + side) ((margin * 2) + side) side side) 1)

  flipBuffer
}

// test drawing flat top triangles

to triangles {
  openWindow 200 300
  clearBuffer (gray 255)
  yellow = (color 255 255 0)
  black = (color)
  pen = (newPen)

  setColor pen yellow
  fillRect pen 10 10 100 100
  setColor pen black
  fillFlatTopTriangle pen 11 109 11 20 109

  setColor pen yellow
  fillRect pen 10 120 100 100
  setColor pen black
  fillFlatBottomTriangle pen 20 121 11 109 219

  flipBuffer
}


corners
sleep 1000
isosceles
sleep 1000
triangles
sleep 1000
