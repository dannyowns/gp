to emptyLoopTest {
  timer = (newTimer)
  repeat 100000000 { noop }
  log (msecs timer) 'msecs'
}

emptyLoopTest
