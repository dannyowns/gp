call (function {
p = (new 'PrettyPrinter' )

str = 'to foo a b {return (+ 3 4)}'

assert (prettyPrintString p str) 'to foo a b {
  return (3 + 4)
}
'

str = 'defineClass AAA a b'

assert (prettyPrintString p str) 'defineClass AAA a b
'

str = 'method foo AAA a b {
  c = (a * b)
    return (3 + 4)
}'

assert (prettyPrintString p str) 'method foo AAA a b {
  c = (a * b)
  return (3 + 4)
}
'

str = 'for a (+ 3 4) {
print a}'

assert (prettyPrintString p str) 'for a (3 + 4) {
  print a
}
'

str = 'for a... (+ 3 4) {
print a...}'

assert (prettyPrintString p str) 'for a... (3 + 4) {
  print a...
}
'


str = 'while true {
  if (test == 123) {
    return nil
  }
  print a...}'

assert (prettyPrintString p str) 'while true {
  if (test == 123) {return nil}
  print a...
}
'

str = 'while true {
  if (test == 123) {
    return nil
  }
  if (test2 != ''abc'') {
    return (3 + 4)
  }
  print a...}'

assert (prettyPrintString p str) 'while true {
  if (test == 123) {return nil}
  if (test2 != ''abc'') {return (3 + 4)}
  print a...
}
'

str = 'while true {
  if (test == 123) {
    return nil
  }
  xyx = 4
  if (test2 != ''abc'') {
    return (3 + 4)
  }
  print a...}'

assert (prettyPrintString p str) 'while true {
  if (test == 123) {return nil}
  xyx = 4
  if (test2 != ''abc'') {
    return (3 + 4)
  }
  print a...
}
'

cmd = (parse 'while true {
  if (test == 123) {return nil}
  xyx = 4
  if (test2 != ''abc'') {
    return (3 + 4)
  }
  print a...
}')

cmd = (getField (at cmd 1) (count (at cmd 1)))

assert (prettyPrintList p cmd) 'if (test == 123) {return nil}
xyx = 4
if (test2 != ''abc'') {
  return (3 + 4)
}
print a...
'

})