// test and demonstrate speech bubbles

to bubbleDemo {
  setGlobal 'scale' 2
  page = (newPage 400 300)
  open page

  cr = (string 13)

  b = (newBubble (join 'tri-tra-trallala!' cr 'der Kasperle ist' cr 'wieder da') 'right' 3)
  setPosition (morph b) 50 20
  addPart page b

  b = (newBubble (join 'ene-mene-miste,' cr 'es rappelt in der Kiste!') 'left' 3)
  setPosition (morph b) 160 140
  addPart page b

  startStepping page
}

bubbleDemo
