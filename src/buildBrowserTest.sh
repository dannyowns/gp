#!/bin/sh

# copy startup and library files
rm -rf runtime
mkdir runtime
cp browserTestStartup.gp runtime/startup.gp
cp -r ../runtime/lib runtime

emcc -std=gnu99 -Wall -O3 \
-D EMSCRIPTEN \
-D NO_SDL \
-D NO_SOCKETS \
-D SHA2_USE_INTTYPES_H \
-I jpeg \
-s USE_ZLIB=1 \
-s TOTAL_MEMORY=117440512 \
-s ALLOW_MEMORY_GROWTH=0 \
--memory-init-file 0 \
-s WASM=1 \
browserPrims.c cache.c dict.c embeddedFS.c events.c gp.c interp.c jpegPrims.c mem.c memGC.c \
oop.c parse.c prims.c serialPortPrims.c sha1.c sha2.c soundPrims.c textAndFontPrims.c vectorPrims.c \
jpeg/jpeglib.bc \
--preload-file runtime \
-o gp_wasm.html

# we use gp.html so can delete gp_wasm.html
rm gp_wasm.html

# remove library files
rm -rf runtime
