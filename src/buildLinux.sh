#!/bin/sh
# Note: Installed packages for cairo, pango, and gtk but not sure which ones were essential
# pango1.0-dev, libgtk-3-dev (not sure about cairo)

gcc -std=gnu99 -m32 -Wall -Wno-strict-aliasing -O3 \
  -I/usr/local/include/SDL2 -Ijpeg \
  `pkg-config --cflags pangocairo` \
  cache.c dict.c embeddedFS.c events.c gp.c graphicsPrims.c interp.c jpegPrims.c mem.c memGC.c oop.c \
  parse.c prims.c serialPortPrims.c sha1.c sha2.c socketPrims.c soundPrims.c textAndFontPrims.c vectorPrims.c \
  /usr/local/lib/libSDL2.a \
  `pkg-config --libs pangocairo` \
  /usr/local/lib/libjpeg.a \
  /usr/local/lib/libportaudio.a -lasound \
  -lm -lpthread -ldl -lz \
  -o gp-linux32bit

