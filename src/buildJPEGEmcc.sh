#!/bin/sh
#
# To build jpeglib for Emscripten, do the following in the jpeglib source folder:
#
#   1. emconfigure ./configure
#   2. Edit the generated "Makefile" to set CFLAGS and CPPFLAGS to "-O3"
#      (I don't think  CPPFLAGS is actually used, but it can't hurt.)
#   3. emmake make
#      (The warnings about libjpeg.9.dylib in the CCLD commands are harmless.)
#   4. Run this script in the jpeglib folder
#   5. Move jpeglib.bc into your build folder and include it in your emcc command.
#
# Note: The same optimization flag (e.g. -O3) should be used for all steps.
# Note: The list of .o files is derived from LIBSOURCES in the generated "Makefile".

emcc -O3 -r -s WASM=1 \
	jaricom.o jcapimin.o jcapistd.o jcarith.o jccoefct.o jccolor.o \
        jcdctmgr.o jchuff.o jcinit.o jcmainct.o jcmarker.o jcmaster.o \
        jcomapi.o jcparam.o jcprepct.o jcsample.o jctrans.o jdapimin.o \
        jdapistd.o jdarith.o jdatadst.o jdatasrc.o jdcoefct.o jdcolor.o \
        jddctmgr.o jdhuff.o jdinput.o jdmainct.o jdmarker.o jdmaster.o \
        jdmerge.o jdpostct.o jdsample.o jdtrans.o jerror.o jfdctflt.o \
        jfdctfst.o jfdctint.o jidctflt.o jidctfst.o jidctint.o jquant1.o \
        jquant2.o jutils.o jmemmgr.o jmemnobs.o \
	-o jpeglib.bc
