#!/bin/sh

ARGS=""
INTERACTIVE=""

cd ..

for var in "$@"; do
        if [ ${var} = '-' ]; then
	    INTERACTIVE='-'
	else
	    ARGS=${ARGS}`echo src/tests/*${var}*.gp`" "
	fi
done

if [ -z "$ARGS" ]
then
	ARGS=src/tests/test?*.gp
fi

for t in $ARGS; do
	echo $t
	./mac_gp runtime/lib/*.gp $t $INTERACTIVE
done
