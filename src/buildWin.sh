#!/bin/sh
# Build GP for Windows with statically-linked SDL2
#
# To create a statically linked version of the portaudio library
# run the following commands in the portaudio folder:
#
#   CFLAGS="-m32 -static" CXXFLAGS="-m32 -static" LDFLAGS="-m32 -static" ./configure
#   CFLAGS="-m32 -static" CXXFLAGS="-m32 -static" LDFLAGS="-m32 -static" make
#
# Then copy the resulting libportaudio.a library from portaudio/lib/.libs into
# the GP build folder and run this build script.
#
# To check the DLL dependencies use:
#
#   objdump -p gp.exe | grep DLL
#

gcc -m32 -std=gnu99 -Wall -Wno-strict-aliasing -Wno-deprecated-declarations -O3 \
-I/usr/local/include -I/usr/local/include/SDL2 -I/usr/local/include/cairo \
-D HAVE_DIRENT_D_TYPE_DT_DIR=1 \
cache.c dict.c embeddedFS.c events.c gp.c graphicsPrims.c interp.c jpegPrims.c mem.c memGC.c oop.c \
parse.c prims.c serialPortPrims.c sha1.c sha2.c socketPrims.c soundPrims.c textAndFontPrims.c vectorPrims.c \
/usr/local/lib/libSDL2.a /usr/local/lib/libcairo.a /usr/local/lib/libpixman-1.a \
/usr/local/lib/libfreetype.a /usr/local/lib/libpng.a /usr/local/lib/libjpeg.a \
/usr/local/lib/libportaudio.a /usr/local/lib/libz.a \
-lm -liconv -ldinput8 -ldxerr8 -lusp10 -ldxguid -lwinmm -lversion -lsetupapi -luuid \
-lmingw32 -luser32 -lgdi32 -limm32 -lole32 -loleaut32 -lshell32 -lws2_32 \
-o gp
