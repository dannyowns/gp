#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

struct timeval startTime;

void startTimer(void) {
	gettimeofday(&startTime, 0);
}

int msecs(void) {
	struct timeval now;
	gettimeofday(&now, 0);
	int secs = now.tv_sec - startTime.tv_sec;
	int usecs = now.tv_usec - startTime.tv_usec;
	return (secs * 1000) + (usecs / 1000);
}

void loop(long n) {
	long i;
	for (i = 0; i < n; i++) { }
}

void timeLoop() {
	long n = 1000000;
	int t = 0;
	while (t < 100) {
		n *= 2;
		startTimer();
		loop(n);
		t = msecs();
	}
	printf("loop: %ld in %d msecs; %.3e loops/sec\n", n, t, ((float) n * 1000) / t);
}

float recurse(float n) {
	if (n < 2) return 1;
	else return recurse(n - 1) + recurse(n - 1) + 1;
}

void timeRecurse() {
	int depth = 20;
	float calls = 0;
	int t = 0;
	while (t < 100) {
		depth += 2;
		startTimer();
		calls = recurse(depth);
		t = msecs();
	}
	printf("recurse: %f calls in %d msecs; %.6e calls/sec\n",
		calls, t, ((double) calls * 1000) / t);
}

int sieve() {
	int i, k, primeCount = 0;
	int count = 8191 + 1; // + 1 for one-based indexing
	int *flags = malloc(count * sizeof(int));
	for (i = 0; i < count; i++) flags[i] = i;
	for (i = 2; i < count; i++) {
		if (flags[i] != 0) {
			primeCount++;
			for (k = i + i; k < count; k += i) flags[k] = 0;
		}
	}
	free(flags);
	return primeCount;
}

void timeSieve() {
	int i, iters = 1;
	int t = 0;
	while (t < 300) {
		iters *= 3;
		startTimer();
		for (i = 0; i < iters; i++) sieve();
		t = msecs();
	}
	printf("sieve: %d sieves in %d msecs; %d sieves/sec\n", iters, t, (iters * 1000) / t);
}

int main() {
//	timeLoop();
//	timeRecurse();
	timeSieve();
	printf("prime count %d\n", sieve());
}
