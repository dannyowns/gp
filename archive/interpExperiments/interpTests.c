// interpTests.c - Compare various hybids of C and GP primitives to better understand interpreter performance.
// John Maloney, October, 2013

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "mem.h"
#include "interp.h"

// ***** Millisecond Timer *****

struct timeval startTime;

void resetTimer(void) { gettimeofday(&startTime, 0); }

int msecs(void) {
	struct timeval now;
	gettimeofday(&now, 0);
	int secs = now.tv_sec - startTime.tv_sec;
	int usecs = now.tv_usec - startTime.tv_usec;
	return (secs * 1000) + (usecs / 1000);
}

// ***** Primitives *****

OBJ primFailed(char *reason);

OBJ primArrayAt(int nargs, OBJ args[]);
OBJ primArrayAtPut(int nargs, OBJ args[]);
OBJ primArrayAtAllPut(int nargs, OBJ args[]);

OBJ primLess(int nargs, OBJ args[]);
OBJ primMul(int nargs, OBJ args[]);

// ***** Dictionary *****
// A Dictionary is an Array of Associations, where an association is a name, value pair.
// Note: Empty slots contain nil. Growing is not yet supported.

static inline OBJ dictAssociationAt(OBJ dict, OBJ key) {
	// Return the first Association with the given key in the given Array of Associations, or nil if there is none.
	// Stop searching at the first nil (or non-Association) entry in dict (assumes all the empty slots are at the end).
	if (NOT_CLASS(dict, ArrayClass)) primFailed("Dictionary must be an Array");
	if (NOT_CLASS(key, StringClass)) primFailed("Key must be a String");
	
	char *k = obj2str(key);
	for (int i = 0; i < objWords(dict); i++) {
		OBJ assoc = FIELD(dict, i);
		if (NOT_CLASS(assoc, AssociationClass)) return nilObj;
		char *assocKey = obj2str(FIELD(assoc, 0));
		if (strcmp(assocKey, k) == 0) return assoc;
	}
	return nilObj;
}

static inline OBJ dictAtPut(OBJ dict, OBJ key, OBJ newValue) {
	// Set the value of the given key to the given value in the given association dictionary.
	OBJ assoc = dictAssociationAt(dict, key); // does class checks
	if (assoc) { // update existing entry
		FIELD(assoc, 1) = newValue;
	} else { // create a new entry
		if (NOT_CLASS(dict, ArrayClass)) primFailed("Dictionary must be an Array");
		assoc = newObj(AssociationClass, 2, nilObj);
		FIELD(assoc, 0) = key;
		FIELD(assoc, 1) = newValue;
		
		// insert the new entry at the first nil entry in dict (an Array).
		for (int i = 0; i < objWords(dict); i++) {
			if (FIELD(dict, i) == nilObj) {
				FIELD(dict, i) = assoc;
				return assoc;
			}
		}
		primFailed("Dictionary is full");
	}
	return assoc;
}

// ***** Variables (modified, inlined versions that always take a Reporter object) *****

static OBJ globals;

static inline OBJ primGetVar(OBJ b, OBJ args[]) {
	OBJ assoc = ((ReporterPtr) b)->reporterCache;
	if (!assoc) {
		OBJ varName = args[0];
		assoc = dictAssociationAt(globals, varName);
		if (!assoc) assoc = dictAtPut(globals, varName, nilObj); // create var
		((ReporterPtr) b)->reporterCache = assoc;
	}
	return FIELD(assoc, 1);
}

static inline OBJ primSetVar(OBJ b, OBJ args[]) {
	OBJ assoc = ((ReporterPtr) b)->reporterCache;
	if (!assoc) {
		OBJ varName = args[0];
		assoc = dictAssociationAt(globals, varName);
		if (!assoc) assoc = dictAtPut(globals, varName, nilObj); // create var
		((ReporterPtr) b)->reporterCache = assoc;
	}
	FIELD(assoc, 1) = args[1];
	return nilObj;
}

static inline OBJ primChangeVarBy(OBJ b, OBJ args[]) {
	OBJ assoc = ((ReporterPtr) b)->reporterCache;
	if (!assoc) {
		OBJ varName = args[0];
		assoc = dictAssociationAt(globals, varName);
		if (!assoc) assoc = dictAtPut(globals, varName, int2obj(0)); // create var, initially zero
		((ReporterPtr) b)->reporterCache = assoc;
	}
	OBJ oldValue = FIELD(assoc, 1);
	if (isInt(oldValue)) FIELD(assoc, 1) = int2obj(obj2int(oldValue) + evalInt(args[1]));
	return nilObj;
}

// ***** Helper Functions *****

OBJ makeVarReporter(char *s) {
	ReporterPtr r = (ReporterPtr) newObj(ReporterClass, ReporterFieldCount + 1, nilObj);
	r->rargs[0] = newString(s);
	return (OBJ) r;
}

inline OBJ v(OBJ varReporter) {
	return primGetVar(varReporter, ((ReporterPtr) varReporter)->rargs);
}

inline void setvar(OBJ varReporter, OBJ value) {
	OBJ args[] = {((ReporterPtr) varReporter)->rargs[0], value};
	primSetVar(varReporter, args);
}

inline void changeBy(OBJ varReporter, OBJ delta) {
	OBJ args[] = {((ReporterPtr) varReporter)->rargs[0], delta};
	primChangeVarBy(varReporter, args);
}

inline OBJ at(OBJ array, OBJ index) {
	OBJ args[] = {array, index};
	return primArrayAt(2, args);
}

inline void atPut(OBJ array, OBJ index, OBJ value) {
	OBJ args[] = {array, index, value};
	primArrayAtPut(3, args);
}

inline void atAllPut(OBJ array, OBJ value) {
	OBJ args[] = {array, value};
	primArrayAtAllPut(2, args);
}

inline OBJ less(OBJ n1, OBJ n2) {
	OBJ args[] = {n1, n2};
	return primLess(2, args);
}

inline OBJ mult(OBJ n1, OBJ n2) {
	OBJ args[] = {n1, n2};
	return primMul(2, args);
}

inline void print(OBJ value) {
	printlnObj(value);
}

// ***** Interpreter Tests  *****

void primeSieves1(int iters) {
	// Uses C control flow, but primitives for variables, arithmetic, and array access.

	globals = newObj(ArrayClass, 10, nilObj); // global variable values

	OBJ primeCount = makeVarReporter("primeCount");
	OBJ flags = makeVarReporter("flags");
	OBJ i = makeVarReporter("i");
	OBJ j = makeVarReporter("j");

	setvar(flags, newObj(ArrayClass, 8190, nilObj));

	for (int n = 0; n < iters; n++) {
		setvar(primeCount, int2obj(0));
		atAllPut(v(flags), trueObj);
		setvar(i, int2obj(2));

		for (int c1 = 0; c1 < 8188; c1++) {
			if (at(v(flags), v(i)) == trueObj) {
				if (iters == 1) print(v(i)); // i is prime!
				changeBy(primeCount, int2obj(1));
				setvar(j, mult(int2obj(2), v(i)));
				while (less(v(j), int2obj(8190)) == trueObj) { // mark multiples of i as non-prime
					atPut(v(flags), v(j), falseObj);
					changeBy(j, v(i));
				}
			}
			changeBy(i, int2obj(1));
		}
		if (iters == 1) printf("Found %d prime numbers\n", obj2int(v(primeCount)));
	}
}

void primeSieves2(int iters) {
	// Use C control flow and variables & arithmetic, but primitives for array access.
	
	int primeCount, i, j;
	OBJ flags = makeVarReporter("flags");
	
	flags = newObj(ArrayClass, 8190, nilObj);
	
	for (int n = 0; n < iters; n++) {
		primeCount = 0;
		atAllPut(flags, trueObj);
		i = 2;		
		for (int c1 = 0; c1 < 8188; c1++) {
			if (at(flags, int2obj(i)) == trueObj) {
				if (iters == 1) printf("%d\n", i);
				primeCount++;
				j = 2 * i;
				while (j < 8190) { // mark multiples of i as non-prime
					atPut(flags, int2obj(j), falseObj);
					j += i;
				}
			}
			i++;
		}
		if (iters == 1) printf("Found %d prime numbers\n", primeCount);
	}
}

void primeSieves3(int iters) {
	// Pure C version, for comparison.
	
	int primeCount, i, j;
	int flags[8190];
	
	for (int n = 0; n < iters; n++) {
		primeCount = 0;
		for (j = 0; j < 8190; j++) flags[j] = 1;
		i = 2;		
		for (int c1 = 0; c1 < 8188; c1++) {
			if (flags[i]) {
				if (iters == 1) printf("%d\n", i);
				primeCount++;
				j = 2 * i;
				while (j < 8190) { // mark multiples of i as non-prime
					flags[j] = 0;
					j += i;
				}
			}
			i++;
		}
		if (iters == 1) printf("Found %d prime numbers\n", primeCount);
	}
}

void runTests() {
	int iters = 1000;
	
	resetTimer();
	primeSieves1(iters);
	printf("1: %d iters in %d msecs (GP prims)\n", iters, msecs());
	
	resetTimer();
	primeSieves2(iters);
	printf("2: %d iters in %d msecs (GP array ops only)\n", iters, msecs());
	
	resetTimer();
	primeSieves3(iters);
	printf("3: %d iters in %d msecs (pure C)\n", iters, msecs());
}

// ***** Main *****

int main(int argc, char *argv[]) {
	memInit(100000);
	initGP();
	runTests();
}