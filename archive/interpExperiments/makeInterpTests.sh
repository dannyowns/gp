#!/bin/sh
rm -f interpTests
gcc -m32 -std=c99 -Wall -O3 mem.c parse.c interp.c prims.c graphicsPrimsStubs.c interpTests.c -o interpTests
if [ ! -e 'interpTests' ]; then echo 'Build failed'; exit; fi
