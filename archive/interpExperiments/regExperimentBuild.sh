#!/bin/sh
rm -f gp
gcc -m32 -Wall -O3 -c interp.c
gcc -m32 -std=c99 -Wall -O3 mem.c parse.c  prims.c graphicsPrimsStubs.c gp.c interp.o -o gp \
	-I/Library/Frameworks/SDL2.framework/Headers -framework SDL2
if [ ! -e 'gp' ]; then echo 'Build failed'; exit; fi
