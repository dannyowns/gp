// Test ability to read a zip file appended to an executable.

#include <alloca.h>
#include <stdio.h>
#include <string.h>

FILE *f;

#define FILE_BUF_SIZE 1000000
unsigned char fileBuffer[FILE_BUF_SIZE];
int fileSize = 0;
int pos;

void readFileIntoBuffer(char *fileName) {
	// Read the entire file with the given name into fileBuffer and
	// set fileSize to the number of bytes read. Set fileSize to zero
	// if the file doesn't exist or is too larger to fit in the buffer.
	
	fileSize = 0; // clear fileBuffer
	
	FILE *f = fopen(fileName, "r");
	if (!f) {
		printf("File not found: %s\n", fileName);
		return;
	}
	
	fseek(f, 0L, SEEK_END);
	long n = ftell(f); // get file size
	fseek(f, 0L, SEEK_SET);	
	
	if (n < FILE_BUF_SIZE) {
		fread(fileBuffer, sizeof(char), n, f);
		fileSize = n;
	}
	
	fclose(f);
}

int scanForEndRecord() {
	for (int i = fileSize - 4; i >= 0; i--) {
		if ((fileBuffer[i] == 0x50) &&
			(fileBuffer[i+1] == 0x4b) &&
			(fileBuffer[i+2] == 0x05) &&
			(fileBuffer[i+3] == 0x06)) {
				printf("found dir end record at: %d\n", i);
				return i;
		}
	}
	printf("No zip directory found.\n");
	return 0;
}

int read16() {
	int result = (fileBuffer[pos+1] << 8) | fileBuffer[pos];
	pos += 2;
	return result;
}

int read32() {
	printf("read32: %d %d %d %d\n", fileBuffer[pos+3], fileBuffer[pos+2], fileBuffer[pos+1], fileBuffer[pos+0]);
	
	int result = (fileBuffer[pos+3] << 24) | (fileBuffer[pos+2] << 16) | (fileBuffer[pos+1] << 8) | fileBuffer[pos];
	pos += 4;
	return result;
}

void readString(char *s, int max) {
	int n = read16();
	printf("string len %d\n", n);
	int count = (n > max) ? max : n;
	for (int i = 0; i < count; i++) {
		s[i] = fileBuffer[pos + i];
	}
	pos += n;
}

void printEndRecord(int i) {
	pos = i;
	int id = read32();
	printf("id %x\n", id);
	int thisDiskNum = read16();
	int startDiskNum = read16();
	int entriesOnThisDisk = read16();
	int totalEntries = read16();
	int directorySize = read32();
	int directoryOffset = read32();
	char comment[101];
	readString(comment, 100);
	printf("thisDiskNum %d, startDiskNum %d, entriesOnThisDisk %d, totalEntries %d, directorySize %d, directoryOffset %d\n",
		   thisDiskNum, startDiskNum, entriesOnThisDisk, totalEntries, directorySize, directoryOffset);
}

int main(int argc, char *argv[]) {
	readFileIntoBuffer(argv[0]);
	printf("read %d\n", fileSize);
//	char *mem = alloca(100);
//	printf("allocated %d\n", (int) mem);
	int i = scanForEndRecord();
	if (i) printEndRecord(i);
}