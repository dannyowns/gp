// Experimental GP BlockFinder w/ built in palette

defineClass BlockFinder morph window searchText blocksFrame

to newBlockFinder {
  return (initialize (new 'BlockFinder'))
}

method initialize BlockFinder {
  scale = (global 'scale')
  window = (window 'BlockFinder')
  morph = (morph window)
  setHandler morph this

  searchText = (newText)
  setFont searchText nil (scale * 15)
  setColor searchText nil nil (color 240 240 240)
  setEditRule searchText 'line'
  setGrabRule (morph searchText) 'ignore'
  setBorders searchText 4 4 true
  addPart morph (morph searchText)

  blocksPane = (newBlocksPalette)
  setSortingOrder (alignment blocksPane) nil
  setRule (alignment blocksPane) 'column'
  blocksFrame = (scrollFrame blocksPane (color 120 120 120))
  addPart morph (morph blocksFrame)

  setMinExtent morph (scale * 200) (scale * 200)
  setExtent morph (scale * 300) (scale * 450)
  return this
}

method redraw BlockFinder {
  fixLayout window
  redraw window
  fixLayout this
}

method clicked BlockFinder hand {
  bnds = (bounds (morph searchText))
  edit (keyboard (page hand)) searchText (left bnds) (top bnds)
  return false
}

method textEdited BlockFinder aText {
  blocksPane = (contents blocksFrame)
  removeAllParts (morph blocksPane)

  specList = (findSpecsMatching this (text aText))
//  print 'found' (count specList)
  if ((count specList) > 30) { specList = (copyArray (toArray specList) 30) }
  for spec specList {
    b = (morph (blockForSpec spec))
    setGrabRule b 'template'
    addPart (morph blocksPane) b
  }
  cleanUp blocksPane
}

method findSpecsMatching BlockFinder prefix {
  result = (list)
  if ('' == prefix) { return result }

  entries = (dictionary)
  authoringSpecs = (authoringSpecs)
  for entry (allSpecs authoringSpecs) {
	fName = (at entry 2)
	spec = (at entry 3)
	if (beginsWith fName prefix) {
	  if (not (contains entries fName)) {
		add entries fName
		add result (specForEntry authoringSpecs entry)
	  }
	}
	specWords = (words spec)
	s = (joinStringArray (copyWithout specWords '_') ' ')
	if (beginsWith s prefix) {
	  if (not (contains entries fName)) {
		add entries fName
		add result (specForEntry authoringSpecs entry)
	  }
	}
  }
  return result
}

method fixLayout BlockFinder {
  fixLayout window
  packer = (newPanePacker (clientArea window))
  packPanesH packer searchText '100%'
  packPanesH packer blocksFrame '100%'
  packPanesV packer searchText 18 blocksFrame '100%'
  finishPacking packer
}

to blockFinderDemo {
  page = (newPage 1600 700)
  open page true
  addPart page (newBlockFinder)
  startStepping page
}
