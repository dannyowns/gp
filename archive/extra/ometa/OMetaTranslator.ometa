rule OMetaTranslator cls level {
        trans      = [ anything:sel {(call sel this)}:v ] -> {v},
        action     = {(pushLevel this)}:d anything:v {(popLevel this)}
                       -> {(array 'result' d ' = ' v (cr this))},
        predAction = {(pushLevel this)}:d
                     anything:v
                     {(popLevel this)}
                       -> {(array
                            'result' d ' = ' v (cr this)
                            'if (not result' d ') {' (cr this)
                            'result' d ' = matcherError' (cr this)
                            '}' (cr this))},

        and        = {(pushLevel this)}:d
                     {(nextLevel this)}:n
                     {(array 'origCursor' d ' = (at cursors 1)' (cr this)
                      'result' d ' = nil' (cr this))}:init
                     (trans:r {(array
                        'if (not (result' d ' === matcherError)) {' (cr this)
                        r
                        'result' d ' = result' n (cr this)
                        'if (result' d ' === matcherError) {' (cr this)
                        'atPut cursors 1 origCursor' d (cr this)
                        '}' (cr this)
                        '}' (cr this))})*:rs
                     {(popLevel this)}
                       -> {(array init (toArray rs))},

        or         = {(pushLevel this)}:d
                     {(nextLevel this)}:n
                     {(array
                        'origCursor' d ' = (at cursors 1)' (cr this)
                        'done' d ' = false' (cr this)
                        'result' d ' = nil' (cr this))}:init
                     (trans:r {(array
                        'if (not done' d ') {' (cr this)
                        r
                        'result' d ' = result' n (cr this)
                        'if (not (result' d ' === matcherError)) {' (cr this)
                        'done' d ' = true' (cr this)
                        '} else {' (cr this)
                        'atPut cursors 1 origCursor' d (cr this)
                        '}' (cr this)
                        '}' (cr this))})*:rs
                     {(array
                        'if (not done' d ') {' (cr this)
                        'result' d ' = matcherError' (cr this)
                        '}' (cr this))}:after
                     {(popLevel this)}
                       -> {(array init (toArray rs) after)},

        option     = {(pushLevel this)}:d
                     {(nextLevel this)}:n
                     trans:r
                     {(popLevel this)}
                       -> {(array
                         'origCursor' d ' = (at cursors 1)' (cr this)
                         r
                         'result' d ' = result' n (cr this)
                         'if (result' d ' === matcherError) {' (cr this)
                         'at cursors 1 origCursor' d (cr this)
                         'result' d ' = nil' (cr this)
                         '}' (cr this))},

        not        = {(pushLevel this)}:d
                     {(nextLevel this)}:n
                     trans:r
                     {(popLevel this)}
                       -> {(array
                         'origCursor' d ' = (at cursors 1)' (cr this)
                         r
                         'result' d ' = result' n (cr this)
                         'atPut cursors 1 origCursor' d (cr this)
                         'if (result' d ' === matcherError) {' (cr this)
                         'result' d ' = true' (cr this)
                         '} else {' (cr this)
                         'result' d ' = matcherError' (cr this)
                         '}' (cr this))},

        many0      = {(pushLevel this)}:d
                     {(nextLevel this)}:n
                     trans:r
                     {(popLevel this)}
                       -> {(array
                          'result' d ' = (list)' (cr this)
                          'val' d ' = nil' (cr this)
                          'while (not (val' d ' === matcherError)) {' (cr this)
                          r
                          'val' d ' = result' n (cr this)
                          'if (not (val' d ' === matcherError)) {' (cr this)
                          'add result' d ' val' d (cr this)
                        '}' (cr this)
                        '}' (cr this)
                        )},

        many1      = {(pushLevel this)}:d
                     {(nextLevel this)}:n
                     trans:r
                     {(popLevel this)}
                       -> {(array
                         'origCursor' d ' = (at cursors 1)' (cr this)
                         'result' d ' = (list)' (cr this)
                         'val' d ' = nil' (cr this)
                         'first' d ' = true' (cr this)
                         'while (not (val' d ' === matcherError)) {' (cr this)
                        r
                        'val' d ' = result' n (cr this)
                        'if (not (val' d ' === matcherError)) {' (cr this)
                        'add result' d ' val' d (cr this)
                        '} else {' (cr this)
                        'if first' d ' {' (cr this)
                        'atPut cursors 1 origCursor' d (cr this)
                        'result' d ' = matcherError' (cr this)
                        '}' (cr this)
                        '}' (cr this)
                        'first' d ' = false' (cr this)
                        '}' (cr this)
                        )},
        lookahead  = {(pushLevel this)}:d
                     {(nextLevel this)}:n
                     trans:r
                     {(popLevel this)}
                       -> {(array
                         'origCursor' d ' = (at cursors 1)' (cr this)
                         r
                         'result' d ' = result' n (cr this)
                         'atPut cursors 1 origCursor' d (cr this))},

        consume    = {(pushLevel this)}:d
                     {(nextLevel this)}:n
                     trans:r
                     {(popLevel this)}
                       -> {(array
                         'origCursor' d ' = (at cursors 1)' (cr this)
                         r
                         'result' d ' = result' n (cr this)
                         'if (result' d ' === matcherError) {' (cr this)
                         'at cursors 1 origCursor' d (cr this)
                         '} else {' (cr this)
                         'result' d ' = (toArray (copyList currentList ((at cursors 1) - origCursor' d ') origCursor' d '))' (cr this)
                         '}' (cr this))},

        bind       = {(pushLevel this)}:d
                     {(nextLevel this)}:n
                     anything:v trans:r
                     {(popLevel this)}
                       -> {(array 
                         'origCursor' d ' = (at cursors 1)' (cr this)
                         r
                         'result' d ' = result' n (cr this)
                         'if (result' d ' === matcherError) {' (cr this)
                         'atPut cursors 1 origCursor' d (cr this)
                         '} else {' (cr this)
                         v ' = result' d (cr this)
                         '}' (cr this))},

        form       = {(pushLevel this)}:d
                     {(nextLevel this)}:n
                     trans:r
                     {(popLevel this)}
                       -> {(array
                         'origCurrentList' d ' = currentList' (cr this)
                         'origCursor' d ' = (at cursors 1)' (cr this)
                         'done' d ' = false' (cr this)
                         'result' d ' = nil' (cr this)
                         'if ((count currentList) < origCursor' d ') {' (cr this)
                         'result' d ' = matcherError' (cr this)
                         'done' d ' = true' (cr this)
                         '}' (cr this)
                         'if (not (result' d ' === matcherError)) {' (cr this)
                         'c' d ' = (at currentList origCursor' d ')' (cr this)
                         'if (and ((classOf c' d ') != (class ''Array'')) ((classOf c' d ') != (class ''List''))) {' (cr this)
                         'result' d ' = matcherError' (cr this)
                         'done' d ' = true' (cr this)
                         '}' (cr this)
                         '}' (cr this)
                         'if (not (result' d ' === matcherError)) {' (cr this)
                         'currentList = c' d (cr this)
                         'addFirst cursors 1' (cr this)
                         r
                         'result' d ' = result' n (cr this)
                         'if (not (result' d ' === matcherError)) {' (cr this)
                         'if ((at cursors 1) > (count currentList)) {' (cr this)
                         'result' d ' = currentList' (cr this)
                         'currentList = origCurrentList' d (cr this)
                         'removeFirst cursors' (cr this)
                         'atPut cursors 1 (origCursor' d ' + 1)' (cr this)
                         'done' d ' = true' (cr this)
                         '}' (cr this)
                         '}' (cr this)
                         '}' (cr this)
                         'if (not done' d ') {' (cr this)
                         'currentList = origCurrentList' d (cr this)
                         'removeFirst cursors' (cr this)
                         'atPut cursors 1 origCursor' d (cr this)
                         '}' (cr this))},

        app        = {(pushLevel this)}:d
                     anything:n anything*:as
                     {(popLevel this)}
                       -> {(appFunc this n as d)},

        rule       = anything:n [ anything*:as ] [ anything*:ts ] trans:r
                       -> {(array
                         'method ' n ' ' (cls this) (map (function a {return (array ' ' a)}) as) ' {' (cr this)
                         r
                         'result1 = result2' (cr this)
                         'return result1' (cr this)
                         '}' (cr this))},

        grammar    = anything:cls [ anything*:insts ] anything:rest trans*:rs
                       -> {(array
                         'defineClass ' cls (map (function i {return (array ' ' i)}) insts) ' cursors currentList matcherError memo' (cr this)
                         'method match ' cls ' input rule {' (cr this)
                         '  cursors = (list 1)' (cr this)
                         '  matcherError = (array ''matcherror'')' (cr this)
                         '  currentList = input' (cr this)
                         '  return (call rule this)' (cr this)
                         '}' (cr this)

			 'method apply ' cls ' rule {' (cr this)
			 '  shouldMemo = false' (cr this)
			 '  if (notNil memo) {' (cr this)
			 '    if ((argCount) == 2) {' (cr this)
			 '      if ((count cursors) == 1) {' (cr this)
			 '        pos = (at cursors 1)' (cr this)
			 '        m = (at memo pos)' (cr this)
			 '        if (notNil m) {' (cr this)
			 '          v = (at m rule)' (cr this)
			 '          if (notNil v) {' (cr this)
			 '            atPut cursors 1 (at v 2)' (cr this)
			 '            return (at v 1)' (cr this)
			 '          }' (cr this)
			 '        }' (cr this)
			 '        shouldMemo = true' (cr this)
			 '      }' (cr this)
			 '    }' (cr this)
			 '  }' (cr this)
			 '  if ((argCount) == 2) {' (cr this)
			 '    result = (call rule this)' (cr this)
			 '  } else {' (cr this)
			 '    args = (newArray ((argCount) - 1))' (cr this)
			 '    atPut args 1 this' (cr this)
			 '    for i ((argCount) - 2) {' (cr this)
			 '      atPut args (i + 1) (arg (i + 2))' (cr this)
			 '    }' (cr this)
			 '    result = (callWith rule args)' (cr this)
			 '  }' (cr this)
			 '  if (not shouldMemo) {' (cr this)
			 '    return result' (cr this)
			 '  }' (cr this)
			 '  newPos = (at cursors 1)' (cr this)
			 '  if (isNil m) {' (cr this)
			 '    m = (dictionary)' (cr this)
			 '    atPut memo pos m' (cr this)
			 '  }' (cr this)
			 '  atPut m rule (array result newPos)' (cr this)
			 '  return result' (cr this)
			 '}' (cr this)
                         'method exactly ' cls ' str {' (cr this)
                         '  if ((at cursors 1) <= (count currentList)) {' (cr this)
                         '    if (str == (at currentList (at cursors 1))) {' (cr this)
                         '      result = (at currentList (at cursors 1))' (cr this)
                         '      atPut cursors 1 (+ (at cursors 1) 1)' (cr this)
                         '      return result' (cr this)
                         '    }' (cr this)
                         '  }' (cr this)
                         '  return matcherError' (cr this)
                         '}' (cr this)
                         'method seq ' cls ' str {' (cr this)
                         '  s = (letters str)' (cr this)
                         '  origCursor = (at cursors 1)' (cr this)
                         '  for i (count s) {' (cr this)
                         '    if ((at cursors 1) <= (count currentList)) {' (cr this)
                         '      if ((at s i) == (at currentList (at cursors 1))) {' (cr this)
                         '        atPut cursors 1 (+ (at cursors 1) 1)' (cr this)
                         '      } else {' (cr this)
                         '        atPut cursors 1 origCursor' (cr this)
                         '        return matcherError' (cr this)
                         '      }' (cr this)
                         '    } else {' (cr this)
                         '      atPut cursors 1 origCursor' (cr this)
                         '      return matcherError' (cr this)
                         '    }' (cr this)
                         '  }' (cr this)
                         '  return str' (cr this)
                         '}' (cr this)
                         'method end ' cls ' {' (cr this)
                         '  if ((at cursors 1) > (count currentList)) {' (cr this)
                         '    return true' (cr this)
                         '  }' (cr this)
                         '  return matcherError' (cr this)
                         '}' (cr this)
                         'method token ' cls ' str {' (cr this)
                         '  origCursor = (at cursors 1)' (cr this)
                         '  spaces this' (cr this)
                         '  result = (seq this str)' (cr this)
                         '  if (result === matcherError) {' (cr this)
                         '    atPut cursors 1 origCursor' (cr this)
                         '  }' (cr this)
                         '  return result' (cr this)
                         '}' (cr this)
                         'method spaces ' cls ' {' (cr this)
                         '  while true {' (cr this)
                         '    if ((at cursors 1) <= (count currentList)) {' (cr this)
                         '       if (isWhiteSpace (at currentList (at cursors 1))) {' (cr this)
                         '         atPut cursors 1 (+ (at cursors 1) 1)' (cr this)
                         '       } else {' (cr this)
                         '         return nil' (cr this)
                         '       }' (cr this)
                         '    } else {' (cr this)
                         '       return nil' (cr this)
                         '    }' (cr this)
                         '  }' (cr this)
                         '  return nil' (cr this)
                         '}' (cr this)
                         'method anything ' cls ' {' (cr this)
                         '  if ((at cursors 1) <= (count currentList)) {' (cr this)
                         '    result = (at currentList (at cursors 1))' (cr this)
                         '    atPut cursors 1 (+ (at cursors 1) 1)' (cr this)
                         '    return result' (cr this)
                         '  }' (cr this)
                         '  return matcherError' (cr this)
                         '}' (cr this)
                         'method empty ' cls ' {' (cr this)
                         '  return true' (cr this)
                         '}' (cr this)
 			 'method useMemo ' cls ' {' (cr this)
			 '  memo = (dictionary)' (cr this)
			 '}' (cr this)

                         (map (function r {return (array (stringFromCodePoints (array 10)) r)}) rs) rest)}
}

method appFunc OMetaTranslator n as d {
  return (array 'result' d ' = (apply this ' (printString n) (map (function a {(array ' ' (printString a))}) as) ')' (cr this))
}

method cr OMetaTranslator {
  return (stringFromCodePoints (array 10))
}

method cls OMetaTranslator {
  if (cls != nil) {
    return cls
  } else {
    return 'Unknown'
  }
}

method pushLevel OMetaTranslator {
  level += 1
  return (toString level)
}

method nextLevel OMetaTranslator {
  return (toString (level + 1))
}

method popLevel OMetaTranslator {
  level = (level - 1)
  return (toString level)
}

