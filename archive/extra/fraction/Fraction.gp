// Fraction

defineClass Fraction numerator denominator

method numerator Fraction { return numerator }
method denominator Fraction { return denominator }

to frac num denom {
  if (denom == 0) { error 'Fraction denominator must be non-zero' }
  return (new 'Fraction' num denom)
}

to toFraction n {
  if (isClass n 'Integer') {
	return (frac n 1)
  } (isClass n 'Float') {
	// approximation! only works for a subset of Floats
	precision = 1000000.0
	return ((toInteger (n * precision)) / precision)
  }
  error 'Could not convert to Fraction.'
}

// converting

method toFraction Fraction { return this }

method toInteger Fraction {
  // Return the nearest integer truncated towards zero. (Rename to "truncated"?)
  return (numerator / denominator)
}

method toFloat Fraction { return ((toFloat numerator) / denominator) }
method toNumber Fraction { return (toFloat this) }

method toString Fraction {
  f = (simplify this)
  return (join (numerator f) ':' (denominator f))
}

method reciprocal Fraction {
  return (frac denominator numerator)
}

method simplify Fraction {
  if (numerator == 0) {return (frac 0 1)}
  gcd = (gcd numerator denominator)
  return (frac (numerator / gcd) (denominator / gcd))
}

// comparing

method '==' Fraction other {
  return (compare this '==' other)
}

method '!=' Fraction other {
  return (compare this '!=' other)
}

method '>' Fraction other {
  return (compare this '>' other)
}

method '>=' Fraction other {
  return (compare this '>=' other)
}

method '<' Fraction other {
  return (compare this '<' other)
}

method '<=' Fraction other {
  return (compare this '<=' other)
}

method compare Fraction operator other {
  denomOther = (denominator other)
  lcm = (lcm denominator denomOther)
  return (call operator
      (* numerator (lcm / denominator))
      (* (numerator other) (lcm / denomOther)))
}

// arithmetic

method '+' Fraction other {
  denomOther = (denominator other)
  lcm = (lcm denominator denomOther)
  return (simplify (frac
    (+
      (* numerator (lcm / denominator))
      (* (numerator other) (lcm / denomOther)))
    lcm))
}

method '-' Fraction other {
  if (isNil other) { // unary minus
    return (frac (- numerator) denominator)
  }
  return (+ this (- other))
}

method '*' Fraction other {
  return (simplify (frac
    (* numerator (numerator other))
    (* denominator (denominator other))))
}

method '/' Fraction other {
  return (* this (reciprocal other))
}
