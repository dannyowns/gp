assert (gcd 8 12) 4 'gcd'
assert (lcm 3 7) 21 'lcm'

assert (reciprocal (frac 3 4)) (frac 4 3) 'reciprocal'
assert (- (frac 3 4)) (frac -3 4) 'unary minus'
assert (simplify (frac 8 12)) (frac 2 3) 'simplify'
assert (toInteger (frac 10 3)) 3 'toInteger'

assert (== (frac 3 4) (frac 3 4)) true '=='
assert (!= (frac 3 4) (frac 3 4)) false '!='
assert (> (frac 3 4) (frac 4 5)) false '>'
assert (< (frac 3 4) (frac 4 5)) true '<'

assert (+ (frac 3 4) (frac 5 6)) (frac 19 12) '+'
assert (- (frac 3 4) (frac 1 6)) (frac 7 12) '-'
assert (* (frac 3 4) (frac 1 6)) (frac 1 8) '*'
assert (/ (frac 3 4) (frac 1 6)) (frac 9 2) '/'

assert (max (frac 1 2) (frac 3 4) (frac 2 3)) (frac 3 4) 'max'
assert (min (frac 5 6) (frac 3 4) (frac 7 8)) (frac 3 4) 'min'

// cases where zero is involved
assert (frac 0 100) (frac 0 1) 'zero gets normalized'
assert (- (frac 2 100) (frac 2 100)) (frac 0 1) 'zero gets normalized'
assert (* (frac 2 3) (frac 0 1)) (frac 0 1) 'zero gets normalized'
