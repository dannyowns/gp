// d1Recog.gp - One dollar gesture recognizer
// Yoshiki Ohshima, February 2014

defineClass Recognizer isDrawing mouseX mouseY lastX lastY recognizer points

method plot Recognizer x y { fillRect nil (color) x y 3 3 }

method drawBresenhamLine Recognizer x0 y0 x1 y1 {
  dx = (abs (x1 - x0))
  dy = (abs (y1 - y0))
  if (x0 < x1) { sx = 1 } else { sx = -1 }
  if (y0 < y1) { sy = 1 } else { sy = -1 }
  err = (dx - dy)
  while true {
	plot this x0 y0
	if (and (x0 == x1) (y0 == y1)) { return }
	e2 = (2 * err)
	if (e2 > (0 - dy)) { 
	  err = (err - dy)
	  x0 = (x0 + sx)
	}
	if (and (x0 == x1) (y0 == y1)) { 
	  plot this x0 y0
	  return
	}
	if (e2 < dx) { 
	  err = (err + dx)
	  y0 = (y0 + sy)
	}
  }
}

method setup Recognizer {
  closeWindow
  openWindow
  isDrawing = false
  recognizer = (new 'D1Recognizer')
  initTemplates recognizer
}

method handleUserInput Recognizer {
  evt = (nextEvent)
  while (notNil evt) {
    evtType = (at evt 'type')
	if ('mouseDown' == evtType) {
      mouseX = (at evt 'x')
      mouseY = (at evt 'y')
      points = (list (new 'Point' mouseX mouseY))
      lastX = mouseX
      lastY = mouseY
      isDrawing = true
	}
	if ('mouseMove' == evtType) {
      mouseX = (at evt 'x')
      mouseY = (at evt 'y')
    }
	if ('mouseUp' == evtType) {
	  gc
      print (recognize recognizer points)
      isDrawing = false
	}
    evt = (nextEvent)
  }
}

method run Recognizer {
  setup this
  while true {
    handleUserInput this
	if isDrawing {
	  add points (new 'Point' mouseX mouseY)
	  drawBresenhamLine this lastX lastY mouseX mouseY
	  flipBuffer
	  drawBresenhamLine this lastX lastY mouseX mouseY // draw in other buffer, too
	  lastX = mouseX
	  lastY = mouseY
	}
    sleep 10
  }
}

run (new 'Recognizer')
