call (function {
  a = (heap)

  add a 1
  add a 3
  add a 6
  add a 4
  add a 5
  add a 2

  assert (count a) 6

  assert (removeFirst a) 1 '1'
  assert (count a) 5

  assert (removeFirst a) 2 '2'
  assert (count a) 4

  assert (removeFirst a) 3 '3'
  assert (count a) 3

  assert (removeFirst a) 4 '4'
  assert (count a) 2

  add a 3 
  assert (count a) 3

  assert (removeFirst a) 3 '3 again'
  assert (count a) 2

  assert (removeFirst a) 5 '5'
  assert (count a) 1

})

defineClass HeapNode n

method n HeapNode { return n }
method <= HeapNode aNode { return (n > (n aNode)) }

to node n { return (new 'HeapNode' n) }

call (function {
  a = (heap 10 (function a b {a > b}))

  add a (node 1)
  add a (node 3)
  add a (node 6)
  add a (node 4)
  add a (node 5)
  add a (node 2)

  assert (count a) 6

  assert (n (removeFirst a)) 6 '1'
  assert (count a) 5

  assert (n (removeFirst a)) 5 '2'
  assert (count a) 4

  assert (n (removeFirst a)) 4 '3'
  assert (count a) 3

  assert (n (removeFirst a)) 3 '4'
  assert (count a) 2

  add a (node 3)
  assert (count a) 3

  assert (n (removeFirst a)) 3 '3 again'
  assert (count a) 2

  assert (n (removeFirst a)) 2 '5'
  assert (count a) 1

})
