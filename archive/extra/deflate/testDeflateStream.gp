call (function {
  b = (newBinaryData (* 32 32 4))

  for i ((byteCount b) / 4) {
    byteAtPut b (((i - 1) * 4) + 1) 255
    byteAtPut b (((i - 1) * 4) + 2) 128
    byteAtPut b (((i - 1) * 4) + 3) 64
    byteAtPut b (((i - 1) * 4) + 4) 32
  }

  strm = (dataStream (newBinaryData 100))

  s = (onZLib (new 'DeflateStream') strm)
  nextPutAll s b 1 (byteCount b)

  assert (getField s 'position') (byteCount b)
  close s
  data = (contents s)

  expected = (array 120 94 229 195 1 13 0 0 8 3 160 42 70 49 154 205 125 143 195 198 223 142 223 239 247 251 253 126 191 223 239 247 251 253 254 238 1 10 227 124 106)

  assert (byteCount data) (count expected) 'count dynamic'

  match = true
  for i (byteCount data) {
    match = (and match ((byteAt data i) == (at expected i)))
  }
  assert match true 'dynamic huffman table result'

  return s
})

call (function {
  b = (newBinaryData (* 4 4 4))

  for i ((byteCount b) / 4) {
    byteAtPut b (((i - 1) * 4) + 1) 255
    byteAtPut b (((i - 1) * 4) + 2) 128
    byteAtPut b (((i - 1) * 4) + 3) 64
    byteAtPut b (((i - 1) * 4) + 4) 32
  }

  strm = (dataStream (newBinaryData 100))

  s = (onZLib (new 'DeflateStream') strm)
  nextPutAll s b 1 64

  assert (getField s 'position') 64
  close s
  data = (contents s)

  expected = (array 120 94 251 223 224 160 64 9 6 0 228 77 29 241)
  assert (byteCount data) (count expected) 'count fixed'
  match = true
  for i (byteCount data) {
    match = (and match ((byteAt data i) == (at expected i)))
  }
  assert match true 'fixed huffman table result'

  return s
})

call (function {
  original = (array 152 102 21 116 179 253 108 97 64 251 111 253 21 78 15 98 82 66 34 195 68 232 197 115 245 185 209 35 91 125 6 19 67 244 233 6 177 38 47 250 249 70 207 47 101 198 177 36 116 182 130 123 203 6 120 203 129 105 45 162 150 8 61 183 200 151 131 253 65 117 159 188 219 17 74 77 139 148 3 43 124 171 108 160 91 250 194 89 67 237 239 221 100 239 145 191 227 76 152 95 69 80 167 239 252 85 74 249 146 175 188 207 118 211 218 20 156 184 77 157 192 227 71 140 218 70 100 119 12 125 20 172 2 20 198 142 207 100 227 245 62 181 184 1 11 2 226 224 47 150 18 175 65 226 216 115 142 247 152 3 122 106 13 16 102 121 255 156 124 7 254 39 11 251 200 255 71 145 249 190 167 246 121 41 219 243 17 203 151 67 177 240 140 127 89 162 7 180 95 9 203 61 3 239 57 38 149 187 34 158 195 38 23 101 47 188 36 164 168 8 16 202 217 138 0 103 158 251 186 206 7 246 83 18 155 202 131 162 42 21 62 17 168 239 28 30 26 203 212 212 236 44 214 178 153 128) // a random number sequence


  b = (newBinaryData (count original))
  for i (byteCount b) {
    byteAtPut b i (at original i)
  }

  strm = (dataStream (newBinaryData 100))
  s = (onZLib (new 'DeflateStream') strm)
  nextPutAll s b 1 (byteCount b)

  assert (getField s 'position') (byteCount b)
  close s
  data = (contents s)

  expected = (array 120 94 1 0 1 255 254 152 102 21 116 179 253 108 97 64 251 111 253 21 78 15 98 82 66 34 195 68 232 197 115 245 185 209 35 91 125 6 19 67 244 233 6 177 38 47 250 249 70 207 47 101 198 177 36 116 182 130 123 203 6 120 203 129 105 45 162 150 8 61 183 200 151 131 253 65 117 159 188 219 17 74 77 139 148 3 43 124 171 108 160 91 250 194 89 67 237 239 221 100 239 145 191 227 76 152 95 69 80 167 239 252 85 74 249 146 175 188 207 118 211 218 20 156 184 77 157 192 227 71 140 218 70 100 119 12 125 20 172 2 20 198 142 207 100 227 245 62 181 184 1 11 2 226 224 47 150 18 175 65 226 216 115 142 247 152 3 122 106 13 16 102 121 255 156 124 7 254 39 11 251 200 255 71 145 249 190 167 246 121 41 219 243 17 203 151 67 177 240 140 127 89 162 7 180 95 9 203 61 3 239 57 38 149 187 34 158 195 38 23 101 47 188 36 164 168 8 16 202 217 138 0 103 158 251 186 206 7 246 83 18 155 202 131 162 42 21 62 17 168 239 28 30 26 203 212 212 236 44 214 178 153 128 174 198 130 82)

  assert (byteCount data) (count expected) 'count stored'
  match = true
  for i (byteCount data) {
    match = (and match ((byteAt data i) == (at expected i)))
  }
  assert match true 'stored result'

  return s
})

call (function {
  original = (newBinaryData ((* 1 1024 1024) + 123))

  i = 1
  more = true
  while more {
    for j 33 {
      for k 67 {
        if (i <= (byteCount original)) {
          byteAtPut original i ((j * k) % 256)
          i += 1
        } else {
          more = false
        }
      }
    }
  }

  strm = (dataStream (newBinaryData 100))
  s = (onZLib (new 'DeflateStream') strm)
  nextPutAll s original 1 (byteCount original)

  close s
  data = (contents s)

  expected100 = (array 120 94 229 214 127 120 8 4 194 7 112 132 150 102 134 209 98 102 73 179 24 205 143 155 97 199 210 184 17 222 165 209 44 177 24 141 118 110 231 87 75 218 187 180 112 90 181 208 72 171 91 154 231 150 164 113 203 139 195 45 188 154 72 203 171 90 90 154 59 135 87 98 39 197 235 22 111 207 243 254 221 189 119 247 254 113 207 219 231 175 239 243 125 190 207 247 255 79 227 38)

  expected5000 = (array 138 79 20 159 40 62 81 124 162 248 68 241 137 226 19 197 39 138 79 20 159 40 62 81 124 162 248 68 241 137 226 19 197 39 138 79 20 159 40 62 81 124 162 76 203 242 137 226 19 197 39 138 79 20 159 40 62 81 124 162 248 68 241 137 226 19 197 39 138 79 20 159 40 62 81 124 162 248 68 241 137 226 19 197 39 138 79 20 159 40 62 81 124 162 248 68 241 137 226 19)

  expectedLast = (array 162 248 68 241 137 226 19 197 39 138 79 20 159 40 62 81 124 162 248 68 241 137 226 19 197 39 138 79 20 159 40 62 81 124 162 248 68 241 137 226 19 197 39 138 79 20 159 40 62 81 124 162 248 68 241 137 226 19 197 39 138 79 20 159 40 62 81 124 162 248 68 241 137 226 19 197 39 138 79 20 159 40 62 81 124 162 248 68 249 186 225 159 70 148 255 6 81 58 117 110)

  assert (byteCount data) 11064 'count large'
  match = true
  for i 100 {
    match = (and match ((byteAt data i) == (at expected100 i)))
  }
  for ii 100 {
    i = (ii + 5000)
    match = (and match ((byteAt data i) == (at expected5000 ii)))
  }
  for ii 100 {
    i = ((ii + (byteCount data)) - 100)
    match = (and match ((byteAt data i) == (at expectedLast ii)))
  }

  assert match true 'large result'

  return s
})
