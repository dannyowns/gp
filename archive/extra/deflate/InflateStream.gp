defineClass InflateStream collection position limit dataStream bitPos bitBuf state litTable distTable MaxBits MinBits BlockTypes StateNewBlock StateNoMoreData BlockProceedBit FixedLitCodes debug ProcessDynamicBlockArray FixedLitTable FixedDistTable LiteralLengthMap DistanceMap

method init InflateStream {
  MaxBits = 16
  MinBits = 0
  StateNewBlock = 0
  StateNoMoreData = 1
  BlockProceedBit = 8

  BlockTypes = (array
                  'processStoredBlock'  // New block in stored format
                  'processFixedBlock'   // New block with fixed huffman tables
                  'processDynamicBlock' // New block with dynamic huffman tables
                  'errorBadBlock'       // Bad block format
                  'proceedStoredBlock'  // Continue block in stored format
                  'proceedFixedBlock'   // Continue block in fixed format
                  'proceedDynamicBlock' // Continue block in dynamic format
                  'errorBadBlock'       //Bad block format
               )

  ProcessDynamicBlockArray = (array 16 17 18 0 8 7 9 6 10 5 11 4 12 3 13 2 14 1 15)
  LiteralLengthMap = (array 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122 123 124 125 126 127 128 129 130 131 132 133 134 135 136 137 138 139 140 141 142 143 144 145 146 147 148 149 150 151 152 153 154 155 156 157 158 159 160 161 162 163 164 165 166 167 168 169 170 171 172 173 174 175 176 177 178 179 180 181 182 183 184 185 186 187 188 189 190 191 192 193 194 195 196 197 198 199 200 201 202 203 204 205 206 207 208 209 210 211 212 213 214 215 216 217 218 219 220 221 222 223 224 225 226 227 228 229 230 231 232 233 234 235 236 237 238 239 240 241 242 243 244 245 246 247 248 249 250 251 252 253 254 255 256 65539 65540 65541 65542 65543 65544 65545 65546 131083 131085 131087 131089 196627 196631 196635 196639 262179 262187 262195 262203 327747 327763 327779 327795 393347 393379 393411 393443 65794 0 0)
  DistanceMap = (array 1 2 3 4 65541 65543 131081 131085 196625 196633 262177 262193 327745 327777 393345 393409 459009 459137 524801 525057 590849 591361 657409 658433 724993 727041 794625 798721 868353 876545 0 0)
  FixedLitTable = (toUInt32Array (array 117440514 256 16777426 16777362 16777490 196639 16777458 16777394 33554762 65546 16777442 16777378 33554730 16777346 16777474 16777410 33554794 65542 16777434 16777370 33554714 262203 16777466 16777402 33554778 131089 16777450 16777386 33554746 16777354 16777482 16777418 33554810 65540 16777430 16777366 16777494 262187 16777462 16777398 33554770 131085 16777446 16777382 33554738 16777350 16777478 16777414 33554802 65544 16777438 16777374 33554722 327763 16777470 16777406 33554786 196631 16777454 16777390 33554754 16777358 16777486 16777422 33554818 65539 16777428 16777364 16777492 262179 16777460 16777396 33554766 131083 16777444 16777380 33554734 16777348 16777476 16777412 33554798 65543 16777436 16777372 33554718 327747 16777468 16777404 33554782 196627 16777452 16777388 33554750 16777356 16777484 16777420 33554814 65541 16777432 16777368 16777496 262195 16777464 16777400 33554774 131087 16777448 16777384 33554742 16777352 16777480 16777416 33554806 65545 16777440 16777376 33554726 327779 16777472 16777408 33554790 196635 16777456 16777392 33554758 16777360 16777488 16777424 33554822 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122 123 124 125 126 127 128 129 130 131 132 133 134 135 136 137 138 139 140 141 142 143 327795 393347 393379 393411 393443 65794 0 0 144 146 145 147 148 150 149 151 152 154 153 155 156 158 157 159 160 162 161 163 164 166 165 167 168 170 169 171 172 174 173 175 176 178 177 179 180 182 181 183 184 186 185 187 188 190 189 191 192 194 193 195 196 198 197 199 200 202 201 203 204 206 205 207 208 210 209 211 212 214 213 215 216 218 217 219 220 222 221 223 224 226 225 227 228 230 229 231 232 234 233 235 236 238 237 239 240 242 241 243 244 246 245 247 248 250 249 251 252 254 253 255))
  FixedDistTable = (toUInt32Array (array 83886082 1 459009 196625 724993 65541 590849 327745 868353 3 524801 262177 794625 131081 657409 393345 0 2 459137 196633 727041 65543 591361 327777 876545 4 525057 262193 798721 131085 658433 393409 0))

  debug = false
}

method onData InflateStream data {
  init this
  state = StateNewBlock
  if ((classOf data) == (class 'BinaryData')) {
    dataStream = (dataStream data true)
  } else {
    // assume it is a DataStream
    dataStream = data
  }
  collection = (newBinaryData 65536)
  position = 0
  limit = 0

  bitPos = 0
  bitBuf = 0

  return this
}

method onZLibData InflateStream data {
  onData this data
  method = (nextBits this 8)
  byte = (nextBits this 8)
//  print 'method' method byte
  return this
}

method atEnd InflateStream {
  // Note: It is possible that we have a few bits left,
  // representing just the EOB marker. To check for
  // this we must force decompression of the next
  // block if at end of data.
  if (and (position >= limit) (state == StateNoMoreData)) {return true}
  // Force decompression, by calling #next. Since #moveContentsToFront
  // will never move data to the beginning of the buffer it is safe to
  // skip back the read position afterwards
  // if (isNil (next this)) {return true}
  position += -1
  return false
}

method next InflateStream {
  if (and ((count collection) > 0) (position < limit)) {
    position += 1
    return (byteAt collection position)
  } else {
    return (pastEndRead this)
  }
}

method nextAll InflateStream asStringFlag {
  if (isNil asStringFlag) { asStringFlag = false }
  buf = (newBinaryData 1000)
  bufSize = (byteCount buf)
  byteCount = 0
  b = (next this)
  while (notNil b) {
    byteCount += 1
	if (byteCount > bufSize) {
	  newBuf = (newBinaryData (2 * bufSize))
	  replaceByteRange newBuf 1 bufSize buf
	  buf = newBuf
	  bufSize = (byteCount buf)
	}
	byteAtPut buf byteCount b
    b = (next this)
  }

  if asStringFlag {
	result = (stringFromByteRange buf 1 byteCount)
  } else {
    result = (newBinaryData byteCount)
    replaceByteRange result 1 byteCount buf
  }
  return result
}

method nextInto InflateStream bytes count start {
  for i count {
    c = (next this)
    byteAtPut bytes ((start + i) - 1) c
  }
  return bytes
}

method pastEndRead InflateStream {
  if (state == StateNoMoreData) {return} // Get out early if possible
  // Check if we can fetch more source data
  // moveSourceToFront this
  // Check if we can fetch more source data
  moveContentsToFront this

  if (state == StateNewBlock) {state = (getNextBlock this)}
  blockType = (state >> 1)
  bp = (bitPosition this)
  call (at BlockTypes (blockType + 1)) this
  // Note: if bit position hasn't advanced then nothing has been decoded.
  if (bp == (bitPosition this)) {
    error 'decode failed'
  }
  if (state == StateNoMoreData) {verifyCrc this}
  return (next this)
}

method moveContentsToFront InflateStream {
  if (limit > 32768) {
    delta = (limit - 32767)
    replaceByteRange collection 1 (((byteCount collection) - delta) + 1) collection delta
    position = ((position - delta) + 1)
    limit = ((limit - delta) + 1)
  }
}

method processStoredBlock InflateStream {
  // Skip to byte boundary
  nextBits this (bitPos & 7)
  length = (nextBits this 16)
  chkSum = (nextBits this 16)
  //if ((chkSum ^ 65535) == length) {
  //  error 'Bad block length'
  //}

  litTable = nil
  distTable = length
  state = (state ^ BlockProceedBit)
  return (proceedStoredBlock this)
}

method proceedStoredBlock InflateStream {
  if (notNil litTable) {error 'Bad state'}
  length = distTable
  while (and (length > 0) (limit < (byteCount collection)) (not (atEnd dataStream))) {
    limit += 1
    byteAtPut collection limit (nextUInt8 dataStream)
    length += -1
  }
  if (length == 0) {state = (state & StateNoMoreData)}
  decoded = (length - distTable)
  distTable = length
  return decoded
}

method nextBits InflateStream n {
  while (bitPos < n) {
    bitBuf = (bitBuf + ((nextByte this) << bitPos))
    bitPos = (bitPos + 8)
  }
  bits = (bitBuf & ((1 << n) - 1))
  bitBuf = (bitBuf >> n)
  bitPos = (bitPos - n)
  return bits
}

method getNextBlock InflateStream {
  return (nextBits this 3)
}

method bitPosition InflateStream {
  return (((position dataStream) * 8) + bitPos)
}

method nextByte InflateStream {
  return (nextUInt8 dataStream)
}

// method processFixedBlock InflateStream {
//   litTable = (huffmanTableFrom this FixedLitCodes)
//  distTable = (huffmanTableFrom this DistanceMap)
//  state = (state | BlockProceedBit)
//  proceedFixedBlock this
//}

method processFixedBlock InflateStream {
  litTable = FixedLitTable
  distTable = FixedDistTable
  state = (state | BlockProceedBit)
  proceedFixedBlock this
}

method proceedFixedBlock InflateStream {
  decompressDeflateData this (getField litTable 'data') (getField distTable 'data')
  // decompressBlock this litTable distTable
}

method processDynamicBlock InflateStream {
  nLit = ((nextBits this 5) + 257)
  nDist = ((nextBits this 5) + 1)
  nLen = ((nextBits this 4) + 4)
  codeLength = (newArray 19)
  fillArray codeLength 0
  for i nLen {
    bits = (at ProcessDynamicBlockArray i)
    atPut codeLength (bits + 1) (nextBits this 3)
  }
  lengthTable = (huffmanTableFrom this codeLength)
  codeLength = (decodeDynamicTable this lengthTable (nLit + nDist))
  litTable = (huffmanTableFrom this (copyArray codeLength nLit 1) LiteralLengthMap)
  distTable = (huffmanTableFrom this (copyArray codeLength ((count codeLength) - nLit) (nLit + 1)) DistanceMap)
  state = (state | BlockProceedBit)
  proceedDynamicBlock this
}

method proceedDynamicBlock InflateStream {
  decompressDeflateData this (getField litTable 'data') (getField distTable 'data')
  // decompressBlock this litTable distTable
}

method decompressBlock InflateStream lTable dTable {
// Process the compressed data in the block.
// llTable is the huffman table for literal/length codes
// and dTable is the huffman table for distance codes.
  while (and (limit < (byteCount collection)) ((getField dataStream 'position') <= (byteCount (getField dataStream 'data')))) {
    // Back up stuff if we're running out of space
    oldBits = bitBuf
    oldBitPos = bitPos
    oldPos = (position dataStream)
    value = (decodeValueFrom this lTable)
    if (value < 256) { // A literal
      limit += 1
      byteAtPut collection limit value
    } else {
      // length/distance or end of block
      if (value == 256) { // End of block
        state = (state & StateNoMoreData)
        return
      }
      // Compute the actual length value (including possible extra bits)
      extra = ((value >> 16) - 1)
      length = (value & 65535)
      if (extra > 0) {length += (nextBits this extra)}
      // Compute the distance value
      value = (decodeValueFrom this dTable)
      extra = (value >> 16)
      distance = (value & 65535)
      if (extra > 0) {distance += (nextBits this extra)}
      if ((limit + length) >= (byteCount collection)) {
        bitBuf = oldBits
        bitPos = oldBitPos
        setField dataStream 'position' oldPos
        return
      }
      for i length {
         byteAtPut collection (limit + i) (byteAt collection ((limit + i) - distance))
      }
      // replaceByteRange collection (limit + 1) (limit + length) collection ((limit - distance) + 1)
      limit += length
    }
  }
}

method decodeDynamicTable InflateStream aHuffmanTable nItems {
  values = (newArray nItems)
  index = 1
  theValue = 0
  while (index <= nItems) {
    value = (decodeValueFrom this aHuffmanTable)
    if (value < 16) {
      theValue = value
      atPut values index value
      index += 1
    } else {
      if (value == 16) {
        repCount = ((nextBits this 2) + 3)
      } else {
        theValue = 0
        if (value == 17) {
          repCount = ((nextBits this 3) + 3)
        } (value == 18) {
          repCount = ((nextBits this 7) + 11)
        } else {
          error 'Invalid bits tree value'
        }
      }
      for i repCount {
        atPut values ((index + i) - 1) theValue
      }
      index += repCount
    }
  }
  return values
}

method huffmanTableFrom InflateStream aCollection valueMap {
  minBits = (MaxBits + 1)
  maxBits = 0
  counts = (newArray (MaxBits + 1))
  fillArray counts 0
  for length aCollection {
    if (length > 0) {
      minBits = (min length minBits)
      maxBits = (max length maxBits)
      atPut counts (length + 1) ((at counts (length + 1)) + 1)
    }
  }
  if (maxBits == 0) {return nil}
  values = (computeHuffmanValues this aCollection counts minBits maxBits)
  mapValues this values valueMap
  return (createHuffmanTables this values counts minBits maxBits)
}

method computeHuffmanValues InflateStream aCollection counts minBits maxBits {
  offsets = (newArray maxBits)
  fillArray offsets 0
  baseOffset = 1
  bits = minBits
  while (bits <= maxBits) {
    atPut offsets bits baseOffset
    baseOffset += (at counts (bits + 1))
    bits += 1
  }
  values = (new 'UInt32Array' (newBinaryData (4 * (count aCollection))))
  for i (count aCollection) {
    codeLength = (at aCollection i)
    if (codeLength > 0) {
      baseOffset = (at offsets codeLength)
      atPut values baseOffset (i - 1)
      atPut offsets codeLength (baseOffset + 1)
    }
  }
  return values
}

method mapValues InflateStream values valueMap {
  if (isNil valueMap) {return values}
  for i (count values) {
    oldValue = (at values i)
    if (isNil oldValue) {return values}
    atPut values i (at valueMap (oldValue + 1))
  }
}

method createHuffmanTables InflateStream values counts minBits maxBits {
  table = (new 'UInt32Array' (newBinaryData (* 4 (max (4 << minBits) 16))))

  // Create the first entry - this is a dummy.
  // It gives us information about how many bits to fetch initially.
  atPut table 1 ((minBits << 24) + 2) // First actual table starts at index 2

  tableStart = 2 // See above
  tableSize = (1 << minBits)
  tableEnd = (tableStart + tableSize)
  // Store the terminal symbols
  valueIndex = (at counts (minBits + 1))
  tableIndex = 0
  for i valueIndex {
    atPut table (tableStart + tableIndex) (at values i)
    tableIndex = (incrementBits this tableIndex minBits)
  }
  // Fill up remaining entries with invalid entries
  tableStack = (list)
  add tableStack (array minBits tableStart tableIndex minBits tableSize)

  // Go to next value index
  valueIndex += 1
  bits = (minBits + 1)
  // Walk over remaining bit lengths and create new subtables
  while (bits <= maxBits) {
    numValues = (at counts (bits + 1))
    while (numValues > 0) { // Create a new subtable
      lastTable = (last tableStack)
      lastTableStart = (at lastTable 2)
      lastTableIndex = (at lastTable 3)
      deltaBits = (bits - (at lastTable 1))
      // Make up a table of deltaBits size
      tableSize = (1 << deltaBits)
      tableStart = tableEnd
      tableEnd = (tableEnd + tableSize)
      while (tableEnd > (count table)) {
        table = (growHuffmanTable this table)
      }
      // Connect to last table
      // assert (at table (lastTableStart + lastTableIndex)) 0 'Entry must be unused'
      atPut table (lastTableStart + lastTableIndex) (((toLargeInteger deltaBits) << 24) + tableStart)
      atPut lastTable 3 (incrementBits this lastTableIndex (at lastTable 4))
      atPut lastTable 5 ((at lastTable 5) - 1)
      // assert ((at lastTable 5) >= 0) true 'Don''t exceed tableSize'
      // Store terminal values
      maxEntries = (min numValues tableSize)
      tableIndex = 0
      for i maxEntries {
        atPut table (tableStart + tableIndex) (at values valueIndex)
        valueIndex += 1
        numValues += -1
        tableIndex = (incrementBits this tableIndex deltaBits)
      }
      // Check if we have filled up the current table completely
      if (maxEntries == tableSize) {
        while (and ((count tableStack) > 0) ((at (last tableStack) 5) == 0)) {
          removeLast tableStack
        }
      } else {
        add tableStack (array bits tableStart tableIndex deltaBits (tableSize - maxEntries))
      }
    }
    bits += 1
  }
  newData = (newBinaryData (4 * (tableEnd - 1)))
  replaceByteRange newData 1 (4 * (tableEnd - 1)) (getField table 'data') 1
  return (new 'UInt32Array' newData)
}

method growHuffmanTable InflateStream table {
  oldData = (getField table 'data')
  newData = (newBinaryData ((byteCount oldData) * 2))
  replaceByteRange newData 1 (byteCount oldData) oldData 1
  return (new 'UInt32Array' newData)
}

method decodeValueFrom InflateStream table {
  bitsNeeded = ((at table 1) >> 24)
  tableIndex = 2
  bits = (nextSingleBits this bitsNeeded)
  value = (at table (tableIndex + bits))
  test = ((value & 1056964608) == 0)
  while (not test) {
    tableIndex = (value & 65535)
    bitsNeeded = ((value >> 24) & 255)
    if (bitsNeeded > MaxBits) {
      error 'Invalid huffman table entry'
    }
    bits = (nextSingleBits this bitsNeeded)
    value = (at table (tableIndex + bits))
    test = ((value & 1056964608) == 0)
  }
  return value
}

method incrementBits InflateStream value nBits {
  // Increment value in reverse bit order, e.g. 
  // for a 3 bit value count as follows:
  //            000 / 100 / 010 / 110
  //            001 / 101 / 011 / 111
  //  See the class comment why we need this.
  result = value
  // Test the lowest bit first
  bit = (1 << (nBits - 1))
  // If the currently tested bit is set then we need to
  // turn this bit off and test the next bit right to it
  while ((result & bit) != 0) {
    // Turn off current bit
    result = (result ^ bit)
    // And continue testing the next bit
    bit = (bit >> 1)
  }
  // Turn on the right-most bit that we haven't touched in the loop above
  return (result ^ bit)
}

method nextSingleBits InflateStream n {
  return (nextBits this n)
}

method verifyCrc InflateStream {
  return true
}
