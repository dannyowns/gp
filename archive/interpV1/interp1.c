// This program tests the speed of displatching via a case statement
// with dispatching via procedure calls.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include "mem.h"

void *lookupPrim(char *primName);
int evalInt(OBJ obj);
OBJ primGetVar(CmdPtr block);
OBJ primAdd(CmdPtr block);

#define STOP 0
#define IF 1
#define REPEAT 2
#define WHILE 3
#define SETVAR 4
#define CHANGEBY 5
#define NOOP 6

// ***** Interpreter *****

OBJ vars[100];
OBJ flagsArray = nilObj;

void clearVars() { for (int i = 0; i < 100; i++) vars[i] = nilObj; }
void makeFlags() { flagsArray = newArray(10000); }

inline OBJ eval(OBJ obj) {
	// If obj is a Reporter block, return the result of evaluating it. Otherwise, return obj itself (a literal value).
	if (objClass(obj) != ReporterClass) return obj;

	ReporterPtr b = (ReporterPtr) obj;
	if (!b->prim) b->prim = lookupPrim(obj2cstr(b->primName));
	if (b->prim == primGetVar) {
		if (!b->cachedVarIndex) b->cachedVarIndex = b->args[0];
		return vars[obj2int(b->cachedVarIndex)];
	}
	return (OBJ) b->prim(b);
}

inline int evalInt(OBJ obj) {
	if (isInt(obj)) return obj2int(obj);
	OBJ value = eval(obj);
	if (isInt(value)) return obj2int(value);
	printf("evalInt got non-integer: %u\n", (unsigned) value);
	return 0;
}

void run(CmdPtr script) {
	int stack[1000];
	int sp = 0;
	CmdPtr b, nextBlock = script;
	int blockEndAction = STOP;
	int loopCounter = 0;
	int i;

	clearVars();
	makeFlags();
	while ((b = nextBlock) != (CmdPtr) nilObj) {
		nextBlock = (CmdPtr) b->nextBlock;
		if (!b->prim) b->prim = lookupPrim(obj2cstr(b->primName));
		if ((int) b->prim < 10) {
			switch ((int) b->prim) {
			case IF:
				if (eval(b->args[0]) == trueObj) {
					stack[++sp] = blockEndAction;
					stack[++sp] = (int) b;
					blockEndAction = IF;
					nextBlock = (CmdPtr) b->subStack;
				}
				break;
			case REPEAT:
				stack[++sp] = loopCounter;
				stack[++sp] = blockEndAction;
				stack[++sp] = (int) b;
				loopCounter = evalInt(b->args[0]); // loop counter
				blockEndAction = REPEAT;
				nextBlock = (CmdPtr) b->subStack;
				break;
			case WHILE:
				if (eval(b->args[0]) == trueObj) {
					stack[++sp] = blockEndAction;
					stack[++sp] = (int) b;
					blockEndAction = WHILE;
					nextBlock = (CmdPtr) b->subStack;
				}
				break;
			case SETVAR:
				if (!b->subStack) b->subStack = eval(b->args[0]);
				vars[obj2int(b->subStack)] = eval(b->args[1]);
				break;
			case CHANGEBY:
				if (!b->subStack) b->subStack = eval(b->args[0]);
				OBJ oldValue = vars[obj2int(b->subStack)];
				if (isInt(oldValue)) vars[obj2int(b->subStack)] = int2obj(obj2int(oldValue) + evalInt(b->args[1]));
				break;
			case NOOP:
				eval(b->args[0]);
				break;
			}
		} else  b->prim(b);

		while ((nextBlock == (CmdPtr) nilObj) && blockEndAction) {
			CmdPtr controlBlock = (CmdPtr) stack[sp];
			switch (blockEndAction) {
				case IF:
					nextBlock = (CmdPtr) controlBlock->nextBlock;
					blockEndAction = stack[sp - 1];
					sp -= 2;
					break;
				case REPEAT:
					if (--loopCounter > 0) { // restart loop body
						nextBlock = (CmdPtr) controlBlock->subStack;
					} else { // exit the loop
						blockEndAction = stack[sp - 1];
						loopCounter = stack[sp - 2];
						sp -= 3;
						nextBlock = (CmdPtr) controlBlock->nextBlock;
					}
					break;
				case WHILE:
					if (eval(controlBlock->args[0]) == trueObj) { // restart loop body if condition is true
						nextBlock = (CmdPtr) controlBlock->subStack;
					} else { // exit the loop
						blockEndAction = stack[sp - 1];
						sp -= 2;
						nextBlock = (CmdPtr) controlBlock->nextBlock;
					}
					break;
				default:
					blockEndAction = 0;
					return;
			}
		}
	}
}

// ***** Variable Primitives *****

OBJ primGetVar(CmdPtr block) {
	return vars[obj2int(block->args[0])];
}

void primSetVar(CmdPtr block) {
	vars[obj2int(block->args[0])] = eval(block->args[1]);
}

void primChangeVarBy(CmdPtr block) {
	int i = obj2int(block->args[0]);
	OBJ oldValue = vars[i];
	if (isInt(oldValue)) vars[i] = int2obj(obj2int(oldValue) + evalInt(block->args[1]));
}

// ***** Print Primitives *****

void primPrint(CmdPtr block) {
	OBJ val = eval(block->args[0]);
	if (isInt(val)) printf("%d", obj2int(val));
	else if (val == nilObj)  printf("nil");
	else if (val == trueObj)  printf("true");
	else if (val == falseObj)  printf("false");
	else if (objClass(val) == StringClass) {
		printf("%s", obj2cstr(val));
	} else {
		printf("OBJ(%d)", (int) val);
	}
	printf("\n");
}

// ***** Array Primitives *****

OBJ primFlagsAt(CmdPtr block) { return arrayAt(flagsArray, evalInt(block->args[0])); }
void primFlagsAtPut(CmdPtr block) { arrayAtPut(flagsArray, evalInt(block->args[0]), eval(block->args[1])); }
void primFlagsAtAllPut(CmdPtr block) { arrayAtAllPut(flagsArray, eval(block->args[0])); }

// ***** Operators *****

OBJ primAdd(CmdPtr block) { return int2obj(evalInt(block->args[0]) + evalInt(block->args[1])); }
OBJ primLess(CmdPtr block) { return (evalInt(block->args[0]) < evalInt(block->args[1])) ? trueObj : falseObj; }

// ***** Primitive Lookup *****

void primNoop(CmdPtr block) {}

void *lookupPrim(char *primName) {
	// Control structures
	if (strcmp("if", primName) == 0) return (void *) IF;
	if (strcmp("repeat", primName) == 0) return (void *) REPEAT;
	if (strcmp("while", primName) == 0) return (void *) WHILE;
	if (strcmp("internalnoop", primName) == 0) return (void *) NOOP;

	if (strcmp("noop", primName) == 0) return primNoop;

	if (strcmp("+", primName) == 0) return primAdd;
	if (strcmp("<", primName) == 0) return primLess;

	if (strcmp("v", primName) == 0) return primGetVar;
	if (strcmp("setvar", primName) == 0) return (void *) SETVAR; // return primSetVar;
	if (strcmp("changeBy", primName) == 0) return (void *) CHANGEBY; // return primChangeVarBy;

	if (strcmp("print", primName) == 0) return primPrint;
	
	if (strcmp("flagsAt", primName) == 0) return primFlagsAt;
	if (strcmp("flagsAtPut", primName) == 0) return primFlagsAtPut;
	if (strcmp("flagsAtAllPut", primName) == 0) return primFlagsAtAllPut;

	printf("Missing primitive: %s\n", primName);
	return primNoop;
}
