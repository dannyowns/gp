to aRandomColor {
  color = (randomColor)
  setField color 'a' (rand 128 200)
  return color
}

to run {
  openWindow
  bm = (newBitmap 500 500)
//  pen = (new 'Pen')
  pen = (new 'GeziraPen')
  setCanvas pen bm

  initFrame pen
  clearImage pen
  drawBlock pen 100 20 89 30 (color 74 108 212 255) 3 6 12 1 

  drawCSlot pen 300 20 89 30 (color 230 168 34) 3 6 12 1

  drawReporter pen 100 50 90 30 (color 230 168 34) 12 3

  drawResizer pen 0 0 18 18 'free'

  finishFrame pen

  clearBuffer
  drawBitmap nil bm
  flipBuffer

  sleep 10000
}

run
