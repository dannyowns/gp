#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "mem.h"
#include "interp.h"
#include "oop.h"

#define NILE_INCLUDE_PROCESS_API
#include "nile.h"
#include "gezira.h"
#include "gezira-image.h"

#ifndef MAXFLOAT
#define MAXFLOAT 0x1.fffffep+127f
#endif

#define NBYTES_PER_THREAD 1000000

static nile_Process_t *gezira = NULL;
static nile_Process_t *lastProcess = NULL;
static nile_Process_t *futureGate = NULL;

static void
finalizeImage(OBJ ref) {
	if (NOT_CLASS(ref, ExternalReferenceClass)) return;
	gezira_Image_t *img = (gezira_Image_t *)*(ADDR*)BODY(ref);
	if (img) {
		free(img);
		*(ADDR*)BODY(ref) = NULL;
	}
}

static OBJ
primShutDownGezira() {
	if (!gezira) return nilObj;
	nile_sync(gezira);
	free(nile_shutdown(gezira));
	gezira = NULL;
	return nilObj;
}

static OBJ
primInitializeGezira(int nargs, OBJ args[]) {
	if (gezira) return nilObj;
	int nThreads = obj2int(args[0]);
	int mem_size = nThreads * NBYTES_PER_THREAD;
	gezira = nile_startup(malloc (mem_size), mem_size, nThreads);
	return nilObj;
}

static OBJ
primAllocateGeziraImage(int nargs, OBJ args[]) {
	OBJ bitmap = args[0];
	int width = obj2int(FIELD(bitmap, 0));
	int height = obj2int(FIELD(bitmap, 1));
	OBJ pixelData = FIELD(bitmap, 2);

	gezira_Image_t *img = malloc(sizeof(gezira_Image_t));
	gezira_Image_init(img, (void*)BODY(pixelData), width, height, width);
	OBJ imgRef = newBinaryObj(ExternalReferenceClass, ExternalReferenceWords);

	ADDR* a = (ADDR*)BODY(imgRef);
	a[0] = (ADDR)img;
	a[1] = (ADDR)finalizeImage;
	gezira_Image_reset_gate(img);
	return imgRef;
}

static OBJ
primSetGezizaImage(int nargs, OBJ args[]) {
	if (nargs < 2) return primFailed("Not enough arguments");
	OBJ imgRef = args[0];
	ADDR *a = (ADDR*)BODY(imgRef);
	gezira_Image_t *img = (gezira_Image_t *)*a;

	OBJ bitmap = args[1];
	int width = obj2int(FIELD(bitmap, 0));
	int height = obj2int(FIELD(bitmap, 1));
	OBJ pixelData = FIELD(bitmap, 2);

	gezira_Image_init(img, (void*)BODY(pixelData), width, height, width);
	gezira_Image_reset_gate(img);
	return imgRef;
}

static OBJ
primGeziraImageExtent(int nargs, OBJ args[]) {
	OBJ imgRef = args[0];
	gezira_Image_t *img = (gezira_Image_t *)*(ADDR*)BODY(imgRef);
	OBJ result = newArray(2);
	FIELD(result, 0) = int2obj(img->width);
	FIELD(result, 1) = int2obj(img->height);
	return result;
}

static OBJ
primGeziraSync(int nargs, OBJ args[]) {
	if (!gezira) return nilObj;
	nile_sync(gezira);
	return nilObj;
}

static nile_Process_t *build(OBJ proc);

static nile_Process_t*
pipeline(int nargs, OBJ args[]) {
	nile_Process_t *buffer[32];
	nile_Process_t **ptr = &buffer[0];

	for (int i = 0; i < nargs; i++) {
		nile_Process_t* proc = build(args[i]);
		lastProcess = proc;
		if (!proc && !futureGate) {
			primFailed("Not a gezira process");
			return NULL;
		}
		if (!IS_CLASS(FIELD(args[i], 0), ExternalReferenceClass)) {
			OBJ ext = newBinaryObj(ExternalReferenceClass, ExternalReferenceWords);
			FIELD(args[i], 0) = ext;
		}
		if (futureGate) {
			*(ADDR*)BODY(FIELD(args[i], 0)) = (ADDR)futureGate;
		} else {
			*ptr++ = proc;
			*(ADDR*)BODY(FIELD(args[i], 0)) = (ADDR)proc;
		}
	}
	return nile_Process_pipe_v(buffer, ptr - &buffer[0]);
}

static nile_Process_t *
build(OBJ proc) {
	futureGate = NULL;
	char *name = getClassName(proc);

	if (IS_CLASS(proc, ArrayClass)) {
		int nargs = objWords(proc);
		ADDR args = BODY(proc);
		return pipeline(nargs, args);
	}
	if (strcmp("TransformBeziers", name) == 0) {
		float *m = (float*)BODY(FIELD(proc, 1));
		return gezira_TransformBeziers (gezira, m[0], m[1], m[2], m[3], m[4], m[5]);
	}
	if (strcmp("ClipBeziers", name) == 0) {
		float *min = (float*)BODY(FIELD(proc, 1));
		float *max = (float*)BODY(FIELD(proc, 2));
		return gezira_ClipBeziers(gezira, min[0], min[1], max[0], max[1]);
	}
	if (strcmp("CalculateBounds", name) == 0) {
		return gezira_CalculateBounds(gezira);
	}
	if (strcmp("DecomposeBeziers", name) == 0) {
		return gezira_DecomposeBeziers(gezira);
	}
	if (strcmp("CombineEdgeSamples", name) == 0) {
		return gezira_CombineEdgeSamples(gezira);
	}
	if (strcmp("Rasterize", name) == 0) {
		return gezira_Rasterize(gezira);
	}
	if (strcmp("RectangleSpans", name) == 0) {
		float *min = (float*)BODY(FIELD(proc, 1));
		float *max = (float*)BODY(FIELD(proc, 2));
		return gezira_RectangleSpans(gezira, min[0], min[1], max[0], max[1]);
	}
	if (strcmp("TransformPoints", name) == 0) {
		float *m = (float*)BODY(FIELD(proc, 1));
		return gezira_TransformPoints(gezira, m[0], m[1], m[2], m[3], m[4], m[5]);
	}
	if (strcmp("PadTexture", name) == 0) {
		float *p = (float*)BODY(FIELD(proc, 1));
		return gezira_PadTexture(gezira, p[0], p[1]);
	}
	if (strcmp("RepeatTexture", name) == 0) {
		float *r = (float*)BODY(FIELD(proc, 1));
		return gezira_RepeatTexture(gezira, r[0], r[1]);
	}
	if (strcmp("ReflectTexture", name) == 0) {
		float *r = (float*)BODY(FIELD(proc, 1));
		return gezira_ReflectTexture(gezira, r[0], r[1]);
	}
	if (strcmp("UniformColor", name) == 0) {
		float *c = (float*)BODY(FIELD(proc, 1));
		return gezira_UniformColor(gezira, c[0], c[1], c[2], c[3]);
	}
	if (strcmp("CompositeTextures", name) == 0) {
		nile_Process_t *t1 = build(FIELD(proc, 1));
		nile_Process_t *t2 = build(FIELD(proc, 2));
		nile_Process_t *c = build(FIELD(proc, 3));
		return gezira_CompositeTextures(gezira, t1, t2, c);
	}
	if (strcmp("ExpandSpans", name) == 0) {
		return gezira_ExpandSpans(gezira);
	}
	if (strcmp("ExtractSamplePoints", name) == 0) {
		return gezira_ExtractSamplePoints(gezira);
	}
	if (strcmp("ApplyTexture", name) == 0) {
		nile_Process_t *t = build(FIELD(proc, 1));
		return gezira_ApplyTexture(gezira, t);
	}
	if (strcmp("CompositeClear", name) == 0) {
		return gezira_CompositeClear(gezira);
	}
	if (strcmp("CompositeSrc", name) == 0) {
		return gezira_CompositeSrc(gezira);
	}
	if (strcmp("CompositeDst", name) == 0) {
		return gezira_CompositeDst(gezira);
	}
	if (strcmp("CompositeOver", name) == 0) {
		return gezira_CompositeOver(gezira);
	}
	if (strcmp("CompositeDstOver", name) == 0) {
		return gezira_CompositeDstOver(gezira);
	}
	if (strcmp("CompositeSrcIn", name) == 0) {
		return gezira_CompositeSrcIn(gezira);
	}
	if (strcmp("CompositeDstIn", name) == 0) {
		return gezira_CompositeDstIn(gezira);
	}
	if (strcmp("CompositeSrcOut", name) == 0) {
		return gezira_CompositeSrcOut(gezira);
	}
	if (strcmp("CompositeDstOut", name) == 0) {
		return gezira_CompositeDstOut(gezira);
	}
	if (strcmp("CompositeSrcAtop", name) == 0) {
		return gezira_CompositeSrcAtop(gezira);
	}
	if (strcmp("CompositeDstAtop", name) == 0) {
		return gezira_CompositeDstAtop(gezira);
	}
	if (strcmp("CompositeXor", name) == 0) {
		return gezira_CompositeXor(gezira);
	}
	if (strcmp("CompositePlus", name) == 0) {
		return gezira_CompositePlus(gezira);
	}
	if (strcmp("CompositeMultiply", name) == 0) {
		return gezira_CompositeMultiply(gezira);
	}
	if (strcmp("CompositeScreen", name) == 0) {
		return gezira_CompositeScreen(gezira);
	}
	if (strcmp("CompositeOverlay", name) == 0) {
		return gezira_CompositeOverlay(gezira);
	}
	if (strcmp("CompositeDarken", name) == 0) {
		return gezira_CompositeDarken(gezira);
	}
	if (strcmp("CompositeLighten", name) == 0) {
		return gezira_CompositeLighten(gezira);
	}
	if (strcmp("CompositeColorDodge", name) == 0) {
		return gezira_CompositeColorDodge(gezira);
	}
	if (strcmp("CompositeColorBurn", name) == 0) {
		return gezira_CompositeColorBurn(gezira);
	}
	if (strcmp("CompositeHardLight", name) == 0) {
		return gezira_CompositeHardLight(gezira);
	}
	if (strcmp("CompositeSoftLight", name) == 0) {
		return gezira_CompositeSoftLight(gezira);
	}
	if (strcmp("CompositeDifference", name) == 0) {
		return gezira_CompositeDifference(gezira);
	}
	if (strcmp("CompositeExclusion", name) == 0) {
		return gezira_CompositeExclusion(gezira);
	}
	if (strcmp("CompositeSubtract", name) == 0) {
		return gezira_CompositeSubtract(gezira);
	}
	if (strcmp("CompositeInvert", name) == 0) {
		return gezira_CompositeInvert(gezira);
	}
	if (strcmp("InverseOver", name) == 0) {
		float *r = (float*)BODY(FIELD(proc, 1));
		return gezira_InverseOver(gezira, r[0]);
	}
	if (strcmp("ContrastiveOver", name) == 0) {
		float *r = (float*)BODY(FIELD(proc, 1));
		return gezira_ContrastiveOver(gezira, r[0]);
	}
	if (strcmp("LinearGradient", name) == 0) {
		float *s = (float*)BODY(FIELD(proc, 1));
		float *e = (float*)BODY(FIELD(proc, 2));
		return gezira_LinearGradient(gezira, s[0], s[1], e[0], e[1]);
	}
	if (strcmp("RadialGradient", name) == 0) {
		float *c = (float*)BODY(FIELD(proc, 1));
		float *r = (float*)BODY(FIELD(proc, 2));
		return gezira_RadialGradient(gezira, c[0], c[1], r[0]);
	}
	if (strcmp("PadGradient", name) == 0) {
		return gezira_PadGradient(gezira);
	}
	if (strcmp("RepeatGradient", name) == 0) {
		return gezira_RepeatGradient(gezira);
	}
	if (strcmp("ReflectGradient", name) == 0) {
		return gezira_ReflectGradient(gezira);
	}
	if (strcmp("ColorSpansBegin", name) == 0) {
		return gezira_ColorSpansBegin(gezira);
	}
	if (strcmp("ColorSpan", name) == 0) {
		float *s = (float*)BODY(FIELD(proc, 1));
		float *e = (float*)BODY(FIELD(proc, 2));
		float *r = (float*)BODY(FIELD(proc, 3));
		return gezira_ColorSpan(gezira, s[0], s[1], s[2], s[3], e[0], e[1], e[2], e[3], r[0]);
	}
	if (strcmp("ColorSpansEnd", name) == 0) {
		return gezira_ColorSpansEnd(gezira);
	}
	if (strcmp("ApplyColorSpans", name) == 0) {
		nile_Process_t *t = build(FIELD(proc, 1));
		return gezira_ApplyColorSpans(gezira, t);
	}
	if (strcmp("OffsetBezier", name) == 0) {
		float *o = (float*)BODY(FIELD(proc, 1));
		float *b = (float*)BODY(FIELD(proc, 2));
		return gezira_OffsetBezier(gezira, o[0], b[0], b[1], b[2], b[3], b[4], b[5]);
	}
	if (strcmp("MiterJoin", name) == 0) {
		float *o = (float*)BODY(FIELD(proc, 1));
		float *l = (float*)BODY(FIELD(proc, 2));
		float *p = (float*)BODY(FIELD(proc, 3));
		float *u = (float*)BODY(FIELD(proc, 4));
		float *v = (float*)BODY(FIELD(proc, 5));
		return gezira_MiterJoin(gezira, o[0], l[0], p[0], p[1], u[0], u[1], v[0], v[1]);
	}
	if (strcmp("RoundJoin", name) == 0) {
		float *o = (float*)BODY(FIELD(proc, 1));
		float *p = (float*)BODY(FIELD(proc, 2));
		float *u = (float*)BODY(FIELD(proc, 3));
		float *v = (float*)BODY(FIELD(proc, 4));
		return gezira_RoundJoin(gezira, o[0], p[0], p[1], u[0], u[1], v[0], v[1]);
	}
	if (strcmp("JoinBeziers", name) == 0) {
		float *o = (float*)BODY(FIELD(proc, 1));
		float *l = (float*)BODY(FIELD(proc, 2));
		float *b1 = (float*)BODY(FIELD(proc, 3));
		float *b2 = (float*)BODY(FIELD(proc, 4));
		return gezira_JoinBeziers(gezira, o[0], l[0], b1[0], b1[1], b1[2], b1[3], b1[4], b1[5], b2[0], b2[1], b2[2], b2[3], b2[4], b2[5]);
	}
	if (strcmp("CapBezier", name) == 0) {
		float *o = (float*)BODY(FIELD(proc, 1));
		float *l = (float*)BODY(FIELD(proc, 2));
		float *b = (float*)BODY(FIELD(proc, 3));
		return gezira_CapBezier(gezira, o[0], l[0], b[0], b[1], b[2], b[3], b[4], b[5]);
	}
	if (strcmp("OffsetAndJoin", name) == 0) {
		float *o = (float*)BODY(FIELD(proc, 1));
		float *l = (float*)BODY(FIELD(proc, 2));
		float *c = (float*)BODY(FIELD(proc, 3));
		float *b1 = (float*)BODY(FIELD(proc, 4));
		float *b2 = (float*)BODY(FIELD(proc, 5));
		return gezira_OffsetAndJoin(gezira, o[0], l[0], c[0], b1[0], b1[1], b1[2], b1[3], b1[4], b1[5], b2[0], b2[1], b2[2], b2[3], b2[4], b2[5]);
	}
	if (strcmp("StrokeOneSide", name) == 0) {
		float *w = (float*)BODY(FIELD(proc, 1));
		float *l = (float*)BODY(FIELD(proc, 2));
		float *c = (float*)BODY(FIELD(proc, 3));
		return gezira_StrokeOneSide(gezira, w[0], l[0], c[0]);
	}
	if (strcmp("ReverseBeziers", name) == 0) {
		return gezira_ReverseBeziers(gezira);
	}
	if (strcmp("SanitizeBezierPath", name) == 0) {
		return gezira_SanitizeBezierPath(gezira);
	}
	if (strcmp("StrokeBezierPath", name) == 0) {
		float *w = (float*)BODY(FIELD(proc, 1));
		float *l = (float*)BODY(FIELD(proc, 2));
		float *c = (float*)BODY(FIELD(proc, 3));
		return gezira_StrokeBezierPath(gezira, w[0], l[0], c[0]);
	}
	if (strcmp("SumWeightedColors", name) == 0) {
		float *n = (float*)BODY(FIELD(proc, 1));
		return gezira_SumWeightedColors(gezira, n[0]);
	}
	if (strcmp("BilinearFilterPoints", name) == 0) {
		return gezira_BilinearFilterPoints(gezira);
	}
	if (strcmp("BilinearFilterWeights", name) == 0) {
		return gezira_BilinearFilterWeights(gezira);
	}
	if (strcmp("BilinearFilter", name) == 0) {
		nile_Process_t *t = build(FIELD(proc, 1));
		return gezira_BilinearFilter(gezira, t);
	}
	if (strcmp("BicubicFilterPoints", name) == 0) {
		return gezira_BicubicFilterPoints(gezira);
	}
	if (strcmp("BicubicFilterDeltas", name) == 0) {
		return gezira_BicubicFilterDeltas(gezira);
	}
	if (strcmp("BicubicFilterWeights", name) == 0) {
		return gezira_BicubicFilterWeights(gezira);
	}
	if (strcmp("BicubicFilter", name) == 0) {
		nile_Process_t *t = build(FIELD(proc, 1));
		return gezira_BicubicFilter(gezira, t);
	}
	if (strcmp("GaussianBlur5x1Points", name) == 0) {
		return gezira_GaussianBlur5x1Points(gezira);
	}
	if (strcmp("GaussianBlur1x5Points", name) == 0) {
		return gezira_GaussianBlur1x5Points(gezira);
	}
	if (strcmp("GaussianBlur5x1Weights", name) == 0) {
		float *w = (float*)BODY(FIELD(proc, 1));
		return gezira_GaussianBlur5x1Weights(gezira, w[0]);
	}
	if (strcmp("GaussianBlur5x1", name) == 0) {
		float *f = (float*)BODY(FIELD(proc, 1));
		nile_Process_t *t = build(FIELD(proc, 2));
		return gezira_GaussianBlur5x1(gezira, f[0], t);
	}
	if (strcmp("GaussianBlur1x5", name) == 0) {
		float *f = (float*)BODY(FIELD(proc, 1));
		nile_Process_t *t = build(FIELD(proc, 2));
		return gezira_GaussianBlur1x5(gezira, f[0], t);
	}
	if (strcmp("GaussianBlur11x1Points", name) == 0) {
		return gezira_GaussianBlur11x1Points(gezira);
	}
	if (strcmp("GaussianBlur1x11Points", name) == 0) {
		return gezira_GaussianBlur1x11Points(gezira);
	}
	if (strcmp("GaussianBlur11x1Weights", name) == 0) {
		float *w = (float*)BODY(FIELD(proc, 1));
		return gezira_GaussianBlur11x1Weights(gezira, w[0]);
	}
	if (strcmp("GaussianBlur11x1", name) == 0) {
		float *f = (float*)BODY(FIELD(proc, 1));
		nile_Process_t *t = build(FIELD(proc, 2));
		return gezira_GaussianBlur11x1(gezira, f[0], t);
	}
	if (strcmp("GaussianBlur1x11", name) == 0) {
		float *f = (float*)BODY(FIELD(proc, 1));
		nile_Process_t *t = build(FIELD(proc, 2));
		return gezira_GaussianBlur1x11(gezira, f[0], t);
	}
	if (strcmp("GaussianBlur21x1Points", name) == 0) {
		return gezira_GaussianBlur21x1Points(gezira);
	}
	if (strcmp("GaussianBlur1x21Points", name) == 0) {
		return gezira_GaussianBlur1x21Points(gezira);
	}
	if (strcmp("GaussianBlur21x1Weights", name) == 0) {
		float *w = (float*)BODY(FIELD(proc, 1));
		return gezira_GaussianBlur21x1Weights(gezira, w[0]);
	}
	if (strcmp("GaussianBlur21x1", name) == 0) {
		float *f = (float*)BODY(FIELD(proc, 1));
		nile_Process_t *t = build(FIELD(proc, 2));
		return gezira_GaussianBlur21x1(gezira, f[0], t);
	}
	if (strcmp("GaussianBlur1x21", name) == 0) {
		float *f = (float*)BODY(FIELD(proc, 1));
		nile_Process_t *t = build(FIELD(proc, 2));
		return gezira_GaussianBlur1x21(gezira, f[0], t);
	}
	if (strcmp("CompositeUniformColorOverImageARGB32", name) == 0) {
		gezira_Image_t *t = (gezira_Image_t *)*(ADDR*)BODY(FIELD(proc, 1));
		float *c = (float*)BODY(FIELD(proc, 2));
		return gezira_CompositeUniformColorOverImage_ARGB32(gezira, t, c[0], c[1], c[2], c[3]);
	}
	if (strcmp("CompositeUniformColorOverImageARGB32Flipped", name) == 0) {
		gezira_Image_t *t = (gezira_Image_t *)*(ADDR*)BODY(FIELD(proc, 1));
		float *c = (float*)BODY(FIELD(proc, 2));
		return gezira_CompositeUniformColorOverImage_ARGB32_Flipped(gezira, t, c[0], c[1], c[2], c[3]);
	}
	if (strcmp("ReadFromImageARGB32", name) == 0) {
	  gezira_Image_t *t = (gezira_Image_t *)*(ADDR*)BODY(FIELD(proc, 1));
		return gezira_ReadFromImage_ARGB32(gezira, t, 1);
	}
	if (strcmp("ReadFromImageARGB32Flipped", name) == 0) {
	  gezira_Image_t *t = (gezira_Image_t *)*(ADDR*)BODY(FIELD(proc, 1));
		return gezira_ReadFromImage_ARGB32_Flipped(gezira, t, 1);
	}
	if (strcmp("WriteToImageARGB32", name) == 0) {
	  gezira_Image_t *t = (gezira_Image_t *)*(ADDR*)BODY(FIELD(proc, 1));
		return gezira_WriteToImage_ARGB32(gezira, t);
	}
	if (strcmp("WriteToImageARGB32Flipped", name) == 0) {
	  gezira_Image_t *t = (gezira_Image_t *)*(ADDR*)BODY(FIELD(proc, 1));
		return gezira_WriteToImage_ARGB32_Flipped(gezira, t);
	}
	if (strcmp("FutureGate", name) == 0) {
		futureGate = nile_Identity(gezira, 8);
		nile_Process_gate(lastProcess, futureGate);
		return NULL;
	}
	if (strcmp("Gate", name) == 0) {
		return (nile_Process_t *)*(ADDR*)BODY(FIELD(proc, 0));
	}
	fprintf(stderr, "unknown process name: %s\n", name);
	return NULL;
}

static OBJ
primPipeline(int nargs, OBJ args[]) {
	if (nargs < 2) return primFailed("Not enough arguments");
	if (!gezira) return primFailed("Gezira is not initialized");
	pipeline(nargs, args);
	return args[0];
}

static OBJ
primFeed(int nargs, OBJ args[]) {
	if (nargs < 2) return primFailed("Not enough arguments");
	if (!gezira) return primFailed("Gezira is not initialized");

	OBJ top = FIELD(args[0], 0);
	if (!IS_CLASS(top, ExternalReferenceClass)) {
		return primFailed("Not a Nile Process");
	}
	ADDR *a = (ADDR*)BODY(top);
	nile_Process_t *pipe = (nile_Process_t *)*a;
	
	OBJ geometry = args[1];
	if (!IS_CLASS(geometry, BinaryDataClass)) {
		return primFailed("Not geometry data");
	}
	
	nile_Process_feed(pipe, (float*)BODY(geometry), objWords(geometry));
	return nilObj;
}

static OBJ
primInverseMatrix2x3Into(int nargs, OBJ args[]) {
	if (nargs < 2) return primFailed("Not enough arguments");
	if (!IS_CLASS(args[0], BinaryDataClass) || objWords(args[0]) < 6 ||
	    !IS_CLASS(args[1], BinaryDataClass) || objWords(args[1]) < 6) {
		return primFailed("Not a matrix");
	}
	float *m = (float*)BODY(args[0]);
	float a = m[0], b = m[1], c = m[2], d = m[3], e = m[4], f = m[5];
	float *r = (float*)BODY(args[1]);

        float det = a * d - b * c;
	if (det == 0) {
		memset(r, 0, sizeof(float) * 6);
		return args[1];
	}
	float n = 1 / det;
	r[0] =  n * d;
	r[1] = -n * b;
	r[2] = -n * c;
	r[3] =  n * a;
	r[4] =  n * (f * c - d * e);
	r[5] = -n * (f * a - b * e);
	return args[1];
}

static OBJ
primRotateMatrix2x3Into(int nargs, OBJ args[]) {
	if (nargs < 3) return primFailed("Not enough arguments");
	if (!IS_CLASS(args[0], BinaryDataClass) || objWords(args[0]) < 6 ||
	    !IS_CLASS(args[2], BinaryDataClass) || objWords(args[2]) < 6) {
		return primFailed("Not a matrix");
	}
	float *m = (float*)BODY(args[0]);
	double t = (evalFloat(args[1]) / 180.0) * M_PI;
	float a = m[0], b = m[1], c = m[2], d = m[3], e = m[4], f = m[5];
	float *r = (float*)BODY(args[2]);

	r[0] = a *  cos(t) + c * sin(t);
	r[1] = b *  cos(t) + d * sin(t);
	r[2] = a * -sin(t) + c * cos(t);
	r[3] = b * -sin(t) + d * cos(t);
	r[4] = e;
	r[5] = f;
	return args[2];
}

static OBJ
primScaleMatrix2x3Into(int nargs, OBJ args[]) {
	if (nargs < 4) return primFailed("Not enough arguments");
	if (!IS_CLASS(args[0], BinaryDataClass) || objWords(args[0]) < 6 ||
	    !IS_CLASS(args[3], BinaryDataClass) || objWords(args[3]) < 6) {
		return primFailed("Not a matrix");
	}
	double sx = evalFloat(args[1]);
	double sy = evalFloat(args[2]);
	float *m = (float*)BODY(args[0]);
	float a = m[0], b = m[1], c = m[2], d = m[3], e = m[4], f = m[5];
	float *r = (float*)BODY(args[3]);

	r[0] = a * sx;
	r[1] = b * sy;
	r[2] = c * sx;
	r[3] = d * sy;
	r[4] = e;
	r[5] = f;
	return args[3];
}

static OBJ
primTranslateMatrix2x3Into(int nargs, OBJ args[]) {
	if (nargs < 4) return primFailed("Not enough arguments");
	if (!IS_CLASS(args[0], BinaryDataClass) || objWords(args[0]) < 6 ||
	    !IS_CLASS(args[3], BinaryDataClass) || objWords(args[3]) < 6) {
		return primFailed("Not a matrix");
	}
	double x = evalFloat(args[1]);
	double y = evalFloat(args[2]);
	float *m = (float*)BODY(args[0]);
	float a = m[0], b = m[1], c = m[2], d = m[3], e = m[4], f = m[5];
	float *r = (float*)BODY(args[3]);

	r[0] = a;
	r[1] = b;
	r[2] = c;
	r[3] = d;
	r[4] = a * x + c * y + e;
	r[5] = b * x + d * y + f;
	return args[3];
}

static OBJ
primComposedMatrix2x3WithLocalInto(int nargs, OBJ args[]) {
	if (nargs < 3) return primFailed("Not enough arguments");
	if (!IS_CLASS(args[0], BinaryDataClass) || objWords(args[0]) < 6 ||
	    !IS_CLASS(args[1], BinaryDataClass) || objWords(args[1]) < 6 ||
	    !IS_CLASS(args[2], BinaryDataClass) || objWords(args[2]) < 6) {
		return primFailed("Not a matrix");
	}
	float *m = (float*)BODY(args[0]);
	float a11 = m[0], a12 = m[1], a21 = m[2], a22 = m[3], a31 = m[4], a32 = m[5];

	m = (float*)BODY(args[1]);
	float b11 = m[0], b12 = m[1], b21 = m[2], b22 = m[3], b31 = m[4], b32 = m[5];

	float *r = (float*)BODY(args[2]);

	r[0] = a11 * b11 + a21 * b12;
	r[1] = a12 * b11 + a22 * b12;
	r[2] = a11 * b21 + a21 * b22;
	r[3] = a12 * b21 + a22 * b22;
	r[4] = a11 * b31 + a21 * b32 + a31;
	r[5] = a12 * b31 + a22 * b32 + a32;
	return args[2];
}

static OBJ
primTransformPointWithMatrix2x3Into(int nargs, OBJ args[]) {
	if (nargs < 3) return primFailed("Not enough arguments");
	if (!IS_CLASS(args[0], BinaryDataClass) || objWords(args[0]) < 6) {
		return primFailed("Not a matrix");
	}
	if (!IS_CLASS(args[1], BinaryDataClass) || objWords(args[1]) < 2 ||
	    !IS_CLASS(args[2], BinaryDataClass) || objWords(args[2]) < 2) {
		return primFailed("Not a point");

	}
	float *m = (float*)BODY(args[0]);
	float a = m[0], b = m[1], c = m[2], d = m[3], e = m[4], f = m[5];
	float *p = (float*)BODY(args[1]);
	float x = p[0], y = p[1];
	float *r = (float*)BODY(args[2]);

	r[0] = a * x + c * y + e;
	r[1] = b * x + d * y + f;
	return args[2];
}

static OBJ
primInvertPointWithMatrix2x3Into(int nargs, OBJ args[]) {
	if (nargs < 3) return primFailed("Not enough arguments");
	if (!IS_CLASS(args[0], BinaryDataClass) || objWords(args[0]) < 6) {
		return primFailed("Not a matrix");
	}
	if (!IS_CLASS(args[1], BinaryDataClass) || objWords(args[1]) < 2 ||
	    !IS_CLASS(args[2], BinaryDataClass) || objWords(args[2]) < 2) {
		return primFailed("Not a point");
	}
	float *m = (float*)BODY(args[0]);
	float a = m[0], b = m[1], c = m[2], d = m[3], e = m[4], f = m[5];
	float *p = (float*)BODY(args[1]);
	float x = p[0], y = p[1];
	float *r = (float*)BODY(args[2]);

        float det = a * d - b * c;
	if (det == 0) {
		memset(r, 0, sizeof(float) * 2);
		return args[2];
	}

	x = x - e;
	y = y - f;
	det = 1 / det;
	float detX = x * d - c * y;
	float detY = a * y - x * b;

	r[0] = detX * det;
	r[1] = detY * det;
	return args[2];
}

static void
transformPointInto(float *m, float *p, float *r) {
	float a = m[0], b = m[1], c = m[2], d = m[3], e = m[4], f = m[5];
	float x = p[0], y = p[1];

	r[0] = a * x + c * y + e;
	r[1] = b * x + d * y + f;
}

static OBJ
primTransformRectWithMatrix2x3Into(int nargs, OBJ args[]) {
	if (nargs < 3) return primFailed("Not enough arguments");
	if (!IS_CLASS(args[0], BinaryDataClass) || objWords(args[0]) < 6) {
		return primFailed("Not a matrix");
	}
	if (!IS_CLASS(args[1], BinaryDataClass) || objWords(args[1]) < 4 ||
	    !IS_CLASS(args[2], BinaryDataClass) || objWords(args[2]) < 4) {
		return primFailed("Not a Rectangle");
	}
	float *m = (float*)BODY(args[0]);
	float *srcRect = (float*)BODY(args[1]);
	float x1 = srcRect[0], y1 = srcRect[1], x2 = srcRect[2], y2 = srcRect[3];
	float *r = (float*)BODY(args[2]);

        float minX = MAXFLOAT;
        float minY = MAXFLOAT;
        float maxX = -MAXFLOAT;
        float maxY = -MAXFLOAT;

	float aPoint[2] = {0.0, 0.0};
	float bPoint[2] = {0.0, 0.0};

	aPoint[0] = x1;
	aPoint[1] = y1;
	transformPointInto(m, aPoint, bPoint);
	if (bPoint[0] < minX) {minX = bPoint[0];}
	if (bPoint[0] > maxX) {maxX = bPoint[0];}
	if (bPoint[1] < minY) {minY = bPoint[1];}
	if (bPoint[1] > maxY) {maxY = bPoint[1];}

	aPoint[0] = x1;
	aPoint[1] = y2;
	transformPointInto(m, aPoint, bPoint);
	if (bPoint[0] < minX) {minX = bPoint[0];}
	if (bPoint[0] > maxX) {maxX = bPoint[0];}
	if (bPoint[1] < minY) {minY = bPoint[1];}
	if (bPoint[1] > maxY) {maxY = bPoint[1];}

	aPoint[0] = x2;
	aPoint[1] = y1;
	transformPointInto(m, aPoint, bPoint);
	if (bPoint[0] < minX) {minX = bPoint[0];}
	if (bPoint[0] > maxX) {maxX = bPoint[0];}
	if (bPoint[1] < minY) {minY = bPoint[1];}
	if (bPoint[1] > maxY) {maxY = bPoint[1];}

	aPoint[0] = x2;
	aPoint[1] = y2;
	transformPointInto(m, aPoint, bPoint);
	if (bPoint[0] < minX) {minX = bPoint[0];}
	if (bPoint[0] > maxX) {maxX = bPoint[0];}
	if (bPoint[1] < minY) {minY = bPoint[1];}
	if (bPoint[1] > maxY) {maxY = bPoint[1];}

	r[0] = minX; r[1] = minY; r[2] = maxX; r[3] = maxY;

	return args[2];
}


// ***** Gezira Primitives *****

PrimEntry geziraPrimList[] = {
	{"-----", NULL, "Gezira"},
	{"initializeGezira",		primInitializeGezira,	"Create a Gezira object."},
	{"geziraShutDown",		primShutDownGezira,	"release the Gezira object."},
	{"allocateGeziraImage",		primAllocateGeziraImage,	"allocate a gezira image."},
	{"setGezizaImage",		primSetGezizaImage,	"Set the target bitmap into an external reference that represents gezira image. Use carefully."},
	{"geziraImageExtent",		primGeziraImageExtent,	"return an array with width and height of a gezira image."},
	{"geziraPipeline",		primPipeline,		"Construct a pipeline from a list of Gezira objects."},
	{"geziraFeed",			primFeed,		"Feed geometry data into a pipeline"},
	{"geziraSync",			primGeziraSync,			"wait until all work in the pipeline."},
	{"-----", NULL, "Matrix2x3"},
	{"inverseMatrix2x3Into",	primInverseMatrix2x3Into,  "compute inverse matrix."},
	{"rotateMatrix2x3Into",		primRotateMatrix2x3Into,   "compute a rotated matrix."},
	{"scaleMatrix2x3Into",		primScaleMatrix2x3Into,    "compute a scaled matrix."},
	{"translateMatrix2x3Into",	primTranslateMatrix2x3Into, "compute a translated matrix."},
	{"composedMatrix2x3WithLocalInto", primComposedMatrix2x3WithLocalInto, "compute a composition of two matrice."},
	{"transformPointWithMatrix2x3Into", primTransformPointWithMatrix2x3Into, "transform a point"},
	{"invertPointWithMatrix2x3Into", primInvertPointWithMatrix2x3Into, "transform a point with inverted matrix"},
	{"transformRectWithMatrix2x3Into", primTransformRectWithMatrix2x3Into, "computes the global bounds from a local rectangle"},
};

PrimEntry* geziraPrimitives(int *primCount) {
	*primCount = sizeof(geziraPrimList) / sizeof(PrimEntry);
	return geziraPrimList;
}
