call (function {
  module = (loadModule 'GeziraMatrix2x3')
  Matrix = (at module 'Matrix')
  Point = (at module 'Point')
  Rectangle = (at module 'Rectangle')

  m = (beIdentity (init (new Matrix)))

  assert (m11 m) 1.0
  assert (m12 m) 0.0
  assert (m21 m) 0.0
  assert (m22 m) 1.0
  assert (m31 m) 0.0
  assert (m32 m) 0.0

  r = (rotatedBy m 45)

  assert (m11 m) 1.0
  assert (m12 m) 0.0
  assert (m21 m) 0.0
  assert (m22 m) 1.0
  assert (m31 m) 0.0
  assert (m32 m) 0.0

  assert ((abs ((m11 r) - (cos 45)))         < 0.00000025) true
  assert ((abs ((m12 r) - (sin 45)))         < 0.00000025) true
  assert ((abs ((m21 r) - (0.0 - (sin 45)))) < 0.00000025) true
  assert ((abs ((m22 r) - (cos 45)))         < 0.00000025) true
  assert (m31 r) 0.0
  assert (m32 r) 0.0

  r = (scaledBy m 0.25)

  assert (m11 m) 1.0
  assert (m12 m) 0.0
  assert (m21 m) 0.0
  assert (m22 m) 1.0
  assert (m31 m) 0.0
  assert (m32 m) 0.0

  assert (m11 r) 0.25
  assert (m12 r) 0.0
  assert (m21 r) 0.0
  assert (m22 r) 0.25
  assert (m31 r) 0.0
  assert (m32 r) 0.0

  m = (beIdentity (init (new Matrix)))
  t = (translatedBy m 25 25)
  r1 = (rotatedBy t 45)

  r2 = (composedWith r1 r1)

  r3 = (rotatedBy r1 45)

  assert ((abs ((m11 r2) - (m11 r3))) < 0.00000025) true
  assert ((abs ((m12 r2) - (m12 r3))) < 0.00000025) true
  assert ((abs ((m21 r2) - (m21 r3))) < 0.00000025) true
  assert ((abs ((m22 r2) - (m22 r3))) < 0.00000025) true

  m = (beIdentity (init (new Matrix)))
  t = (translatedBy m 25 25)
  r1 = (rotatedBy t 45)

  p = (init (new Point))
  setX p 10
  setY p 5
  r = (transformPoint r1 p)
  assert (toString r) '(28.5355, 35.6066)'

  p2 = (invertPoint r1 r)
  assert (toString p2) '(10, 5)'

  m = (beIdentity (init (new Matrix)))
  t = (translatedBy m 25 25)
  r1 = (rotatedBy t 45)

  assert (rotation r1) 45.0
  assert (rotation r2) 90.0

  m = (beIdentity (init (new Matrix)))
//  t = (translatedBy m 25 25)
//  t = (scaledBy m 0.3)
  r1 = (rotatedBy m 45)

  rect = (init (new Rectangle) 0 0 100 100)

  result = (transformRect r1 rect)

  assert (toString result) '((-70.7107,0.0), (70.7107,141.421))'

})
