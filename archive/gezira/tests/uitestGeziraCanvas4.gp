call (function {

gezira = (loadModule 'GeziraCanvas')
canvas = (init (new (at gezira 'GeziraCanvas')))
bitmap = (newBitmap 500 500)
fill bitmap (color 255 255 255)
setTarget canvas bitmap

box = (call (at gezira 'rectPath') 0 0 200 200 0)

tr = (beIdentity (new (at gezira 'Matrix')))

translateBy tr 100 100
rotateBy tr 45
scaleBy tr 0.6

transformBy canvas tr

fill = (init (new (at gezira 'ImageFill')))
gp = (readFrom (new 'PNGReader') (readFile 'tests/gp.png' true))
setBitmap fill gp

setFill canvas fill

//stroke = (call (at gezira 'stroke'))
//setWidth stroke 3

//setStroke canvas stroke

drawPath canvas box
sync canvas

openWindow

drawBitmap nil bitmap
flipBuffer
sleep 2000

geziraShutDown
})
