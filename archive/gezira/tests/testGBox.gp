call (function {
boxModule = (loadModule 'GeziraBox')
Box = (at boxModule 'Box')

topBox = (init (new Box))

aBox = (init (new Box))

setPosition aBox 100 100
setExtent aBox 200 200
setScale aBox 1.5
addPart topBox aBox

bBox = (init (new Box))
setPosition bBox 100 180
addPart aBox bBox

cBox = (init (new Box))
setPosition cBox 170 100
addPart bBox cBox

trans = (transformFromGlobal cBox)
assert (m31 trans) (100 + (* (100 + 170) 1.5))
assert (m32 trans) (100 + (* (180 + 100) 1.5))

setRotation bBox 30

trans = (transformFromGlobal cBox)
assert (toString (m31 trans)) (toString
          ((100 + (100 * 1.5)) + (((170 * 1.5) * (cos 30)) - ((100 * 1.5) * (sin 30)))))
assert (toString (m32 trans)) (toString
          ((100 + (180 * 1.5)) + (((170 * 1.5) * (sin 30)) + ((100 * 1.5) * (cos 30)))))

})
