call (function {

  boxModule = (loadModule 'GeziraBox')
  Matrix = (at boxModule 'Matrix')
  Point = (at boxModule 'Point')
  Box = (at boxModule 'Box')

aBox = (init (new Box))

setPosition aBox 100 100

bBox = (init (new Box))
setPosition bBox 100 100
addPart aBox bBox

assert (x (localPoint bBox (init (new Point) 210 210))) 110  'x local'
assert (y (localPoint bBox (init (new Point) 210 210))) 110  'y local'

assert (x (globalPoint bBox (init (new Point) 110 110))) 210  'x global'
assert (y (globalPoint bBox (init (new Point) 110 110))) 210  'y global'

setScale aBox 0.5

assert (x (localPoint aBox (init (new Point) 210 210))) 210  'x local a scaled'
assert (y (localPoint aBox (init (new Point) 210 210))) 210  'y local a scaled'

})
