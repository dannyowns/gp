call (function {
boxModule = (loadModule 'GeziraBox')
Box = (at boxModule 'Box')

topBox = (init (new Box))
setExtent topBox 500 500
setFill topBox (call (at boxModule 'uniformColorFill') (color 200 200 200))

aBox = (init (new Box))

setPosition aBox 200 20
setExtent aBox 200 50
setRotation aBox 30
setFill aBox (call (at boxModule 'uniformColorFill') (color 250 250 250))
addPart topBox aBox
call (at boxModule 'setHandler') aBox (init (new (at boxModule 'Slider')))

bBox = (init (new Box))
setPosition bBox 0 0
setExtent bBox 20 50
setFill bBox (call (at boxModule 'uniformColorFill') (color 10 10 10))
addPart aBox bBox

a = (call (at boxModule 'openBoxAsWindow') topBox)
startStepping a
})
